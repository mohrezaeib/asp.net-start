﻿using Microsoft.Extensions.Configuration;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System;

using Utility.Tools;

namespace Utility.Tools.EmailServies
{
    public interface ISimpleEmailSender 
    {
        Task<string> SendAsync(string Text, string Destination);
        Task<string> SendAsync(string email,  string subject, string body, string ReceiverName ="", bool isHtml = false);
    }
}
