﻿using System.Threading.Tasks;

namespace Utility.Tools.SMS
{
    public interface ISMSService
    {
        Task<string> SendAsync(string Text,string Destination);
    }
   
}
