﻿//using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using Twilio;
using Twilio.Clients;
using Utility.Tools.SMS.TwilioSMS;

namespace Utility.Tools.SMS
{
    public static class Extensions
    {
        public static void AddTwilioSMSService(this IServiceCollection services)
        {

            services.AddHttpClient<ITwilioRestClient, TwilioClient>();
            services.AddScoped<ISMSService, TwilioService>();

        }
    }
}
