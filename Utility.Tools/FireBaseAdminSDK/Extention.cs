﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Utility.Tools
{
    public static class Extention
    {
        public static void FireBaseNotification(this IServiceCollection services, IConfiguration config)
        {
            services.AddSingleton<IFireBase, FireBaseAdminSDK>();
        }
    }
}
