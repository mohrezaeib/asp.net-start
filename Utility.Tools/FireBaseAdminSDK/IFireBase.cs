﻿using FirebaseAdmin.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Utility.Tools
{
    public interface IFireBase
    {
        public  Task SendNotification(string token, Notification not);
        public  Task SendNotification(string token, string text);
        public  Task SendNotification(List<string> token, string text);
        public  Task SendNotification(List<string> token, Notification not);
    }
}
