# Get the base image
FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build-env
WORKDIR /app
COPY . .
WORKDIR /app/API
RUN dotnet publish -c Release -o /app/out/

# Build runtime image
FROM mcr.microsoft.com/dotnet/sdk:5.0
WORKDIR /app
COPY --from=build-env /app/out .
EXPOSE 5000
EXPOSE 5001
ENTRYPOINT ["dotnet", "API.dll"]
