﻿
using DomainData.Contracts;
using DomainData.Ef.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace DomainData.Ef.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {

        public UnitOfWork(

            IUserRepository Users,
            IUserRoleRepository UserRole,
            IUserNotificationRepository UserNotification,
            INotificationRepository Notification ,


            IRoleRepository Role,
            IActiveCodeRepository ActiveCode,
            IDeviceRepository Device,
            ISocialRepository Social,
            IAnnouncementRepository Announcement,
            IFaQRepository FaQ,
            IFaQCategoryRepository FaQCategory,
            IAboutUsRepository AboutUs,
            IContactUsRepository ContactUs,
            IDocumentRepository Document,
            ICityRepository City,
            IProvinceRepository Province,
            ICountryRepository Country,
            IUpdateRepository Update,
           
            ISliderRepository Slider,
            
            IChatRepository Chat,
            IMessageRepository Message,
            

        IContext ctx
            )
        {
            this.Users = Users;
            this.UserRole = UserRole;
            this.UserNotification = UserNotification;
            this.Notification = Notification;
    
            this.Role = Role;
            this.ActiveCode = ActiveCode;
            this.Device = Device;
            this.Social = Social;
            this.Announcement = Announcement;
            this.FaQ = FaQ;
            this.FaQCategory = FaQCategory;
            this.AboutUs = AboutUs;
            this.ContactUs = ContactUs;
            this.Document = Document;
            this.City = City;
            this.Province = Province;
            this.Country = Country;
            this.Update = Update;
         
            this.Slider = Slider;
       
            this.Chat = Chat;
            this.Message = Message;
     
            this.ctx = ctx;

        }
        public IUserRepository Users { get; set; }
    
        public IUserRoleRepository UserRole { get; set; }
        public IUserNotificationRepository UserNotification { get; set; }
        public INotificationRepository Notification { get; set; }

       

        public IRoleRepository Role { get; set; }
        public IActiveCodeRepository ActiveCode { get; set; }
        public ISocialRepository Social { get; set; }
        public IAnnouncementRepository Announcement { get; set; }
        public IFaQRepository FaQ { get; set; }
        public IFaQCategoryRepository FaQCategory { get; set; }
        public IAboutUsRepository AboutUs { get; set; }
        public IContactUsRepository ContactUs { get; set; }

        public IDeviceRepository Device { get; set; }
        public IDocumentRepository Document { get; set; }
        public ICityRepository City { get; set; }
        public IProvinceRepository Province { get; set; }
        public ICountryRepository Country { get; set; }
        // public ITransactionCategoryRepository TransactionCategory { get; set; }
        public IUpdateRepository Update { get; set; }
     
        public ISliderRepository Slider { get; set; }
     
        public IChatRepository Chat { get; set; }
        public IMessageRepository Message { get; set; }
       
        public IContext ctx { get; set; }


        public void Complete()
        {
            ctx.SaveChanges();
        }
        public async Task CompleteAsync()
        {
            // CancellationToken cancellationToken = new CancellationToken();
            await ctx.SaveChangesAsync();
        }
    }
}
