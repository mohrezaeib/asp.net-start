﻿

using DomainData.Contracts;
using DomainCore.Entities;
using DomainCore.AppUsers;
using DomainCore.General;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DomainCore.Entities.General;

using DomainCore.Entities.AppMore;
using DomainCore.Entities.SupportChat;

using System.Threading;
using Microsoft.AspNetCore.Identity;

namespace DomainData.Ef.Context
{
    public interface IContext
    {
        //DbSet<LoggedEvent> LoggedEvents { get; set; }
        DbSet<IdentityUser> IdentityUsers { get; set; }
        DbSet<User> AppUsers { get; set; }
       
        
         DbSet<UserNotification> UserNotifications { get; set; }
        DbSet<Notification> Notifications { get; set; }
       

        DbSet<Role> AppRoles { get; set; }
        DbSet<UserRole> AppUserRoles { get; set; }
        DbSet<ActiveCode> ActiveCodes { get; set; }
        DbSet<Social> Socials { get; set; }
        DbSet<AboutUs> AboutUs { get; set; }
        DbSet<ContactUs> ContactUs { get; set; }
         DbSet<Slider> Sliders { get; set; }

        DbSet<FaQ> FaQs { get; set; }
        DbSet<FaQCategory> FaQCategorys { get; set; }
        DbSet<Device> Devices { get; set; }
        DbSet<Document> Documents { get; set; }
        DbSet<PrivateDocument> PrivateDocuments { get; set; }
        DbSet<DocumentUser> DocumentUsers { get; set; }
        DbSet<City> Cities { get; set; }
        DbSet<Country> Countries { get; set; }
        DbSet<Province> Provinces { get; set; }
        DbSet<Update> Updates { get; set; }

       
        DbSet<Chat> Chats { get; set; }
        DbSet<Message> Messages { get; set; }
       
        DbSet<Announcement> Announcements { get; set; }

        int SaveChanges();
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken));
    }
}
