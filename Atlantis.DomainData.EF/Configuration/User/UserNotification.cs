﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DomainData.Ef.Configuration
{
    public class UserNotification : IEntityTypeConfiguration<DomainCore.AppUsers.UserNotification>
    {
        public void Configure(EntityTypeBuilder<DomainCore.AppUsers.UserNotification> builder)
        {
            builder.HasKey(p => new { p.Id });
            builder.HasQueryFilter(m => !m.isDeleted);


        }
    }

}