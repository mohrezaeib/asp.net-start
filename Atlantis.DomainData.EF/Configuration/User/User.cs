﻿using DomainCore.AppUsers;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DomainData.Ef.Configuration
{
    public class User : IEntityTypeConfiguration<DomainCore.AppUsers.User>
    {
        public void Configure(EntityTypeBuilder<DomainCore.AppUsers.User> builder)
        {
            builder.HasKey(p => new { p.Id });
            builder.HasQueryFilter(m => !m.isDeleted);
            builder.OwnsOne(p => p.Email, m =>
           {
               m.HasIndex(mm => mm.Address).IsUnique();
           });/*.HasForeignKey(p=> p.UserId)*/;
            builder.OwnsOne(p => p.Mobile, m =>
            {
                m.HasIndex(mm => mm.FullNumber).IsUnique();
            });

            /*.HasForeignKey(p => p.UserId)*/
            ;

            builder.Property(p => p.Status).IsRequired();

            builder.HasOne(p => p.ProfileImage).WithOne().HasForeignKey<DomainCore.AppUsers.User>(p => p.ProfileImageId).OnDelete(DeleteBehavior.Cascade);

            builder.HasMany(p => p.UserRole).WithOne(p => p.User).HasForeignKey(p => p.UserId).OnDelete(DeleteBehavior.Restrict);
            builder.HasMany(p => p.Documents).WithOne(p => p.User).HasForeignKey(p => p.UserId).OnDelete(DeleteBehavior.Restrict);
            builder.OwnsOne(p => p.UserSetting).WithOwner(p => p.User)
               ;
           
        }
    }

}