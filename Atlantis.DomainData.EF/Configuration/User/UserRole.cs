﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DomainData.Ef.Configuration
{
    public class UserRole : IEntityTypeConfiguration<DomainCore.AppUsers.UserRole>
    {
        public void Configure(EntityTypeBuilder<DomainCore.AppUsers.UserRole> builder)
        {
            builder.HasKey(p => new { p.Id });
            builder.HasQueryFilter(m => !m.isDeleted);

            builder.HasOne(p => p.User).WithMany(p => p.UserRole).HasForeignKey(p => p.UserId).OnDelete(DeleteBehavior.Restrict);
            builder.HasOne(p => p.Role).WithMany(p=> p.UserRole).HasForeignKey(p => p.RoleId).OnDelete(DeleteBehavior.Restrict);

        }
    }
    
}