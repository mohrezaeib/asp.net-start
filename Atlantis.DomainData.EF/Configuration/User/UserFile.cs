﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DomainData.Ef.Configuration
{
    public class UserFile : IEntityTypeConfiguration<DomainCore.AppUsers.UserFile>
    {
        public void Configure(EntityTypeBuilder<DomainCore.AppUsers.UserFile> builder)
        {
            builder.HasKey(p => new { p.Id });
            builder.HasQueryFilter(m => !m.isDeleted);

            builder.Property(p => p.CreatedAt).IsRequired();
            builder.HasOne(p => p.Document).WithMany().HasForeignKey(p => p.DocumentId).OnDelete(DeleteBehavior.Restrict);


        }
    }

}