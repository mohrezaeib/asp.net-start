﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DomainData.Ef.Configuration
{
    public class Announcement : IEntityTypeConfiguration<DomainCore.Entities.AppMore.Announcement>
    {
        public void Configure(EntityTypeBuilder<DomainCore.Entities.AppMore.Announcement> builder)
        {
            builder.HasKey(p => new { p.Id });
            //builder.Property<bool>("isDeleted");
            //builder.HasQueryFilter(m => EF.Property<bool>(m, "isDeleted"));
            builder.HasQueryFilter(m => !m.isDeleted);



        }
    }
}
