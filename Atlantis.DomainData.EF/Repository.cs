﻿using DomainCore.Base;
using DomainData.Contracts;
using DomainData.Ef.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DomainData.Ef
{
    public class Repository<TEntity > : IRepository<TEntity> where TEntity : BaseEntity
    {

        public   IQueryable<TEntity> baseInclude;
        private readonly DbContext Context;

        public Repository(IContext context  )
        {
            Context = (context as DbContext);

            baseInclude = Context.Set<TEntity>();
        }
        public async Task<List<TEntity>> GetAllAsync()
        {
            return await baseInclude.ToListAsync();

        }
      
        public async Task<TEntity> GetByIdAsync(object Id)
        {
            
            Expression<Func<TEntity, bool>> predict = (TEntity p) =>(p.Id ==Id); 
            return await baseInclude.FirstOrDefaultAsync(predict);

        }
        public async Task<TEntity> GetFirstOrDefaultAsync(Expression<Func<TEntity, bool>> predicate)
        {

            return await baseInclude.FirstOrDefaultAsync(predicate);

        }
        public async Task<List<TEntity>> GetByExpression(Expression<Func<TEntity, bool>> predicate)
        {
            return await baseInclude.Where(predicate).ToListAsync();
        }




        public List<TEntity> FindAll()
        {
            return Context.Set<TEntity>().ToList();
        }
        public async Task<List<TEntity>> FindAllAsync()
        {
            return await Context.Set<TEntity>().ToListAsync();
        }

        public TEntity FindById(object id)
        {
            return Context.Set<TEntity>().Find(id);
        }
        public async Task<TEntity> FindByIdAsync(object id)
        {
            return await Context.Set<TEntity>().FindAsync(id);
        }
        public List<TEntity> FindByExpression(Expression<Func<TEntity, bool>> predicate)
        {
            return Context.Set<TEntity>().Where(predicate).ToList();
        }
  
        
        public Task<List<TEntity>> FindByExpressionAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return Context.Set<TEntity>().Where(predicate).ToListAsync();
        }
  

       

        public void Add(TEntity entity)
        {
            Context.Set<TEntity>().Add(entity);
        }
        public async Task AddAsync(TEntity entity)
        {
            await Context.Set<TEntity>().AddAsync(entity);
        }
        public void AddRange(List<TEntity> entities)
        {
            Context.Set<TEntity>().AddRange(entities);
        }

       

        public async Task AddRangeAsync(List<TEntity> entities)
        {
            await Context.Set<TEntity>().AddRangeAsync(entities);
        }

        public void Remove(TEntity entity)
        {
            Context.Set<TEntity>().Remove(entity);
        }

        public void RemoveRange(IEnumerable<TEntity> entities)
        {
            Context.Set<TEntity>().RemoveRange(entities);
        } 
 

        public IEnumerable<TEntity> ExecuteStored(string ProcedureName, object[] parameters)
        {
            for (int i = 0; i < parameters.Length; i++)
                ProcedureName += " @p" + i.ToString() + ",";
            ProcedureName = ProcedureName.Remove(ProcedureName.Length - 1, 1);
            return Context.Set<TEntity>().FromSqlRaw(ProcedureName, parameters);
        }





        public void Update(TEntity obj)
        {
            Context.Set<TEntity>().Update(obj);
        }

        public void DeleteById(object id)
        {
         
            var obj = FindById(id);
            Context.Set<TEntity>().Remove(obj);
        }



    
        public T GetMax<T>(Expression<Func<TEntity, int>> expression)
        {
            if (Context.Set<TEntity>().Count() == 0)
                return (T)Convert.ChangeType(0, typeof(T));
            return (T)Convert.ChangeType(Context.Set<TEntity>().Max(expression), typeof(T));
        }

        public int GetTotalCount()
        {
            return Context.Set<TEntity>().Count();
        }

        public int GetCountWhere(Expression<Func<TEntity, bool>> predicate)
        {
            return Context.Set<TEntity>().Where(predicate).Count();
        }
        public List<TEntity> GetByRange(Expression<Func<TEntity, bool>> predicate , int start , int items)
        {
            return Context.Set<TEntity>().Where(predicate).Skip(start).Take(items).ToList();
        }

      
    }
}
