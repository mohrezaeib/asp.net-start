﻿
using DomainData.Contracts;
using DomainCore.Entities;
using DomainCore.AppUsers;
using DomainCore.General;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using DomainData.Ef.Extentions;
using System.Threading.Tasks;
using DomainCore.Entities.General;
using System.Threading;
using DomainCore.Base;
using Utility.Tools.General;

using DomainCore.Entities.AppMore;
using DomainCore.Entities.SupportChat;
using System;

using MediatR;
using DomainCore.Entities.BaseEntities;
using Microsoft.EntityFrameworkCore.Storage;
using System.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace DomainData.Ef.Context
{
    public class MainContext : IdentityDbContext, IContext
    {
        private readonly ICurrentUser sessionContext;
        private readonly IMediator mediator;

        // public DbSet<LoggedEvent> LoggedEvents { get; set; }
        public DbSet<IdentityUser> IdentityUsers { get; set; }

        public DbSet<User> AppUsers { get; set; }
        public DbSet<UserNotification> UserNotifications { get; set; }
        public DbSet<Notification> Notifications { get; set; }
      

        public DbSet<Role> AppRoles { get; set; }
        public DbSet<UserRole> AppUserRoles { get; set; }
        public DbSet<Social> Socials { get; set; }
        public DbSet<AboutUs> AboutUs { get; set; }
        public DbSet<ContactUs> ContactUs { get; set; }
        public DbSet<FaQ> FaQs { get; set; }
        public DbSet<Announcement> Announcements { get; set; }
        public DbSet<FaQCategory> FaQCategorys { get; set; }

        public DbSet<ActiveCode> ActiveCodes { get; set; }
        public DbSet<Device> Devices { get; set; }
        public DbSet<Document> Documents { get; set; }
        public DbSet<PrivateDocument> PrivateDocuments { get; set; }
        public DbSet<DocumentUser> DocumentUsers { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Province> Provinces { get; set; }
        public DbSet<Update> Updates { get; set; }

     
        public DbSet<Slider> Sliders { get; set; }
       
        public DbSet<Chat> Chats { get; set; }
        public DbSet<Message> Messages { get; set; }
        
        public MainContext(DbContextOptions options, ICurrentUser sessionContext
            , IMediator mediator
            ) : base(options)
        {
            this.sessionContext = sessionContext;
            this.mediator = mediator;
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.AddEntityConfiguration();
            builder.SeedData();
            builder.GetCitiesFromJson();


        }


        public override int SaveChanges()
        {

            this.TrackAuditableEntities();
            mediator.DispatchBeforeSaveChangesDomainEventsAsync(this).Wait(); 
            var result = base.SaveChanges();
            mediator.DispatchBeforeSaveChangesDomainEventsAsync(this).Wait();
            return result;

        }




        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            TrackAuditableEntities();
            await mediator.DispatchBeforeSaveChangesDomainEventsAsync(this);
            var result = await base.SaveChangesAsync(cancellationToken);
            await mediator.DispatchAfterSaveChangesDomainEventsAsync(this);
            return result;
        }

        public void TrackAuditableEntities()
        {

            UpdateSoftDeleteStatuses();

            // Get all the entities that inherit from AuditableEntity
            // and have a state of Added or Modified
            var entries = ChangeTracker
                .Entries()
                .Where(e => e.Entity is IAuditable && (
                        e.State == EntityState.Added
                        || e.State == EntityState.Modified));

            // For each entity we will set the Audit properties
            foreach (var entityEntry in entries)
            {
                // If the entity state is Added let's set
                // the CreatedAt and CreatedBy properties
                if (entityEntry.State == EntityState.Added)
                {
                    ((IAuditable)entityEntry.Entity).CreatedAt = Agent.Now;
                    ((IAuditable)entityEntry.Entity).CreatedAtDate = DateTime.Now;
                    ((IAuditable)entityEntry.Entity).CreatedBy = sessionContext?.UserId;
                }
                else
                {
                    // If the state is Modified then we don't want
                    // to modify the CreatedAt and CreatedBy properties
                    // so we set their state as IsModified to false
                    Entry((IAuditable)entityEntry.Entity).Property(p => p.CreatedAt).IsModified = false;
                    Entry((IAuditable)entityEntry.Entity).Property(p => p.CreatedAtDate).IsModified = false;
                    Entry((IAuditable)entityEntry.Entity).Property(p => p.CreatedBy).IsModified = false;
                }

                // In any case we always want to set the properties
                // ModifiedAt and ModifiedBy
                ((IAuditable)entityEntry.Entity).UpdatedAt = Agent.Now;
                ((IAuditable)entityEntry.Entity).UpdatedAtDate = DateTime.Now;
                ((IAuditable)entityEntry.Entity).UpdatedBy = sessionContext?.UserId;
            }

            // After we set all the needed properties
            // we call the base implementation of SaveChangesAsync
            // to actually save our entities in the database
        }

        private void UpdateSoftDeleteStatuses()
        {
            foreach (var entry in ChangeTracker.Entries().Where(e => e.Entity is IAuditable))
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.CurrentValues["isDeleted"] = false;
                        break;
                    case EntityState.Deleted:
                        entry.State = EntityState.Modified;
                        entry.CurrentValues["isDeleted"] = true;
                        break;
                }
            }
        }

    }

    static class MediatorExtension
    {
        public static async Task DispatchAfterSaveChangesDomainEventsAsync(this IMediator mediator, MainContext ctx)
        {
            var domainEntities = ctx.ChangeTracker
                .Entries<BaseEntity>().ToList()
                ;
            domainEntities = domainEntities.Where(x => x.Entity.DomainEvents != null && x.Entity.DomainEvents.Any())
                .ToList();

            var domainEvents = domainEntities
                .SelectMany(x => x.Entity.DomainEvents)
                .Where(p => (p is IDispatchRightAfterSaveChanges)).ToList();

            foreach (var domainEvent in domainEvents)
                await mediator.Publish(domainEvent);
        }
        public static async Task DispatchBeforeSaveChangesDomainEventsAsync(this IMediator mediator, MainContext ctx)
        {
            var domainEntities = ctx.ChangeTracker
                .Entries<BaseEntity>().ToList()
                ;
            domainEntities = domainEntities
                .Where(x => x.Entity.DomainEvents != null && x.Entity.DomainEvents.Any())
                .ToList();

            var domainEvents = domainEntities
                .SelectMany(x => x.Entity.DomainEvents)
                .Where(p => (p is IDispatchRightBeforeSaveChanges)).ToList();

            foreach (var domainEvent in domainEvents)
                await mediator.Publish(domainEvent);
        }
        public static async Task SaveInDBForDispatchLater(this IMediator mediator, MainContext ctx)
        {
            var domainEntities = ctx.ChangeTracker
                .Entries<BaseEntity>().ToList();

            domainEntities = domainEntities
                .Where(x => x.Entity.DomainEvents != null && x.Entity.DomainEvents.Any())
                .ToList();

            var domainEvents = domainEntities
                .SelectMany(x => x.Entity.DomainEvents)
                .Where(p => (p is ISaveInDBAndDispatchSomeTimeLater)).ToList();



        }


    }
}
