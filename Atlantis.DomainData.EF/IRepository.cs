﻿using DomainCore.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DomainData.Contracts
{
    public interface IRepository<TEntity> where TEntity : class
    {
        Task<List<TEntity>> GetAllAsync();
        Task<TEntity> GetFirstOrDefaultAsync(Expression<Func<TEntity, bool>> predicate);
        Task<List<TEntity>> GetByExpression(Expression<Func<TEntity, bool>> predicate);

        Task<TEntity> GetByIdAsync(object Id);

        TEntity FindById(object id);
        Task<TEntity> FindByIdAsync(object id);
        int GetTotalCount();
        List<TEntity> FindAll();
        Task<List<TEntity>> FindAllAsync();
        void Update(TEntity obj);
        void DeleteById(object id);
        List<TEntity> FindByExpression(Expression<Func<TEntity, bool>> predicate);
        Task<List<TEntity>> FindByExpressionAsync(Expression<Func<TEntity, bool>> predicate);


        void Add(TEntity entity);
        void AddRange(List<TEntity> entities);
        Task AddAsync(TEntity entity);
        Task AddRangeAsync(List<TEntity> entities);

        void Remove(TEntity entity);
        void RemoveRange(IEnumerable<TEntity> entities);
        IEnumerable<TEntity> ExecuteStored(string ProcedureName, object[] parameters);
        T GetMax<T>(Expression<Func<TEntity, int>> expression);

        int GetCountWhere(Expression<Func<TEntity, bool>> predicate);
        List<TEntity> GetByRange(Expression<Func<TEntity,
            bool>> predicate, int start, int items);
    }

}
