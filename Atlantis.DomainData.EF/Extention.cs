﻿using Core.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

using System.Threading;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using Microsoft.EntityFrameworkCore.Query.Internal;


using Microsoft.Extensions.DependencyInjection;
using Enums;
using Utility.Tools.Auth;
using Utility.Tools.General;
using MD.PersianDateTime.Standard;
using System.IO;
using DomainCore.Dto.Base;
using DomainCore.AppUsers;
using DomainCore.Enums;
using Newtonsoft.Json;

using DomainCore.General;

using DomainCore.Entities.General;
using DomainCore.Entities.AppMore;
using Microsoft.AspNetCore.Builder;
using DomainData.Ef.Context;

namespace DomainData.Ef.Extentions
{

    // Getting the scope of our database context

    public static class EnumerableExtensions
    {
        public static IQueryable<T> OrderByAdaptive<T, TKey>(
            this IQueryable<T> enumr,
            Func<T, TKey> selector,
            bool? ascending
        )
        {
            return ascending == null ?
                enumr :
                (ascending.Value ? enumr.OrderBy(selector).AsQueryable()
                : enumr.OrderByDescending(selector).AsQueryable());
        }


        public static IEnumerable<T> OrderByAdaptive<T, TKey>(
            this IEnumerable<T> enumr,
            Func<T, TKey> selector,
            IComparer<TKey> comparer,
            bool ascending
        )
        {
            return ascending
                ? enumr.OrderBy(selector, comparer)
                : enumr.OrderByDescending(selector, comparer);
        }
    }
    public static class Extention
    {

        public static void MigrationInitialisation(this IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.CreateScope())
            {
                // Takes all of our migrations files and apply them against the database in case they are not implemented
                serviceScope.ServiceProvider.GetService<MainContext>().Database.Migrate();
            }
        }

        public static void AddEntityConfiguration(this ModelBuilder builder)
        {
            var typesToRegister = Assembly.GetExecutingAssembly().GetTypes()
                         .Where(t => t.GetInterfaces().Any(gi => gi.IsGenericType && gi.GetGenericTypeDefinition() == typeof(IEntityTypeConfiguration<>))).ToList();

            foreach (var type in typesToRegister)
            {
                dynamic configurationInstance = Activator.CreateInstance(type);
                builder.ApplyConfiguration(configurationInstance);
            }
        }


        //public static void AddApplicationServices(this IServiceCollection services)
        //{
        //    var applicationServiceType = typeof(IApplicationService).Assembly;
        //    var AllApplicationServices = applicationServiceType.ExportedTypes
        //       .Where(x => x.IsClass && x.IsPublic && x.FullName.Contains("Core.Services")).ToList();
        //    foreach (var type in AllApplicationServices)
        //    {
        //        Console.WriteLine(type.ToString());
        //        Console.WriteLine(type.Name);
        //        services.AddScoped(type.GetInterface($"I{type.Name}"), type);
        //    }
        //}
        public static void AddRepositories(this IServiceCollection services)
        {
            var repositpryType = typeof(Repository<>).Assembly;
            var AllRepositories = repositpryType.ExportedTypes
               .Where(x => x.IsClass && x.IsPublic && x.Name.Contains("Repository") && !x.Name.StartsWith("Repository")).ToList();
            foreach (var type in AllRepositories)
            {
                Console.WriteLine(type.Name);
                services.AddScoped(type.GetInterface($"I{type.Name}"), type);
            }


        }



        public static void SeedData(this ModelBuilder builder)
        {


            builder.Entity<Role>().HasData(new Role { Id = Roles.User, Name = "User" });
            builder.Entity<Role>().HasData(new Role { Id = Roles.Admin, Name = "Admin" });
            builder.Entity<Role>().HasData(new Role { Id = Roles.ClinicManager, Name = "Clinic Manager" });
            builder.Entity<Role>().HasData(new Role { Id = Roles.Employee, Name = "Employee" });
            builder.Entity<Role>().HasData(new Role { Id = Roles.Accountant, Name = "Accountant" });
            builder.Entity<Role>().HasData(new Role { Id = Roles.SalesMan, Name = "Influencer" });
            builder.Entity<Country>().HasData(new Country { Id = 1, Name = "Canada" });

            var now = 0;
            var pass = "a123456A!";
            var lorem = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec nec nunc fermentum, imperdiet urna nec, imperdiet nunc. Aliquam pellentesque elementum dictum. Suspendisse sit amet mauris eget ipsum euismod facilisis quis et nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Cras non sagittis lacus. Nam iaculis scelerisque blandit. Praesent porta est non efficitur volutpat. Nam nisi est, ullamcorper vel lorem id, iaculis sodales ipsum. Phasellus eleifend aliquam justo, vel rhoncus odio facilisis eget. Nulla tincidunt porta ligula, vitae malesuada leo. Sed laoreet nisi fermentum ligula dapibus, at volutpat eros auctor. Phasellus a pellentesque risus, a suscipit orci. Maecenas in luctus nunc. Nulla hendrerit suscipit neque, vitae ornare neque rhoncus in. ";
            User user = new User
            {
                // Mobile = "+989308071499",
                Name = "Mohammad Reza",
                FamilyName = "Rezaei",
                //Email = "mohrezaeib@gmail.com",
                Id = Guid.Parse("11111111-1111-1111-1111-111111111111"),
                Status = UserStatus.Active,
            };
            //var e = new Email("mohrezaeib@gmail.com");
            //var m = new Mobile("+989308071499");
            //m.Number
            user.SetPassword(pass, new Encrypter());
            builder.Entity<User>(p =>
            {
                p.HasData(user);
                p.OwnsOne(p => p.Email).HasData(new { isConfirmed = true, Address = "mohrezaeib@gmail.com", UserId = Guid.Parse("11111111-1111-1111-1111-111111111111"), });
                p.OwnsOne(p => p.Mobile).HasData(new { FullNumber = "989308071499", CountryCode = "98", Number = "9308071499", UserId = Guid.Parse("11111111-1111-1111-1111-111111111111"), });

            });
            builder.Entity<UserRole>().HasData(new UserRole { CreatedAt = now, Id = Guid.Parse("11111111-1111-1111-1111-111111317111"), RoleId = Roles.Admin, UserId = Guid.Parse("11111111-1111-1111-1111-111111111111") });
            builder.Entity<UserRole>().HasData(new UserRole { CreatedAt = now, Id = Guid.Parse("11111111-1111-1111-1111-111111417211"), RoleId = Roles.User, UserId = Guid.Parse("11111111-1111-1111-1111-111111111111") });
            builder.Entity<UserRole>().HasData(new UserRole { CreatedAt = now, Id = Guid.Parse("11111111-1111-1111-1111-111111517311"), RoleId = Roles.ClinicManager, UserId = Guid.Parse("11111111-1111-1111-1111-111111111111") });
            builder.Entity<UserRole>().HasData(new UserRole { CreatedAt = now, Id = Guid.Parse("11111111-1111-1111-1111-111111617311"), RoleId = Roles.Employee, UserId = Guid.Parse("11111111-1111-1111-1111-111111111111") });

            User user2 = new User
            {
                //Mobile = "+989360064055",
                Name = "Kimia",
                FamilyName = "Khaki",
                //Email = "kimiakhaki@gmail.com",
                Id = Guid.Parse("11111111-1111-1111-1111-111111111112"),
                Status = UserStatus.Active,
            };
            user2.SetPassword(pass, new Encrypter());
            builder.Entity<User>(p =>
            {

                p.HasData(user2);
                p.OwnsOne(p => p.Email).HasData(new { isConfirmed = true, Address = "kimiakhaki@gmail.com", UserId = Guid.Parse("11111111-1111-1111-1111-111111111112"), });
                p.OwnsOne(p => p.Mobile).HasData(new { FullNumber = "989360064055", CountryCode = "98", Number = "9360064055", UserId = Guid.Parse("11111111-1111-1111-1111-111111111112"), });


            });

            builder.Entity<UserRole>().HasData(new UserRole { CreatedAt = now, Id = Guid.Parse("11111111-1111-1111-1111-111111107112"), RoleId = Roles.Admin, UserId = Guid.Parse("11111111-1111-1111-1111-111111111112") });
            builder.Entity<UserRole>().HasData(new UserRole { CreatedAt = now, Id = Guid.Parse("11111111-1111-1111-1111-111111117212"), RoleId = Roles.ClinicManager, UserId = Guid.Parse("11111111-1111-1111-1111-111111111112") });
            builder.Entity<UserRole>().HasData(new UserRole { CreatedAt = now, Id = Guid.Parse("11111111-1111-1111-1111-111111127312"), RoleId = Roles.Employee, UserId = Guid.Parse("11111111-1111-1111-1111-111111111112") });
            builder.Entity<UserRole>().HasData(new UserRole { CreatedAt = now, Id = Guid.Parse("11111111-1111-1111-1111-111111137412"), RoleId = Roles.User, UserId = Guid.Parse("11111111-1111-1111-1111-111111111112") });



            User user3 = new User
            {
                //Mobile = "+989365857579",
                Name = "Milad",
                FamilyName = "Jalali",
                //Email = "miladjalali.dev@gmail.com",
                Id = Guid.Parse("11111111-1111-1111-1111-111111111113"),
                ProfileImageId = Guid.Parse("11111111-1111-1111-1111-111111111168"),
                Status = UserStatus.Active,
            };
            user3.SetPassword(pass, new Encrypter());
            builder.Entity<User>(p =>
            {

                p.HasData(user3);

                p.OwnsOne(p => p.Email).HasData(new { isConfirmed = true, Address = "miladjalali.dev@gmail.com", UserId = Guid.Parse("11111111-1111-1111-1111-111111111113"), });
                p.OwnsOne(p => p.Mobile).HasData(new { FullNumber = "989365857579", CountryCode = "98", Number = "9365857579", UserId = Guid.Parse("11111111-1111-1111-1111-111111111113"), });

            });


            builder.Entity<UserRole>().HasData(new UserRole { CreatedAt = now, Id = Guid.Parse("11111111-1111-1111-1111-111111147113"), RoleId = Roles.Admin, UserId = Guid.Parse("11111111-1111-1111-1111-111111111113") });
            builder.Entity<UserRole>().HasData(new UserRole { CreatedAt = now, Id = Guid.Parse("11111111-1111-1111-1111-111111157213"), RoleId = Roles.ClinicManager, UserId = Guid.Parse("11111111-1111-1111-1111-111111111113") });
            builder.Entity<UserRole>().HasData(new UserRole { CreatedAt = now, Id = Guid.Parse("11111111-1111-1111-1111-111111167313"), RoleId = Roles.Employee, UserId = Guid.Parse("11111111-1111-1111-1111-111111111113") });
            builder.Entity<UserRole>().HasData(new UserRole { CreatedAt = now, Id = Guid.Parse("11111111-1111-1111-1111-111111177413"), RoleId = Roles.User, UserId = Guid.Parse("11111111-1111-1111-1111-111111111113") });


            User user4 = new User
            {
                // Mobile = "+989131234567",
                Name = "Mohammad",
                FamilyName = "Sabet Nejad",
                // Email = "msabetnejad@gmail.com",
                Id = Guid.Parse("11111111-1111-1111-1111-111111111114"),
                Status = UserStatus.Active,
            };
            user4.SetPassword(pass, new Encrypter());
            builder.Entity<User>(p =>
            {

                p.HasData(user4);

                p.OwnsOne(p => p.Email).HasData(new { isConfirmed = true, Address = "msabetnejad@gmail.com", UserId = Guid.Parse("11111111-1111-1111-1111-111111111114"), });
                p.OwnsOne(p => p.Mobile).HasData(new { FullNumber = "989131234567", CountryCode = "98", Number = "9131234567", UserId = Guid.Parse("11111111-1111-1111-1111-111111111114"), });

            });
            builder.Entity<UserRole>().HasData(new UserRole { CreatedAt = now, Id = Guid.Parse("11111111-1111-1111-1111-111111187114"), RoleId = Roles.Admin, UserId = Guid.Parse("11111111-1111-1111-1111-111111111114") });
            builder.Entity<UserRole>().HasData(new UserRole { CreatedAt = now, Id = Guid.Parse("11111111-1111-1111-1111-111111197214"), RoleId = Roles.ClinicManager, UserId = Guid.Parse("11111111-1111-1111-1111-111111111114") });
            builder.Entity<UserRole>().HasData(new UserRole { CreatedAt = now, Id = Guid.Parse("11111111-1111-1111-1111-111111207314"), RoleId = Roles.Employee, UserId = Guid.Parse("11111111-1111-1111-1111-111111111114") });
            builder.Entity<UserRole>().HasData(new UserRole { CreatedAt = now, Id = Guid.Parse("11111111-1111-1111-1111-111111217414"), RoleId = Roles.User, UserId = Guid.Parse("11111111-1111-1111-1111-111111111114") });

            User user5 = new User
            {
                //Mobile = "+989136843492",
                Name = "Mohammad",
                FamilyName = "Najafi",
                //Email = "mohammadnjf950@gmail.com",
                Id = Guid.Parse("11111111-1111-1111-1111-111111111115"),
                Status = UserStatus.Active,
            };
            user5.SetPassword(pass, new Encrypter());
            builder.Entity<User>(p =>
            {

                p.HasData(user5);
                p.OwnsOne(p => p.Email).HasData(new { isConfirmed = true, Address = "mohammadnjf950@gmail.com", UserId = Guid.Parse("11111111-1111-1111-1111-111111111115"), });
                p.OwnsOne(p => p.Mobile).HasData(new { FullNumber = "989136843492", CountryCode = "98", Number = "9136843492", UserId = Guid.Parse("11111111-1111-1111-1111-111111111115"), });

            });


            builder.Entity<UserRole>().HasData(new UserRole { CreatedAt = now, Id = Guid.Parse("11111111-1111-1111-1111-111111147115"), RoleId = Roles.Admin, UserId = Guid.Parse("11111111-1111-1111-1111-111111111115") });
            builder.Entity<UserRole>().HasData(new UserRole { CreatedAt = now, Id = Guid.Parse("11111111-1111-1111-1111-111111157215"), RoleId = Roles.ClinicManager, UserId = Guid.Parse("11111111-1111-1111-1111-111111111115") });
            builder.Entity<UserRole>().HasData(new UserRole { CreatedAt = now, Id = Guid.Parse("11111111-1111-1111-1111-111111167315"), RoleId = Roles.Employee, UserId = Guid.Parse("11111111-1111-1111-1111-111111111115") });
            builder.Entity<UserRole>().HasData(new UserRole { CreatedAt = now, Id = Guid.Parse("11111111-1111-1111-1111-111111177415"), RoleId = Roles.User, UserId = Guid.Parse("11111111-1111-1111-1111-111111111115") });





            builder.Entity<User>().HasData(new User { Id = Guid.Parse("11111111-1111-1111-1111-111111111120"), ProfileImageId = Guid.Parse("11111111-1111-1111-1111-111111111131"), Name = "customer", FamilyName = "one",/* Email = "Customer1@gmail.com",*/ Status = UserStatus.Active });
            builder.Entity<UserRole>().HasData(new UserRole { Id = Guid.Parse("11111111-1111-1111-1111-111111111120"), UserId = Guid.Parse("11111111-1111-1111-1111-111111111120"), RoleId = Roles.User });

            builder.Entity<User>().HasData(new User { Id = Guid.Parse("11111111-1111-1111-1111-111111111121"), ProfileImageId = Guid.Parse("11111111-1111-1111-1111-111111111132"), Name = "customer", FamilyName = "two",/* Email = "Customer2@gmail.com",*/ Status = UserStatus.Active });
            builder.Entity<UserRole>().HasData(new UserRole { Id = Guid.Parse("11111111-1111-1111-1111-111111111121"), UserId = Guid.Parse("11111111-1111-1111-1111-111111111121"), RoleId = Roles.User });

            builder.Entity<User>().HasData(new User { Id = Guid.Parse("11111111-1111-1111-1111-111111111122"), ProfileImageId = Guid.Parse("11111111-1111-1111-1111-111111111133"), Name = "customer", FamilyName = "three",/* Email = "Customer3@gmail.com",*/ Status = UserStatus.Active });
            builder.Entity<UserRole>().HasData(new UserRole { Id = Guid.Parse("11111111-1111-1111-1111-111111111122"), UserId = Guid.Parse("11111111-1111-1111-1111-111111111122"), RoleId = Roles.User });

            builder.Entity<User>().HasData(new User { Id = Guid.Parse("11111111-1111-1111-1111-111111111123"), ProfileImageId = Guid.Parse("11111111-1111-1111-1111-111111111134"), Name = "customer", FamilyName = "four",/* Email = "Customer4@gmail.com",*/ Status = UserStatus.Active });
            builder.Entity<UserRole>().HasData(new UserRole { Id = Guid.Parse("11111111-1111-1111-1111-111111111123"), UserId = Guid.Parse("11111111-1111-1111-1111-111111111123"), RoleId = Roles.User });

            builder.Entity<User>().HasData(new User { Id = Guid.Parse("11111111-1111-1111-1111-111111111124"), ProfileImageId = Guid.Parse("11111111-1111-1111-1111-111111111135"), Name = "customer", FamilyName = "five",/*/ Email = "Customer5@gmail.com",/*/ Status = UserStatus.Active });
            builder.Entity<UserRole>().HasData(new UserRole { Id = Guid.Parse("11111111-1111-1111-1111-111111111124"), UserId = Guid.Parse("11111111-1111-1111-1111-111111111124"), RoleId = Roles.User });

            builder.Entity<User>().HasData(new User { Id = Guid.Parse("11111111-1111-1111-1111-111111111125"), ProfileImageId = Guid.Parse("11111111-1111-1111-1111-111111111136"), Name = "customer", FamilyName = "six",/*/ Email = "Customer6@gmail.com",/*/ Status = UserStatus.Active });
            builder.Entity<UserRole>().HasData(new UserRole { Id = Guid.Parse("11111111-1111-1111-1111-111111111125"), UserId = Guid.Parse("11111111-1111-1111-1111-111111111125"), RoleId = Roles.User });

            builder.Entity<User>().HasData(new User { Id = Guid.Parse("11111111-1111-1111-1111-111111111126"), ProfileImageId = Guid.Parse("11111111-1111-1111-1111-111111111137"), Name = "customer", FamilyName = "seven",/*/ Email = "Customer7@gmail.com",/*/ Status = UserStatus.Active });
            builder.Entity<UserRole>().HasData(new UserRole { Id = Guid.Parse("11111111-1111-1111-1111-111111111126"), UserId = Guid.Parse("11111111-1111-1111-1111-111111111126"), RoleId = Roles.User });

            builder.Entity<User>().HasData(new User { Id = Guid.Parse("11111111-1111-1111-1111-111111111127"), ProfileImageId = Guid.Parse("11111111-1111-1111-1111-111111111138"), Name = "customer", FamilyName = "eight",/*/ Email = "Customer8@gmail.com",/*/ Status = UserStatus.Active });
            builder.Entity<UserRole>().HasData(new UserRole { Id = Guid.Parse("11111111-1111-1111-1111-111111111127"), UserId = Guid.Parse("11111111-1111-1111-1111-111111111127"), RoleId = Roles.User });

            builder.Entity<User>().HasData(new User { Id = Guid.Parse("11111111-1111-1111-1111-111111111128"), ProfileImageId = Guid.Parse("11111111-1111-1111-1111-111111111139"), Name = "customer", FamilyName = "nine", /*/Email = "Customer9@gmail.com",/*/ Status = UserStatus.Active });
            builder.Entity<UserRole>().HasData(new UserRole { Id = Guid.Parse("11111111-1111-1111-1111-111111111128"), UserId = Guid.Parse("11111111-1111-1111-1111-111111111128"), RoleId = Roles.User });

            builder.Entity<User>().HasData(new User { Id = Guid.Parse("11111111-1111-1111-1111-111111111129"), ProfileImageId = Guid.Parse("11111111-1111-1111-1111-111111111140"), Name = "customer", FamilyName = "ten",/*/ Email = "Customer10@gmail.com",/*/ Status = UserStatus.Active });
            builder.Entity<UserRole>().HasData(new UserRole { Id = Guid.Parse("11111111-1111-1111-1111-111111111129"), UserId = Guid.Parse("11111111-1111-1111-1111-111111111129"), RoleId = Roles.User });

            builder.Entity<User>().HasData(new User { Id = Guid.Parse("11111111-1111-1111-1111-111111111130"), ProfileImageId = Guid.Parse("11111111-1111-1111-1111-111111111141"), Name = "customer", FamilyName = "eleven",/*/ Email = "Customer11@gmail.com",/*/ Status = UserStatus.Active });
            builder.Entity<UserRole>().HasData(new UserRole { Id = Guid.Parse("11111111-1111-1111-1111-111111111130"), UserId = Guid.Parse("11111111-1111-1111-1111-111111111130"), RoleId = Roles.User });

            builder.Entity<User>().HasData(new User { Id = Guid.Parse("11111111-1111-1111-1111-111111111131"), ProfileImageId = Guid.Parse("11111111-1111-1111-1111-111111111142"), Name = "customer", FamilyName = "twelve",/* Email = "Customer12@gmail.com", */Status = UserStatus.Active });
            builder.Entity<UserRole>().HasData(new UserRole { Id = Guid.Parse("11111111-1111-1111-1111-111111111131"), UserId = Guid.Parse("11111111-1111-1111-1111-111111111131"), RoleId = Roles.User });



            builder.Entity<Document>().HasData(new Document { CreatedAt = now, DocumentType = DocumentType.Image, Url = "/Documents/11111111-1111-1111-1111-111111111111.jpg", Id = Guid.Parse("11111111-1111-1111-1111-111111111111") });
            builder.Entity<Document>().HasData(new Document { CreatedAt = now, DocumentType = DocumentType.Image, Url = "/Documents/11111111-1111-1111-1111-111111111112.jpg", Id = Guid.Parse("11111111-1111-1111-1111-111111111112") });
            builder.Entity<Document>().HasData(new Document { CreatedAt = now, DocumentType = DocumentType.Image, Url = "/Documents/11111111-1111-1111-1111-111111111113.jpg", Id = Guid.Parse("11111111-1111-1111-1111-111111111113") });
            builder.Entity<Document>().HasData(new Document { CreatedAt = now, DocumentType = DocumentType.Image, Url = "/Documents/11111111-1111-1111-1111-111111111114.jpg", Id = Guid.Parse("11111111-1111-1111-1111-111111111114") });
            builder.Entity<Document>().HasData(new Document { CreatedAt = now, DocumentType = DocumentType.Image, Url = "/Documents/11111111-1111-1111-1111-111111111115.jpg", Id = Guid.Parse("11111111-1111-1111-1111-111111111115") });
            builder.Entity<Document>().HasData(new Document { CreatedAt = now, DocumentType = DocumentType.Image, Url = "/Documents/11111111-1111-1111-1111-111111111116.jpg", Id = Guid.Parse("11111111-1111-1111-1111-111111111116") });
            builder.Entity<Document>().HasData(new Document { CreatedAt = now, DocumentType = DocumentType.Image, Url = "/Documents/11111111-1111-1111-1111-111111111117.jpg", Id = Guid.Parse("11111111-1111-1111-1111-111111111117") });
            builder.Entity<Document>().HasData(new Document { CreatedAt = now, DocumentType = DocumentType.Image, Url = "/Documents/11111111-1111-1111-1111-111111111118.jpg", Id = Guid.Parse("11111111-1111-1111-1111-111111111118") });
            builder.Entity<Document>().HasData(new Document { CreatedAt = now, DocumentType = DocumentType.Image, Url = "/Documents/11111111-1111-1111-1111-111111111119.jpg", Id = Guid.Parse("11111111-1111-1111-1111-111111111119") });
            builder.Entity<Document>().HasData(new Document { CreatedAt = now, DocumentType = DocumentType.Image, Url = "/Documents/11111111-1111-1111-1111-111111111120.jpg", Id = Guid.Parse("11111111-1111-1111-1111-111111111120") });
            builder.Entity<Document>().HasData(new Document { CreatedAt = now, DocumentType = DocumentType.Image, Url = "/Documents/11111111-1111-1111-1111-111111111121.jpg", Id = Guid.Parse("11111111-1111-1111-1111-111111111121") });
            builder.Entity<Document>().HasData(new Document { CreatedAt = now, DocumentType = DocumentType.Image, Url = "/Documents/11111111-1111-1111-1111-111111111122.jpg", Id = Guid.Parse("11111111-1111-1111-1111-111111111122") });
            builder.Entity<Document>().HasData(new Document { CreatedAt = now, DocumentType = DocumentType.Image, Url = "/Documents/11111111-1111-1111-1111-111111111123.jpg", Id = Guid.Parse("11111111-1111-1111-1111-111111111123") });
            builder.Entity<Document>().HasData(new Document { CreatedAt = now, DocumentType = DocumentType.Image, Url = "/Documents/11111111-1111-1111-1111-111111111124.jpg", Id = Guid.Parse("11111111-1111-1111-1111-111111111124") });
            builder.Entity<Document>().HasData(new Document { CreatedAt = now, DocumentType = DocumentType.Image, Url = "/Documents/11111111-1111-1111-1111-111111111125.jpg", Id = Guid.Parse("11111111-1111-1111-1111-111111111125") });
            builder.Entity<Document>().HasData(new Document { CreatedAt = now, DocumentType = DocumentType.Image, Url = "/Documents/11111111-1111-1111-1111-111111111126.jpg", Id = Guid.Parse("11111111-1111-1111-1111-111111111126") });


            builder.Entity<Document>().HasData(new Document { CreatedAt = now, DocumentType = DocumentType.Image, Url = "/Documents/11111111-1111-1111-1111-111111111130.jpg", Id = Guid.Parse("11111111-1111-1111-1111-111111111130") });
            builder.Entity<Document>().HasData(new Document { CreatedAt = now, DocumentType = DocumentType.Image, Url = "/Documents/11111111-1111-1111-1111-111111111131.jpg", Id = Guid.Parse("11111111-1111-1111-1111-111111111131") });
            builder.Entity<Document>().HasData(new Document { CreatedAt = now, DocumentType = DocumentType.Image, Url = "/Documents/11111111-1111-1111-1111-111111111132.jpg", Id = Guid.Parse("11111111-1111-1111-1111-111111111132") });
            builder.Entity<Document>().HasData(new Document { CreatedAt = now, DocumentType = DocumentType.Image, Url = "/Documents/11111111-1111-1111-1111-111111111133.jpg", Id = Guid.Parse("11111111-1111-1111-1111-111111111133") });
            builder.Entity<Document>().HasData(new Document { CreatedAt = now, DocumentType = DocumentType.Image, Url = "/Documents/11111111-1111-1111-1111-111111111134.jpg", Id = Guid.Parse("11111111-1111-1111-1111-111111111134") });
            builder.Entity<Document>().HasData(new Document { CreatedAt = now, DocumentType = DocumentType.Image, Url = "/Documents/11111111-1111-1111-1111-111111111135.jpg", Id = Guid.Parse("11111111-1111-1111-1111-111111111135") });
            builder.Entity<Document>().HasData(new Document { CreatedAt = now, DocumentType = DocumentType.Image, Url = "/Documents/11111111-1111-1111-1111-111111111136.jpg", Id = Guid.Parse("11111111-1111-1111-1111-111111111136") });
            builder.Entity<Document>().HasData(new Document { CreatedAt = now, DocumentType = DocumentType.Image, Url = "/Documents/11111111-1111-1111-1111-111111111137.jpg", Id = Guid.Parse("11111111-1111-1111-1111-111111111137") });
            builder.Entity<Document>().HasData(new Document { CreatedAt = now, DocumentType = DocumentType.Image, Url = "/Documents/11111111-1111-1111-1111-111111111138.jpg", Id = Guid.Parse("11111111-1111-1111-1111-111111111138") });
            builder.Entity<Document>().HasData(new Document { CreatedAt = now, DocumentType = DocumentType.Image, Url = "/Documents/11111111-1111-1111-1111-111111111139.jpg", Id = Guid.Parse("11111111-1111-1111-1111-111111111139") });
            builder.Entity<Document>().HasData(new Document { CreatedAt = now, DocumentType = DocumentType.Image, Url = "/Documents/11111111-1111-1111-1111-111111111140.jpg", Id = Guid.Parse("11111111-1111-1111-1111-111111111140") });
            builder.Entity<Document>().HasData(new Document { CreatedAt = now, DocumentType = DocumentType.Image, Url = "/Documents/11111111-1111-1111-1111-111111111141.jpg", Id = Guid.Parse("11111111-1111-1111-1111-111111111141") });
            builder.Entity<Document>().HasData(new Document { CreatedAt = now, DocumentType = DocumentType.Image, Url = "/Documents/11111111-1111-1111-1111-111111111142.jpg", Id = Guid.Parse("11111111-1111-1111-1111-111111111142") });

            builder.Entity<Document>().HasData(new Document { CreatedAt = now, DocumentType = DocumentType.Image, Url = "/Documents/11111111-1111-1111-1111-111111111150.svg", Id = Guid.Parse("11111111-1111-1111-1111-111111111150") });
            builder.Entity<Document>().HasData(new Document { CreatedAt = now, DocumentType = DocumentType.Image, Url = "/Documents/11111111-1111-1111-1111-111111111151.svg", Id = Guid.Parse("11111111-1111-1111-1111-111111111151") });
            builder.Entity<Document>().HasData(new Document { CreatedAt = now, DocumentType = DocumentType.Image, Url = "/Documents/11111111-1111-1111-1111-111111111152.svg", Id = Guid.Parse("11111111-1111-1111-1111-111111111152") });
            builder.Entity<Document>().HasData(new Document { CreatedAt = now, DocumentType = DocumentType.Image, Url = "/Documents/11111111-1111-1111-1111-111111111153.svg", Id = Guid.Parse("11111111-1111-1111-1111-111111111153") });
            builder.Entity<Document>().HasData(new Document { CreatedAt = now, DocumentType = DocumentType.Image, Url = "/Documents/11111111-1111-1111-1111-111111111154.svg", Id = Guid.Parse("11111111-1111-1111-1111-111111111154") });
            builder.Entity<Document>().HasData(new Document { CreatedAt = now, DocumentType = DocumentType.Image, Url = "/Documents/11111111-1111-1111-1111-111111111155.svg", Id = Guid.Parse("11111111-1111-1111-1111-111111111155") });
            builder.Entity<Document>().HasData(new Document { CreatedAt = now, DocumentType = DocumentType.Image, Url = "/Documents/11111111-1111-1111-1111-111111111156.jpg", Id = Guid.Parse("11111111-1111-1111-1111-111111111156") });
            builder.Entity<Document>().HasData(new Document { CreatedAt = now, DocumentType = DocumentType.Image, Url = "/Documents/11111111-1111-1111-1111-111111111157.jpg", Id = Guid.Parse("11111111-1111-1111-1111-111111111157") });
            builder.Entity<Document>().HasData(new Document { CreatedAt = now, DocumentType = DocumentType.Image, Url = "/Documents/11111111-1111-1111-1111-111111111158.jpg", Id = Guid.Parse("11111111-1111-1111-1111-111111111158") });
            builder.Entity<Document>().HasData(new Document { CreatedAt = now, DocumentType = DocumentType.Image, Url = "/Documents/11111111-1111-1111-1111-111111111159.jpg", Id = Guid.Parse("11111111-1111-1111-1111-111111111159") });
            builder.Entity<Document>().HasData(new Document { CreatedAt = now, DocumentType = DocumentType.Image, Url = "/Documents/11111111-1111-1111-1111-111111111160.jpg", Id = Guid.Parse("11111111-1111-1111-1111-111111111160") });
            builder.Entity<Document>().HasData(new Document { CreatedAt = now, DocumentType = DocumentType.Image, Url = "/Documents/11111111-1111-1111-1111-111111111161.jpg", Id = Guid.Parse("11111111-1111-1111-1111-111111111161") });
            builder.Entity<Document>().HasData(new Document { CreatedAt = now, DocumentType = DocumentType.Image, Url = "/Documents/11111111-1111-1111-1111-111111111162.jpg", Id = Guid.Parse("11111111-1111-1111-1111-111111111162") });
            builder.Entity<Document>().HasData(new Document { CreatedAt = now, DocumentType = DocumentType.Image, Url = "/Documents/11111111-1111-1111-1111-111111111163.jpg", Id = Guid.Parse("11111111-1111-1111-1111-111111111163") });
            builder.Entity<Document>().HasData(new Document { CreatedAt = now, DocumentType = DocumentType.Image, Url = "/Documents/11111111-1111-1111-1111-111111111164.jpg", Id = Guid.Parse("11111111-1111-1111-1111-111111111164") });
            builder.Entity<Document>().HasData(new Document { CreatedAt = now, DocumentType = DocumentType.Image, Url = "/Documents/11111111-1111-1111-1111-111111111165.jpg", Id = Guid.Parse("11111111-1111-1111-1111-111111111165") });
            builder.Entity<Document>().HasData(new Document { CreatedAt = now, DocumentType = DocumentType.Image, Url = "/Documents/11111111-1111-1111-1111-111111111166.jpg", Id = Guid.Parse("11111111-1111-1111-1111-111111111166") });
            builder.Entity<Document>().HasData(new Document { CreatedAt = now, DocumentType = DocumentType.Image, Url = "/Documents/11111111-1111-1111-1111-111111111167.jpg", Id = Guid.Parse("11111111-1111-1111-1111-111111111167") });
            builder.Entity<Document>().HasData(new Document { CreatedAt = now, DocumentType = DocumentType.Image, Url = "/Documents/11111111-1111-1111-1111-111111111168.jpg", Id = Guid.Parse("11111111-1111-1111-1111-111111111168") });




            builder.Entity<Slider>().HasData(new Slider { Id = 1, DocumentId = Guid.Parse("11111111-1111-1111-1111-111111111124"), InternalLinkId = 1, IsInside = true, });
            builder.Entity<Slider>().HasData(new Slider { Id = 2, DocumentId = Guid.Parse("11111111-1111-1111-1111-111111111111"), InternalLinkId = 1, IsInside = true, });
            builder.Entity<Slider>().HasData(new Slider { Id = 3, DocumentId = Guid.Parse("11111111-1111-1111-1111-111111111117"), InternalLinkId = 1, IsInside = true, });






        }
        public static void GetCitiesFromJson(this ModelBuilder builder)
        {
            var jsonCities = new List<JsonCity>();
            var cities = new List<City>();
            var provinces = new List<Province>();
            var jsonProvinces = new List<string>();

            try
            {
                using (StreamReader r = new StreamReader(@"Canada.json"))
                {
                    string json = r.ReadToEnd();
                    jsonCities = JsonConvert.DeserializeObject<List<JsonCity>>(json);
                }

            }
            catch (Exception e)
            {

            }
            //for (int i = 0; i < jsonCities?.Count; i++)
            for (int i = 0; i < 100; i++)
            {
                var jCity = jsonCities[i];
                if (!jsonProvinces.Contains(jCity.province_id))
                {
                    jsonProvinces.Add(jCity.province_id);
                    provinces.Add(new Province
                    {
                        Id = provinces.Count + 1,
                        CountryId = 1,
                        Name = jCity.province_name,
                        ShortName = jCity.province_id,
                    });
                }
                var proId = provinces.FirstOrDefault(p => p.ShortName == jCity.province_id).Id;
                cities.Add(new City { Id = cities.Count + 1, Lat = jCity.lat, Lng = jCity.lng, Name = jCity.city, PostalCode = jCity.postal, ProvinceId = proId });

            }

            builder.Entity<Province>().HasData(provinces);
            builder.Entity<City>().HasData(cities);


        }



        ////========================================================================================================================================================================
        //internal static readonly MethodInfo IncludeMethodInfo
        //    = typeof(EntityFrameworkQueryableExtensions)
        //        .GetTypeInfo().GetDeclaredMethods(nameof(Include))
        //        .Single(
        //            mi =>
        //                mi.GetGenericArguments().Count() == 2
        //                && mi.GetParameters().Any(
        //                    pi => pi.Name == "navigationPropertyPath" && pi.ParameterType != typeof(string)));

        //internal static readonly MethodInfo ThenIncludeAfterReferenceMethodInfo
        //    = typeof(EntityFrameworkQueryableExtensions)
        //        .GetTypeInfo().GetDeclaredMethods(nameof(ThenInclude))
        //        .Single(
        //            mi => mi.GetGenericArguments().Count() == 3
        //                  && mi.GetParameters()[0].ParameterType.GenericTypeArguments[1].IsGenericParameter);

        //internal static readonly MethodInfo ThenIncludeAfterEnumerableMethodInfo
        //    = GetThenIncludeMethodInfo(typeof(IEnumerable<>));

        //private static MethodInfo GetThenIncludeMethodInfo(Type navType)
        //    => typeof(EntityFrameworkQueryableExtensions)
        //        .GetTypeInfo().GetDeclaredMethods(nameof(ThenInclude))
        //        .Where(mi => mi.GetGenericArguments().Count() == 3)
        //        .Single(
        //            mi =>
        //            {
        //                var typeInfo = mi.GetParameters()[0].ParameterType.GenericTypeArguments[1].GetTypeInfo();
        //                return typeInfo.IsGenericType
        //                       && typeInfo.GetGenericTypeDefinition() == navType;
        //            });

        //public static IIncludableQueryable<TEntity, TProperty> Include<TEntity, TProperty>(
        //    [NotNull] this IQueryable<TEntity> source,
        //    [NotNull] Expression<Func<TEntity, TProperty>> navigationPropertyPath,
        //    bool shouldInclude)
        //    where TEntity : class
        //{
        //    if (shouldInclude)
        //    {
        //        return new IncludableQueryable<TEntity, TProperty>(
        //            source.Provider is EntityQueryProvider
        //                ? source.Provider.CreateQuery<TEntity>(
        //                    Expression.Call(
        //                        instance: null,
        //                        method: IncludeMethodInfo.MakeGenericMethod(typeof(TEntity), typeof(TProperty)),
        //                        arguments: new[] { source.Expression, Expression.Quote(navigationPropertyPath) }))
        //                : source);
        //    }
        //    else
        //    {
        //        return new IncludableQueryable<TEntity, TProperty>(source);
        //    }
        //}


        //public static IIncludableQueryable<TEntity, TProperty> ThenInclude<TEntity, TPreviousProperty, TProperty>(
        //    [NotNull] this IIncludableQueryable<TEntity, TPreviousProperty> source,
        //    [NotNull] Expression<Func<TPreviousProperty, TProperty>> navigationPropertyPath,
        //    bool shouldInclude)
        //    where TEntity : class
        //{
        //    if (shouldInclude)
        //    {
        //        return new IncludableQueryable<TEntity, TProperty>(
        //            source.Provider is EntityQueryProvider
        //                ? source.Provider.CreateQuery<TEntity>(
        //                    Expression.Call(
        //                        instance: null,
        //                        method: ThenIncludeAfterReferenceMethodInfo.MakeGenericMethod(
        //                            typeof(TEntity), typeof(TPreviousProperty), typeof(TProperty)),
        //                        arguments: new[] { source.Expression, Expression.Quote(navigationPropertyPath) }))
        //                : source);
        //    }
        //    return new IncludableQueryable<TEntity, TProperty>(source);
        //}

        //public static IIncludableQueryable<TEntity, TProperty> ThenInclude<TEntity, TPreviousProperty, TProperty>(
        //    [NotNull] this IIncludableQueryable<TEntity, IEnumerable<TPreviousProperty>> source,
        //    [NotNull] Expression<Func<TPreviousProperty, TProperty>> navigationPropertyPath,
        //    bool shouldInclude)
        //    where TEntity : class
        //{
        //    if (shouldInclude)
        //    {
        //        return new IncludableQueryable<TEntity, TProperty>(
        //            source.Provider is EntityQueryProvider
        //                ? source.Provider.CreateQuery<TEntity>(
        //                    Expression.Call(
        //                        instance: null,
        //                        method: ThenIncludeAfterEnumerableMethodInfo.MakeGenericMethod(
        //                            typeof(TEntity), typeof(TPreviousProperty), typeof(TProperty)),
        //                        arguments: new[] { source.Expression, Expression.Quote(navigationPropertyPath) }))
        //                : source);
        //    }

        //    return new IncludableQueryable<TEntity, TProperty>(source);
        //}

        //private class IncludableQueryable<TEntity, TProperty> : IIncludableQueryable<TEntity, TProperty>, IAsyncEnumerable<TEntity>
        //{
        //    private readonly IQueryable<TEntity> _queryable;

        //    public IncludableQueryable(IQueryable<TEntity> queryable)
        //    {
        //        _queryable = queryable;
        //    }

        //    public Expression Expression => _queryable.Expression;
        //    public Type ElementType => _queryable.ElementType;
        //    public IQueryProvider Provider => _queryable.Provider;

        //    public IAsyncEnumerator<TEntity> GetAsyncEnumerator(CancellationToken cancellationToken = default)
        //        => ((IAsyncEnumerable<TEntity>)_queryable).GetAsyncEnumerator(cancellationToken);

        //    public IEnumerator<TEntity> GetEnumerator() => _queryable.GetEnumerator();

        //    IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        //}




    }
    public class JsonCity
    {
        public string city { get; set; }
        public string city_ascii { get; set; }
        public string province_id { get; set; }
        public string province_name { get; set; }
        public double lat { get; set; }
        public double lng { get; set; }
        public long population { get; set; }
        public string density { get; set; }
        public string timezone { get; set; }
        public string ranking { get; set; }
        public string postal { get; set; }
        public int id { get; set; }
    }

    public class CitiesFromJson
    {
        public List<JsonCity> Cities { get; set; }
    }
}



