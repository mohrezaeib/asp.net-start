﻿using DomainCore.DTO.Base;
using DomainCore.Entities.AppMore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DomainData.Contracts
{
    public interface IAnnouncementRepository : IRepository<Announcement>
    {
        IQueryable<Announcement> GetByFilter(BaseGetByPageDto dto);  
        List<Announcement> GetActive();
    }
}
