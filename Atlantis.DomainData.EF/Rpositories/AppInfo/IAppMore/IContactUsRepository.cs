﻿using DomainCore.DTO.AppMore;
using DomainCore.Entities.AppMore;
using System.Linq;

namespace DomainData.Contracts
{
    public interface IContactUsRepository : IRepository<ContactUs>
    {
        IQueryable<ContactUs> GetByFilter(GetContactUsByFilterDto dto);
    } 
}
