﻿using DomainCore.Entities.AppMore;

namespace DomainData.Contracts
{
    public interface ISocialRepository : IRepository<Social>
    {
      
    }
}
