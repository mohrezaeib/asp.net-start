﻿using DomainCore.DTO.Base;
using DomainCore.Entities.AppMore;
using DomainData.Contracts;
using DomainData.Ef.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DomainData.Ef.Repositories
{
    public class AnnouncementRepository : Repository<Announcement>, IAnnouncementRepository
    {
        private readonly IContext ctx;

        public AnnouncementRepository(IContext ctx) : base(ctx)
        {
            this.ctx = ctx;
            this.baseInclude = this.ctx.Announcements;
        }

        public List<Announcement> GetActive()
        {
            return baseInclude.OrderBy(p => p.Start)
                .OrderBy(p => p.CreatedAtDate)
                .Where(p=> 
                p.Start < DateTime.Now &&
                p.End > DateTime.Now
                ).ToList();
        }

        public IQueryable<Announcement> GetByFilter(BaseGetByPageDto dto)
        {
            return baseInclude.OrderBy(p=> p.CreatedAtDate).AsQueryable();
        }
    } 
}
