﻿using DomainCore.Entities.General;
using DomainCore.General;
using System;
using System.Collections.Generic;
using System.Text;

namespace DomainData.Contracts
{
    public interface ISliderRepository : IRepository<Slider>
    {
    }
}
