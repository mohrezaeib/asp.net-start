﻿
using DomainCore.Dto.General;
using DomainCore.General;
using System;
using System.Collections.Generic;
using System.Text;

namespace DomainData.Contracts
{
    public interface IActiveCodeRepository : IRepository<ActiveCode>
    {
         
        bool CheckExeedMobile(string mobile);
        bool CheckExeedEmail(string email);
        bool CheckActiveCode(CheckActiveCodeDto dto);
        bool CheckActiveCodeByMobile(string mobile, string code);
        bool CheckActiveCodeByEmail(string email, string code);
    }

}
