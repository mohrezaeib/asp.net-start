﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

using System.Linq;
using Utility.Tools.General;
using Core.Entities;
using DomainData.Contracts;
using DomainCore.General;
using DomainData.Ef.Context;
using DomainCore.Dto.General;
using System.Linq.Expressions;
using DomainCore.AppUsers;

namespace DomainData.Ef.Repositories
{
    public class ActiveCodeRepository : Repository<ActiveCode>, IActiveCodeRepository
    {
        private readonly IContext ctx;

        public ActiveCodeRepository(IContext ctx) : base(ctx )
        {
            this.ctx = ctx;
        }

       

        public bool CheckExeedMobile(string mobile)
        {
            long date1 = UnixTime.ToUnix(DateTime.Now.AddMinutes(-15));
            var Mobile = new Mobile(mobile);

            // var date = DateTime.Now.AddMinutes(-15).ToUnix();
            return ctx.ActiveCodes.Where(p => p.Mobile.FullNumber == Mobile.FullNumber && p.CreatedAt > date1 ).ToList().Count() >= 5;
        }
        public bool CheckExeedEmail(string email)
        {

            long date1 = UnixTime.ToUnix(DateTime.Now.AddMinutes(-15));
            var Email = new Email(email);

            // var date = DateTime.Now.AddMinutes(-15).ToUnix();
            return ctx.ActiveCodes.Where(p => p.Email.Address ==Email.Address && p.CreatedAt > date1).ToList().Count() >= 5;

        }

        public bool CheckActiveCode(CheckActiveCodeDto dto)
        {
            long date1 = UnixTime.ToUnix(DateTime.Now.AddMinutes(-15));
            var mobile = new Mobile(dto.Mobile);
            // my logic : return ctx.ActiveCodes.Any(p => p.Code == dto.ActiveCode && p.Mobile == dto.Mobile && p.CreatedAt > date1);
            var result = ctx.ActiveCodes.Where(p => p.Mobile.FullNumber == mobile.FullNumber && p.CreatedAt > date1).OrderByDescending(p => p.CreatedAt).FirstOrDefault();
            return result != null && result.Code == dto.Code;
        }
        public bool CheckActiveCodeByMobile(string mobile, string code)
        {
            long date1 = UnixTime.ToUnix(DateTime.Now.AddMinutes(-15));
            var Mobile = new Mobile(mobile);
            // my logic : return ctx.ActiveCodes.Any(p => p.Code == dto.ActiveCode && p.Mobile == dto.Mobile && p.CreatedAt > date1);
            var result = ctx.ActiveCodes.Where(p => p.Mobile.FullNumber == Mobile.FullNumber && p.CreatedAt > date1).OrderByDescending(p => p.CreatedAt).FirstOrDefault();
            return result != null && result.Code == code;
        }
        public bool CheckActiveCodeByEmail(string email , string code)
        {
            long date1 = UnixTime.ToUnix(DateTime.Now.AddMinutes(-30));
            var Email = new Email(email);
            // my logic : return ctx.ActiveCodes.Any(p => p.Code == dto.ActiveCode && p.Mobile == dto.Mobile && p.CreatedAt > date1);
            var result = ctx.ActiveCodes.Where(p => p.Email.Address == Email.Address && p.CreatedAt > date1).OrderByDescending(p => p.CreatedAt).FirstOrDefault();
            return result != null && result.Code == code;
        }

    }
}
