﻿using DomainCore.DTO.SupportChat;
using DomainCore.Entities.AppMore;
using DomainCore.Entities.SupportChat;
using DomainCore.Enums;
using DomainData.Contracts;
using DomainData.Ef.Context;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace DomainData.Ef.Repositories
{
    public class ChatRepository : Repository<Chat>, IChatRepository
    {
        private readonly IContext ctx;

        public ChatRepository(IContext ctx) : base(ctx)
        {
            this.ctx = ctx;
            baseInclude = this.ctx.Chats
                .Include(p => p.Customer)
                .Include(p => p.Messages).ThenInclude(p => p.Sender)
                .Include(p => p.Messages).ThenInclude(p => p.Document)
                                .OrderByDescending(p => p.CreatedAt)
;

        }
        public IQueryable<Chat> GetByFilter(GetChatByFilterDto dto)
        {
            return baseInclude.Where(p => (dto.ChatId == null || dto.ChatId == p.Id) &&
           
(dto.MessageDate == null || 
                                    p.Messages.Any(x => x.CreatedAtDate.Date == dto.MessageDate.Value.Date)
                                    )
                                    );
        }


    }  
}
