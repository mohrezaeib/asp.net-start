﻿using DomainCore.Entities.SupportChat;

namespace DomainData.Contracts
{
    public interface IMessageRepository : IRepository<Message>
    {
       
    } 

}
