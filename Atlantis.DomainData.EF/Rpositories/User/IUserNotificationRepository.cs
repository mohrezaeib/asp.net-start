﻿
using DomainCore.AppUsers;
using DomainCore.Dto.User;
using System.Collections.Generic;
using System.Linq;

namespace DomainData.Contracts
{
    public interface IUserNotificationRepository : IRepository<UserNotification>
    {
        IQueryable<UserNotification> GetByFilter(GetUserNotificationsByFilterDto dto);
    }

}
