﻿using DomainCore.AppUsers;
using DomainCore.Dto.Base;
using DomainCore.Dto.User;
using DomainCore.General;
using DomainData.Contracts;
using DomainData.Ef.Context;
using DomainData.Ef.Extentions;
using MD.PersianDateTime.Standard;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Utility.Tools.General;

namespace DomainData.Ef.Repositories
{
    public partial class UserRepository : Repository<User>, IUserRepository
    {
        private readonly IContext ctx;

        public UserRepository(IContext ctx) : base(ctx)
        {
            this.ctx = ctx;
            baseInclude = ctx.AppUsers
                .Include(p => p.Province)
                .Include(p => p.UserRole).ThenInclude(p => p.Role)
                .Include(p => p.UserRole)
                .Include(p => p.ProfileImage)
                .Include(p => p.UserSetting)
                .Include(p => p.Notification)
                .Include(p => p.Device)
                .Include(p => p.Documents).ThenInclude(p=> p.Document);


        }


        public async Task<User> GetByMobileAsync(string mobile)
        {
            var Mobile = new Mobile(mobile);
            return await baseInclude
                .Where(p => p.Mobile.FullNumber== Mobile.FullNumber).FirstOrDefaultAsync();
        }
         public async Task<User> GetUserWithPointsAsync(Guid UserId)
        {
           
            return await baseInclude
                                .Include(p => p.ProfileImage)

                .FirstOrDefaultAsync(p => p.Id == UserId);
        }

        public async Task<User> GetDetailAsync(Guid Id)
        {
            return await baseInclude
                            .Where(p => p.Id == Id).FirstOrDefaultAsync();
        }
        public IQueryable<User> GetByFilter(GetUsersByFilterDto dto)
        {
           
            var querry =  baseInclude.Where(p =>
                            (dto.ProvinceId == null || dto.ProvinceId == p.ProvinceId) &&
                            (dto.City == null || (p.City!= null && p.City.Contains(dto.City))) &&
                            (dto.Status == null  || dto.Status == p.Status) &&
                            (dto.Gender == null  || dto.Gender == p.Gender) &&
                             (dto.Date == null || dto.Date.Value.Date == p.CreatedAtDate.Date) &&
            (dto.FromDate == null || dto.FromDate.Value <= p.CreatedAtDate) &&
            (dto.ToDate == null || dto.ToDate.Value >= p.CreatedAtDate) &&    
                            (
                            dto.Role == null ||dto.Role.Count==0|| p.UserRole.Select(r=> r)
                            .Any(rp => dto.Role.Contains(rp.RoleId) )
                            ) &&
                            
                            (string.IsNullOrEmpty(dto.Keyword) || ((p.Name != null && p.Name.Contains(dto.Keyword))
                                                                  || (p.FamilyName != null && p.FamilyName.Contains(dto.Keyword))
                                                                  || (p.Email.Address != null && p.Email.Address.Contains(dto.Keyword))
                                                                  )) &&
                            (string.IsNullOrEmpty(dto.KeywordEmail) || (p.Email != null && p.Email.Address.Contains(dto.KeywordEmail)))&&
                            (string.IsNullOrEmpty(dto.KeywordName) || ((p.Name != null && p.Name.Contains(dto.KeywordName)) || (p.FamilyName != null && p.FamilyName.Contains(dto.KeywordName))))&&
                            (string.IsNullOrEmpty(dto.KeywordPhone) || (p.Mobile != null &&p.Mobile.FullNumber != null && p.Mobile.FullNumber.Contains(dto.KeywordPhone))) 
                            ).OrderBy(p=> p.CreatedAt)
                ;
           

           
            return querry;


        }

        public bool IsExist(string mobile)
        {
            var Mobile = new Mobile(mobile);
            return ctx.AppUsers.Any(p => p.Mobile.FullNumber == Mobile.FullNumber);
        }

        public async  Task<User> GetByEmailAsync(string email)
        {
            var Email = new Email(email);
            return await baseInclude.Where(p => p.Email.Address == Email.Address).FirstOrDefaultAsync();
        }

        public async Task<User> GetIncludeOrdersAsync(Guid Id)
        {
            return await ctx.AppUsers.Where(p => p.Id == Id).FirstOrDefaultAsync();
        }
        public  User GetIncludeAll(Guid? Id)
        {
            return  baseInclude.Where(p => p.Id == Id).FirstOrDefault();
        }
    }
    }

