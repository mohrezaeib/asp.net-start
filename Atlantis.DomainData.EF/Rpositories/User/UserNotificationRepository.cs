﻿using DomainCore.AppUsers;
using DomainCore.Dto.User;
using DomainData.Contracts;
using DomainData.Ef.Context;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace DomainData.Ef.Repositories
{
    public class UserNotificationRepository : Repository<UserNotification>, IUserNotificationRepository
    {
        private readonly IContext ctx;

        public UserNotificationRepository(IContext ctx) : base(ctx)
        {
            this.ctx = ctx;
            this.baseInclude = ctx.UserNotifications.Include(p=> p.User);
        }

        public IQueryable<UserNotification> GetByFilter(GetUserNotificationsByFilterDto dto)
        {
            return baseInclude.Where(p =>
                             (dto.IsSeen == null || dto.IsSeen == p.IsSeen) &&
                             (dto.UserId == null || dto.UserId == p.UserId) &&
                              (dto.Date == null || dto.Date.Value.Date == p.CreatedAtDate.Date) &&
             (dto.FromDate == null || dto.FromDate.Value <= p.CreatedAtDate) &&
             (dto.ToDate == null || dto.ToDate.Value >= p.CreatedAtDate) &&
             (string.IsNullOrEmpty(dto.Keyword) || ((p.Title != null && p.Title.Contains(dto.Keyword))
                                                                   || (p.Body != null && p.Body.Contains(dto.Keyword))))
                                                                   ); 
        }
    }  public class NotificationRepository : Repository<Notification>, INotificationRepository
    {
        private readonly IContext ctx;

        public NotificationRepository(IContext ctx) : base(ctx)
        {
            this.ctx = ctx;
            this.baseInclude = ctx.Notifications;
        }

        public IQueryable<Notification> GetByFilter(GetNotificationsByFilterDto dto)
        {
            return baseInclude.Where(p =>
                            
                              (dto.Date == null || dto.Date.Value.Date == p.CreatedAtDate.Date) &&
             (dto.FromDate == null || dto.FromDate.Value <= p.CreatedAtDate) &&
             (dto.ToDate == null || dto.ToDate.Value >= p.CreatedAtDate) &&
             (string.IsNullOrEmpty(dto.Keyword) || ((p.Title != null && p.Title.Contains(dto.Keyword))
                                                                   || (p.Body != null && p.Body.Contains(dto.Keyword))))
                                                                   ); 
        }
    } 
}
