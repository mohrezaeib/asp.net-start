﻿
using DomainCore.AppUsers;
using DomainCore.Dto.User;
using System.Linq;

namespace DomainData.Contracts
{
    public interface INotificationRepository : IRepository<Notification>
    {
        IQueryable<Notification> GetByFilter(GetNotificationsByFilterDto dto);
    }

}
