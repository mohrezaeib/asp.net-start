﻿using DomainCore.Dto.General;
using DomainCore.Entities.Services;
using DomainCore.General;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DomainData.Contracts
{
    public interface IBodyPartRepository : IRepository<BodyPart>
    {
        //Task<List<BodyPart>> GetAllWithDetailAsync();
        //Task<BodyPart> GetWithDetailAsync(Expression<Func<BodyPart, bool>> predicate);
    } 
}
