﻿using DomainCore.Dto.General;
using DomainCore.General;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DomainCore.Entities.Services;

namespace DomainData.Contracts
{
    public interface IServiceRepository : IRepository<Service>
    {
    }
    public interface IServiceImageRepository : IRepository<ServiceImage>
    {
    } 
    public interface IServiceBodyPartRepository : IRepository<ServiceBodyPart>
    {
    }
}

