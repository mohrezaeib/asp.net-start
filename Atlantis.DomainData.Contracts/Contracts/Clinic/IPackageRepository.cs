﻿using DomainCore.Entities.Packages;

namespace DomainData.Contracts
{
    public interface IPackageRepository : IRepository<Package>
    {
    }
  
    
}

