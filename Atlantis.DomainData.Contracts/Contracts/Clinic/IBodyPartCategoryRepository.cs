﻿using DomainCore.Entities.Services;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DomainData.Contracts
{
    public interface IBodyPartCategoryRepository : IRepository<BodyPartCategory>
    {
        //Task<List<BodyPartCategory>> GetAllWithDetailAsync();
        //Task<BodyPartCategory> GetWithDetailAsync(Expression<Func<BodyPartCategory, bool>> predicate);
    }
}
