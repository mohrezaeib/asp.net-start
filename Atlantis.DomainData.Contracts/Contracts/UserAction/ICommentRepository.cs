﻿using DomainCore.Dto.General;
using DomainCore.General;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DomainCore.Entities.Services;
using DomainCore.Entities.Clinics;
using DomainCore.Entities.UserActions;
using System.Linq;
using DomainCore.DTO.UserActions;

namespace DomainData.Contracts
{
    public interface ICommentRepository : IRepository<Comment>
    {
        IQueryable<Comment> GetByFilter(GetCommentByFiltrerDto dto);
    }
}

