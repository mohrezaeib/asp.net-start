﻿
using DomainCore.AppTransactions;
using System;
using System.Collections.Generic;
using System.Text;

namespace DomainData.Contracts
{
    public interface ITransactionCategoryRepository : IRepository<TransactionCategory>
    {
    }
}
