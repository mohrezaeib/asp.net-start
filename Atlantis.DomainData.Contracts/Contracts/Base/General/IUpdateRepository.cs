﻿using DomainCore.Dto.General;
using DomainCore.General;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DomainData.Contracts
{
    public interface IUpdateRepository : IRepository<Update>
    {
        Task<Update> GetUpdateAsync(GetSplashDto dto);
    }
}
