﻿using DomainCore.General;
using System;
using System.Collections.Generic;
using System.Text;

namespace DomainData.Contracts
{
    public interface IDocumentRepository : IRepository<Document>
    {
        string GetUrl(Guid documentId);
    }
}
