﻿using DomainServices.Contracts;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using AutoMapper;
using DomainServices.DtoService.Profiles;

using AutoMapper.EquivalencyExpression;
using DomainData.Ef.Context;


namespace Atlantis.DomainServices
{
    public static class Extentions
    {

        public static void AddApplicationServices(this IServiceCollection services )
        {
            var alltypes = typeof(IApplicationService).Assembly.ExportedTypes.ToList();
            var allAppInterfaces = alltypes
              .Where(x => x.IsInterface && x.IsPublic
                && typeof(IApplicationService).IsAssignableFrom(x)
              ).ToList();

            foreach (var appInterface in allAppInterfaces)
            {
                var appClass = alltypes.FirstOrDefault(x => x.IsClass &&
                x.IsPublic 
                && appInterface.IsAssignableFrom(x));
                if(appClass != null)
                services.AddScoped(appInterface, appClass);
            }
        }
        //public static void AddEventServices(this IServiceCollection services)
        //{
        //    services.AddScoped<IOrderingIntegrationEventService, OrderingIntegrationEventService>();
        //    services.AddTransient<NewOrderCreatedIntegrationEventHandler>();


        //}

        public static IServiceCollection AddAutoMapperExtention(this IServiceCollection servicess)
        {
            var services = servicess;
            //services.AddEntityFrameworkInMemoryDatabase();

            services.AddAutoMapper((srp, cfg) =>
            {
                cfg.AddCollectionMappers();

                // cfg.UseEntityFrameworkCoreModel<MainContext>(services);
            }, typeof(GeneralProfile).Assembly);
            var serviceProvider = services.BuildServiceProvider();



            //Validate configs 

            var config = new MapperConfiguration(cfg =>
                {
                    cfg.AddCollectionMappers();

                    //cfg.AddProfile(typeof(GeneralProfile)); // <- you can do this however you like
                    //cfg.AddProfile(typeof(ClinicProfile)); // <- you can do this however you like
                    //cfg.AddProfile(typeof(ServiceProfile)); // <- you can do this however you like
                    //cfg.AddProfile(typeof(BodyPartProfile)); // <- you can do this however you like
                    //cfg.AddProfile(typeof(PackageProfile)); // <- you can do this however you like
                    //cfg.AddProfile(typeof(CommentProfile)); // <- you can do this however you like
                    //cfg.AddProfile(typeof(UserFavoriteProfile)); // <- you can do this however you like
                    //cfg.AddProfile(typeof(UserProfile)); // <- you can do this however you like
                    //cfg.AllowNullDestinationValues = true;


                });
            //config.AssertConfigurationIsValid();

            return servicess;
        }


        //public static void AddEventBus(this IServiceCollection services, IConfiguration Configuration )
        //{
        //    services.AddSingleton<IRabbitMQPersistentConnection>(sp =>
        //    {
        //        var logger = sp.GetRequiredService<ILogger<DefaultRabbitMQPersistentConnection>>();


        //        var factory = new ConnectionFactory()
        //        {
        //            HostName = Configuration["EventBusConnection"],
        //            DispatchConsumersAsync = true
        //        };

        //        if (!string.IsNullOrEmpty(Configuration["EventBusUserName"]))
        //        {
        //            factory.UserName = Configuration["EventBusUserName"];
        //        }

        //        if (!string.IsNullOrEmpty(Configuration["EventBusPassword"]))
        //        {
        //            factory.Password = Configuration["EventBusPassword"];
        //        }

        //        var retryCount = 5;
        //        if (!string.IsNullOrEmpty(Configuration["EventBusRetryCount"]))
        //        {
        //            retryCount = int.Parse(Configuration["EventBusRetryCount"]);
        //        }

        //        return new DefaultRabbitMQPersistentConnection(factory, logger, retryCount);
        //    });



        //    services.AddSingleton<IEventBus, EventBusRabbitMQ>(sp =>
        //    {
        //        var subscriptionClientName = Configuration["SubscriptionClientName"];
        //        var rabbitMQPersistentConnection = sp.GetRequiredService<IRabbitMQPersistentConnection>();
        //        var iLifetimeScope = sp.GetRequiredService<ILifetimeScope>();
        //        var logger = sp.GetRequiredService<ILogger<EventBusRabbitMQ>>();
        //        var eventBusSubcriptionsManager = sp.GetRequiredService<IEventBusSubscriptionsManager>();

        //        var retryCount = 5;
        //        if (!string.IsNullOrEmpty(Configuration["EventBusRetryCount"]))
        //        {
        //            retryCount = int.Parse(Configuration["EventBusRetryCount"]);
        //        }

        //        return new EventBusRabbitMQ(rabbitMQPersistentConnection, logger, iLifetimeScope, eventBusSubcriptionsManager, subscriptionClientName, retryCount);
        //    });
      
        //    services.AddSingleton<IEventBusSubscriptionsManager, InMemoryEventBusSubscriptionsManager>();

        //}

        //public static void ConfigureEventBus(this IApplicationBuilder app)
        //{
        //    var eventBus = app.ApplicationServices.GetRequiredService<IEventBus>();

        //    eventBus.Subscribe<NewOrderCreatedIntegrateEvent, NewOrderCreatedIntegrationEventHandler>();
        //}
    }
}
