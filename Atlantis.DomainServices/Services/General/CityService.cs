﻿
using Atlantis.DomainServices.Mapping;
using AutoMapper;
using DomainCore.Dto.Base;
using DomainCore.Dto.General;
using DomainCore.DTO.Base;
using DomainCore.General;
using DomainData.Contracts;
using DomainServices.Contracts;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.Drawing;

using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility.Tools.General;

namespace DomainServices.Services
{
    public class CityService : ICityService
    {
        private readonly IUnitOfWork unit;
        private readonly IMapper mapper;
        private readonly IHostingEnvironment hosting;

        public CityService(IUnitOfWork unit , IMapper mapper)
        {
            this.unit = unit;
            this.mapper = mapper;
        }
        public async Task<ApiListResult<ProvinceDto>> GetProvines()
        {
            var result = new ApiListResult<ProvinceDto> { Status = true, Message = Messages.EngOK };
            try
            {
                var prs= await unit.Province.GetIncludeCtiesAsync();
                result.Data = prs.Select(mapper.Map<ProvinceDto>).ToList();
            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;

        }
        public async Task<ApiListResult<CityDto>> GetByProvinceId(BaseByIntDto dto)
        {
            var result = new ApiListResult<CityDto> { Status = true, Message = Messages.EngOK };
            try
            {
                var prs = await unit.City.GetByProvinceId(dto.Id);
                result.Data = prs.Select(p=> mapper.Map<CityDto>(p)).ToList();
            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;

        }

    }
}
