﻿
using Atlantis.DomainServices.Mapping;
using AutoMapper;
using DomainCore.Dto.Base;
using DomainCore.Dto.General;
using DomainCore.DTO.AppMore;
using DomainCore.DTO.Base;
using DomainCore.DTO.Slider;
using DomainCore.General;
using DomainData.Contracts;
using DomainData.Ef.Context;
using DomainServices.Contracts;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Drawing;

using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility.Tools.General;

namespace DomainServices.Services
{
    public class GeneralService : IGeneralService
    {
        private readonly IUnitOfWork unit;
        private readonly IHostingEnvironment hosting;
        private readonly IContext context;
        private readonly IMapper mapper;

        public GeneralService(
            IUnitOfWork unit,
            IHostingEnvironment hosting,
            IContext context ,
            IMapper mapper
            )
        {
            this.unit = unit;
            this.hosting = hosting;
            this.context = context;
            this.mapper = mapper;
        }
        public async Task<ApiResult<SplashResultDto>> GetSplash(GetSplashDto dto)
        {
            var result = new ApiResult<SplashResultDto> { Status = true, Message = Messages.EngOK ,Data = new SplashResultDto() { } };
            try
            {
                var update = await unit.Update.GetUpdateAsync(dto);
                result.Data.Announcements = unit.Announcement.GetActive()
                    .Select(mapper.Map<AnnouncementDto>).ToList();
                result.Data.Update = mapper.Map<UpdateDto>(update);

            }catch(Exception e)
            {
                result.Error(e);
            }


            return result;

        }
         public async Task<ApiListResult<SliderDto>> GetSliders()
        {
            var result = new ApiListResult<SliderDto> { Status = true, Message = Messages.EngOK };
            try
            {
                

            }catch(Exception e)
            {
                result.Error(e);
            }


            return result;

        }

        public async Task<ApiResult<DocumentDto>> Get(BaseByGuidDto dto)
        {
            var result = new ApiResult<DocumentDto> { Status = true, Message = Messages.EngOK };

           
            return result;

        }
    }
}
