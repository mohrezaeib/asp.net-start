using Atlantis.DomainServices.Mapping;
using AutoMapper;
using DomainCore.Dto.Base;
using DomainCore.Dto.General;
using DomainCore.DTO.Base;
using DomainCore.Entities;
using DomainCore.General;
using DomainData.Contracts;
using DomainServices.Contracts;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.Drawing;

using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Utility.Tools.General;

namespace DomainServices.Services
{
    public class DocumentService : IDocumentService
    {
        private readonly IUnitOfWork unit;
        private readonly IHostingEnvironment hosting;
        private readonly ICurrentUser currentUser;
        private readonly IMapper mapper;

        public DocumentService(IUnitOfWork unit, IHostingEnvironment hosting, ICurrentUser currentUser, IMapper mapper)
        {
            this.unit = unit;
            this.hosting = hosting;
            this.currentUser = currentUser;
            this.mapper = mapper;
        }
        public async Task<ApiResult<DocumentDto>> Send(SendDocumentDto dto)
        {
            var result = new ApiResult<DocumentDto> { Status = true, Message = Messages.EngOK };
            try
            {
                var file = dto.File;
                string extension = Path.GetExtension(file.FileName);
                Guid guid = Guid.NewGuid();
                string fileName = guid.ToString() + extension;
                var path = hosting.WebRootPath + "/Documents";
                Directory.CreateDirectory(path);
                var HardLocation = path + $"/{fileName}";
                var HostLocation = "/Documents" + $"/{fileName}";

                if (file.Length > 0)
                {
                    using (var fileStream = new FileStream(HardLocation, FileMode.Create))
                    {
                        await file.CopyToAsync(fileStream);
                    }
                }

                Document doc = new Document()
                {
                    Id = guid,
                    Url = HostLocation,
                    CreatedAt = Agent.Now,
                    DocumentType = dto.Type,
                    Size = file.Length,
                    Name = file.FileName

                };
                await unit.Document.AddAsync(doc);
                await unit.CompleteAsync();
                result.Data = mapper.Map<DocumentDto>(doc);
            }
            catch (Exception e)
            {
                result.Error(e);
            }
            return result;

        }

        public async Task<ApiResult<DocumentDto>> rembg(removebgDto dto)
        {
            var result = new ApiResult<DocumentDto> { Status = true, Message = Messages.EngOK };
            try
            {

                var uri = new Uri("http://135.181.107.173:5000/?url=" + dto.url);
                HttpClient client = new HttpClient();
                var response = await client.GetAsync(uri);
                string extension = ".png";
                Guid guid = Guid.NewGuid();
                string fileName = guid.ToString() + extension;
                var path = hosting.WebRootPath + "/Documents";
                Directory.CreateDirectory(path);
                var HardLocation = path + $"/{fileName}";
                var HostLocation = "/Documents" + $"/{fileName}";


                using (var fileStream = new FileStream(HardLocation, FileMode.Create))
                {
                    await response.Content.CopyToAsync(fileStream);
                }


                Document doc = new Document()
                {
                    Id = guid,
                    Url = HostLocation,
                    CreatedAt = Agent.Now,
                    DocumentType = DomainCore.Enums.DocumentType.Image,
                    Size = 0,
                    Name = "removedbg_ "+dto.url,

                };
                await unit.Document.AddAsync(doc);
                await unit.CompleteAsync();
                result.Data = mapper.Map<DocumentDto>(doc);
            }
            catch (Exception e)
            {
                result.Error(e);
            }
            return result;

        }

        public async Task<ApiResult<DocumentDto>> Get(BaseByGuidDto dto)
        {
            var result = new ApiResult<DocumentDto> { Status = true, Message = Messages.EngOK };

            try
            {
                var doc = await unit.Document.GetByIdAsync(dto.Id);
                result.Data = mapper.Map<DocumentDto>(doc);
            }
            catch (Exception e)
            {
                result.Error(e);
            }
            return result;

        }




        public async Task<ApiResult<DocumentDto>> RemoveBg(SendDocumentDto dto)
        {
            var result = new ApiResult<DocumentDto> { Status = true, Message = Messages.EngOK };
            try
            {
                var file = dto.File;
                string extension = Path.GetExtension(file.FileName);
                Guid guid = Guid.NewGuid();
                string fileName = guid.ToString() + extension;
                var path = hosting.WebRootPath + "/Documents";
                Directory.CreateDirectory(path);
                var HardLocation = path + $"/{fileName}";
                var HostLocation = "/Documents" + $"/{fileName}";

                if (file.Length > 0)
                {
                    using (var fileStream = new FileStream(HardLocation, FileMode.Create))
                    {
                        await file.CopyToAsync(fileStream);
                    }
                }

                Document doc = new Document()
                {
                    Id = guid,
                    Url = HostLocation,
                    CreatedAt = Agent.Now,
                    DocumentType = dto.Type,
                    Size = file.Length,
                    Name = file.FileName

                };
                await unit.Document.AddAsync(doc);
                await unit.CompleteAsync();
                result.Data = mapper.Map<DocumentDto>(doc);
            }
            catch (Exception e)
            {
                result.Error(e);
            }
            return result;

        }




    }



}
