﻿using DomainCore.Dto.Base;
using DomainCore.Dto.General;
using DomainCore.DTO.Base;
using DomainCore.DTO.Slider;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DomainServices.Contracts
{
    public interface IGeneralService : IApplicationService
    {
        Task<ApiResult<SplashResultDto>> GetSplash(GetSplashDto dto);
        Task<ApiListResult<SliderDto>> GetSliders();
    }
    
}
