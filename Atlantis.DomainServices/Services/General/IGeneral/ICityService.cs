﻿using DomainCore.Dto.Base;
using DomainCore.Dto.General;
using DomainCore.DTO.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DomainServices.Contracts
{
    public interface ICityService : IApplicationService
    {
        Task<ApiListResult<ProvinceDto>> GetProvines();
        Task<ApiListResult<CityDto>> GetByProvinceId(BaseByIntDto dto);

    }

}
