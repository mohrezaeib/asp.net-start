﻿
using Atlantis.DomainServices.Mapping;
using AutoMapper;
using DomainCore.Dto.Base;
using DomainCore.Dto.General;
using DomainCore.DTO.Base;
using DomainCore.DTO.Slider;
using DomainCore.Entities.General;
using DomainCore.Enums;
using DomainCore.General;
using DomainData.Contracts;
using DomainServices.Contracts;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.Drawing;

using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility.Tools.General;

namespace DomainServices.Services
{
    public class SliderCRUD : ISliderCRUD
    {
        private readonly IUnitOfWork unit;
        private readonly IMapper mapper;

        public SliderCRUD(IUnitOfWork unit , IMapper mapper)
        {
            this.unit = unit;
            this.mapper = mapper;
        }
        //CRUD Admin
        #region
        //Admin area
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<ApiResult<SliderDto>> EditSlider(EditSliderDto dto)
        {
            var result = new ApiResult<SliderDto> { Status = true, Message = Messages.EngOK };
            try
            {

                var ser = await unit.Slider.GetByIdAsync(dto.Id);

                if (ser == null) { result.Error(Errors.EntityNotFound); return result; }
               
                ser = mapper.Map<EditSliderDto, Slider>(dto, ser);

                await unit.CompleteAsync();
                ser = await unit.Slider.GetByIdAsync(ser.Id);
                result.Data = mapper.Map<SliderDto>(ser);
            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;

        }
        public async Task<ApiResult<SliderDto>> AddSlider(AddSliderDto dto)
        {
            var result = new ApiResult<SliderDto> { Status = true, Message = Messages.EngOK };
            try
            {

                var ser = mapper.Map<Slider>(dto);
                
                await unit.Slider.AddAsync(ser);
                await unit.CompleteAsync();
                ser = await unit.Slider.GetByIdAsync(ser.Id);
                result.Data = mapper.Map<SliderDto>(ser);


            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;

        }
     
        public async Task<BaseApiResult> DeleteSlider(BaseByIntDto dto)
        {
            var result = new BaseApiResult { Status = true, Message = Messages.EngOK };
            try
            {
                var bp = await unit.Slider.GetByIdAsync(dto.Id);
              
                unit.Slider.Remove(bp);
                try
                {
                    await unit.CompleteAsync();

                } catch(Exception e)
                {
                    result.Error(Errors.EntityCouldNotBeDeleted);
                }
            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;
        }


        public async Task<ApiListResult<SliderDto>> GetAllSlider()
        {
            var result = new ApiListResult<SliderDto> { Status = true, Message = Messages.EngOK };
            try
            {
                var resp = await unit.Slider.GetAllAsync();
                result.Data = resp.Select(p=> mapper.Map<SliderDto>(p)).ToList();

            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;
        }

        public async Task<ApiResult<SliderDto>> GetSliderById(BaseByIntDto dto)
        {
            var result = new ApiResult<SliderDto> { Status = true, Message = Messages.EngOK };
            try
            {
                var ser = await unit.Slider.GetByIdAsync(dto.Id);

                result.Data = mapper.Map<SliderDto>(ser);
            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;
        }

        public async Task<ApiPageResult<SliderDto>> GetSliderByFilter(BaseByFilter dto)
        {
            var result = new ApiPageResult<SliderDto> { Status = true, Message = Messages.EngOK };
            try
            {

                var resp = await unit.Slider.GetAllAsync();

                var query = resp.AsQueryable().Where(p=> 
              
                (dto.From == null || dto.From >= p.CreatedAt)&&
                (dto.To == null || dto.To <= p.CreatedAt)
                );
                var paged = new Paged<Slider, SliderDto>(query, dto, mapper.Map<SliderDto>);
                result.Data = await paged.GetItmesAsync();
                result.PageDto = paged.GetPageDto();

            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;
        }
        #endregion // 

       


    }
}
