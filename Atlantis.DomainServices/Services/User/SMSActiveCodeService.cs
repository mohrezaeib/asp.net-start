﻿

using Atlantis.DomainServices.Mapping;
using AutoMapper;
using DomainCore.AppUsers;
using DomainCore.Dto.Base;
using DomainCore.Dto.General;
using DomainCore.Dto.User;
using DomainCore.Enums;
using DomainCore.General;
using DomainData.Contracts;
using DomainServices.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utility.Tools;
using Utility.Tools.Auth;
using Utility.Tools.EmailServies;
using Utility.Tools.General;
using Utility.Tools.SMS;
using Utility.Tools.SMS.TwilioSMS;

namespace DomainServices.Services
{
    /// <summary>
    /// Active Code And Login
    /// </summary>
    public class SMSActiveCodeService : ISMSActiveCodeService
    {
        private readonly IUnitOfWork unit;
        private readonly IJwtHandler jwtHandler;
        private readonly ISMSService messageService;
        private readonly ISimpleEmailSender emailService;
        private readonly ILoginService loginService;
        private readonly IMapper mapper;

        public SMSActiveCodeService(
            ISMSService twilioService,
            IUnitOfWork unit,
            IJwtHandler jwtHandler,
            ISMSService messageService,
            ISimpleEmailSender emailService,
            ILoginService loginService,
            IMapper mapper
            )
        {
            this.unit = unit;
            this.jwtHandler = jwtHandler;
            this.messageService = messageService;
            this.emailService = emailService;
            this.loginService = loginService;
            this.mapper = mapper;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto">SendActiveCodeDto</param>
        /// <returns></returns>
        public async Task<BaseApiResult> SendActiveCode(SendActiveCodeDto dto)
        {
            BaseApiResult result = new BaseApiResult { Message = Messages.LimitExceeded };

            try
            {
                //if (!String.IsNullOrEmpty(dto.Email))
                //{
                //    var result2 = await loginService.RegisterByEmail(new RegisterByEmailDto { Email = dto.Email });
                //    result =result2;
                //    return result;

                //}
                var now = Agent.UnixTimeNow();
                if (!unit.ActiveCode.CheckExeedMobile(dto.Mobile))
                {
                    string Code = Agent.GenerateRandomNo(6);
                    await unit.ActiveCode.AddAsync(new ActiveCode() { Mobile =new Mobile( dto.Mobile), Code = Code, CreatedAt = now });
                    await unit.CompleteAsync();
                    var sent = $"{AdminSettings.SMSName}\n{AdminSettings.SMSTitle}:{Code}\n{AdminSettings.SMSCode}";
                    //TODO
                    // var messageResp2 = await twilioService.SendAsync(sent, dto.Mobile);
                    var messageResp = await messageService.SendAsync(sent, dto.Mobile);

                    result.Message = Messages.OK;
                    result.Status = true;
                }
            }
            catch (Exception e)
            {
                result.Error(e);
            }
            return result;
        }

        public async Task<ApiResult<UserDto>> CheckActiveCode(CheckActiveCodeDto dto)
        {
            var result = new ApiResult<UserDto> { Message = Messages.NotOK };

            try
            {

                //if (!String.IsNullOrEmpty(dto.Email))
                //{
                //    var result2 = await loginService.ConfirmEmail(new ConfirmEmailDto { Email = dto.Email , Code= dto.Code });
                //    result = result2;
                //    return result;

                //}
                var now = Agent.UnixTimeNow();
                if (unit.ActiveCode.CheckActiveCode(dto))
                {
                    result.Message = Messages.OK;
                    result.Status = true;

                    var user = await unit.Users.GetByMobileAsync(dto.Mobile);

                    if (user != null)
                    {

                        if (!unit.Device.IsExist(user.Id, dto.PushId))
                        {
                            await unit.Device.AddAsync(new Device { PushId = dto.PushId, CreatedAt = now, UserId = user.Id });
                            await unit.CompleteAsync();
                        }

                    }
                    else
                    {
                        user = User.Create();

                        user.SetMobile (new Mobile(dto.Mobile));
                        user.CreatedAt = now;
                        user.Device = new List<Device> { new Device { PushId = dto.PushId, CreatedAt = now } };
                        user.Status = UserStatus.Active;
                        user.UserRole = new List<UserRole> { new UserRole { RoleId = Roles.User, CreatedAt = now } };
                        await unit.Users.AddAsync(user);
                        await unit.CompleteAsync();
                    }
                    user = await unit.Users.GetDetailAsync(user.Id);
                    var userDto = mapper.Map<UserDto>(user);
                    userDto.Token = jwtHandler.Create(user.Id);
                    result.Data = userDto;

                }
            }
            catch (Exception e)
            {
                result.Error(e);
            }
            return result;
        }


    }
}
