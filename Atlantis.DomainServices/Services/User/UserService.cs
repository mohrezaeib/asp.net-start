﻿
using Atlantis.DomainServices.Mapping;
using AutoMapper;
using DomainCore.AppUsers;
using DomainCore.Dto.Base;
using DomainCore.Dto.General;
using DomainCore.Dto.User;
using DomainCore.DTO.Base;
using DomainCore.Entities;
using DomainCore.Entities.Base;
using DomainCore.Enums;
using DomainCore.General;
using DomainData.Contracts;
using DomainServices.Contracts;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility.Tools;
using Utility.Tools.Auth;
using Utility.Tools.General;

namespace DomainServices.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork unit;
        private readonly IEncrypter encrypter;
        private readonly ICurrentUser currentUser;
        private readonly IMapper mapper;
        private readonly IRepository<UserFile> userFileRepo;
        private readonly IRepository<UserRole> userRoleRepo;
        private readonly IFireBase fireBase;

        public UserService(IUnitOfWork unit,
            IEncrypter encrypter,
            ICurrentUser sessionContext,
            IMapper mapper,
            IRepository<UserFile> userFileRepo,
            IRepository<UserRole> userRoleRepo,
            IFireBase fireBase
            )
        {
            this.unit = unit;
            this.encrypter = encrypter;
            this.currentUser = sessionContext;
            this.mapper = mapper;
            this.userFileRepo = userFileRepo;
            this.userRoleRepo = userRoleRepo;
            this.fireBase = fireBase;
        }

        public async Task<ApiResult<UserDto>> AddUserByAdmin(AddUserByAdminDto dto)
        {
            var result = new ApiResult<UserDto> { Status = true, Message = Messages.EngOK };
            try
            {
                var now = Agent.Now;
                var searchResult = await unit.Users.FindByExpressionAsync(p => p.Mobile.FullNumber == dto.Mobile || p.Email.Address == dto.Email);
                var user = User.Create();

                if (searchResult.Count == 0)

                {
                    mapper.Map<AddUserByAdminDto, User>(dto, user);
                    await unit.Users.AddAsync(user);

                    //Extra 
                    user.Status = UserStatus.Active;
                    if (!string.IsNullOrEmpty(dto.Password))
                        user.SetPassword(dto.Password, encrypter);
                    await unit.CompleteAsync();
                    //End Extra
                    user = await unit.Users.GetDetailAsync(user.Id);
                    result.Data = mapper.Map<UserDto>(user);


                }
                else
                {
                    throw new Exception("There is already an user whith this email or mobile.");
                }

            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;

        }

        public async Task<ApiResult<UserDto>> EditUserSetting(EditUserSettingDto dto)
        {
            var result = new ApiResult<UserDto> { Status = true, Message = Messages.EngOK };
            try
            {

                var user = currentUser.UserId.HasValue ? await unit.Users.GetDetailAsync(currentUser.UserId.Value) : null;
                if (user == null) { result.Error(Errors.UserNotFound); return result; }
                var setting = mapper.Map<UserSetting>(dto);

                user.UserSetting = setting;
                await unit.CompleteAsync();
                user = await unit.Users.GetDetailAsync(user.Id);
                result.Data = mapper.Map<UserDto>(user);

            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;

        }

        public async Task<ApiResult<UserDto>> EditProfile(EditProfileDto dto)
        {

            var result = new ApiResult<UserDto> { Status = true, Message = Messages.EngOK };
            try
            {
                var user = await unit.Users.GetDetailAsync(dto.UserId);
                if (user == null) { result.Error(Errors.UserNotFound); return result; }
                user = mapper.Map<EditProfileDto, User>(dto, user);

               
                await unit.CompleteAsync();

                user = await unit.Users.GetDetailAsync(user.Id);
                result.Data = mapper.Map<UserDto>(user);


            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;

        }

        public async Task<ApiResult<UserDto>> GetUser(BaseByGuidDto dto)
        {
            var result = new ApiResult<UserDto> { Status = true, Message = Messages.EngOK };

            try
            {
                var user = await unit.Users.GetDetailAsync(dto.Id);
                result.Data = mapper.Map<UserDto>(user);

            }
            catch (Exception e)
            {
                result.Error(e);
            }
            return result;
        }
        public async Task<ApiPageResult<UserDto>> GetUsersByFilter(GetUsersByFilterDto dto)
        {
            var result = new ApiPageResult<UserDto> { Status = true, Message = Messages.EngOK };
            try
            {
                var query = unit.Users.GetByFilter(dto);
                var paged = new Paged<User, UserDto>(query, dto, mapper.Map<UserDto>);

                result.Data = await paged.GetItmesAsync();
                result.PageDto = paged.GetPageDto();
            }
            catch (Exception e)
            {
                result.Error(e);
            }
            return result;

        }

        public Guid GetUserIdFromToken(string basedToken)
        {
            var userId = new Guid();
            try
            {
                if (basedToken != null && basedToken != "")
                {
                    var token = basedToken;
                    var handler = new JwtSecurityTokenHandler();
                    var jwtToken = handler.ReadToken(token) as JwtSecurityToken;
                    var IdString = jwtToken.Claims.FirstOrDefault(p => p.Type == "sub").Value;
                    userId = Guid.Parse(IdString);
                }
            }
            catch (Exception e)
            {

            }

            return userId;

        }



        public async Task<ApiResult<UserDto>> EditUserByAdmin(EditProfileByAdminDto dto)
        {
            var result = new ApiResult<UserDto> { Status = true, Message = Messages.EngOK };
            try
            {



                var user = await unit.Users.GetDetailAsync(dto.UserId);
                if (user == null) { result.Error(Errors.EntityNotFound); return result; }



                user = mapper.Map<EditProfileByAdminDto, User>(dto, user);
                await unit.CompleteAsync();
                user = await unit.Users.GetByIdAsync(user.Id);
                result.Data = mapper.Map<UserDto>(user);


            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;
        }

        public async Task<ApiResult<UserDto>> SetPassword(SetPasswordByUserDto dto)
        {
            var result = new ApiResult<UserDto> { Status = true, Message = Messages.EngOK };
            try
            {

                var user = await unit.Users.GetDetailAsync(dto.UserId);
                if (user == null) { result.Error(Errors.EntityNotFound); return result; }
                if (user.Status == UserStatus.Active)
                {
                    if (!String.IsNullOrEmpty(dto.Password))
                        user.SetPassword(dto.Password, encrypter);
                }
                else
                {
                    result.Error(Errors.UserNotActivated);
                    return result;
                }

                await unit.CompleteAsync();
                user = await unit.Users.GetByIdAsync(user.Id);
                result.Data = mapper.Map<UserDto>(user);


            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;
        }


        public async Task<ApiResult> AddUserNotification(AddUserNotificationDto dto)
        {
            var result = new ApiResult { Status = true, Message = Messages.EngOK, };
            try
            {
                var Notif = new Notification { Body = dto.Body, Title = dto.Title, ImageUrl = dto.ImageUrl };
                unit.Notification.Add(Notif);
                unit.Complete();
                var notif = mapper.Map<UserNotification>(dto);

                foreach (var id in dto.Users)
                {
                    var user = await unit.Users.GetByIdAsync(id);
                    user.Notification.Add(notif);
                    await unit.CompleteAsync();
                    var tokens = user.Device.Select(p => p.PushId).ToList();
                    await fireBase.SendNotification(tokens, new FirebaseAdmin.Messaging.Notification { Body = notif.Body, ImageUrl = notif.ImageUrl, Title = notif.Title });
                }


            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;

        }
        public async Task<ApiResult> SeenUserNotification(BaseByGuidDto dto)
        {
            var result = new ApiResult { Status = true, Message = Messages.EngOK, };
            try
            {


                var user = await unit.Users.GetByIdAsync(currentUser.UserId.Value);
                var notif = user.Notification.FirstOrDefault(p => p.Id == dto.Id);
                notif.IsSeen = true;
                await unit.CompleteAsync();



            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;

        }
        public async Task<ApiPageResult<NotificationDto>> GetNotificationsByFilter(GetNotificationsByFilterDto dto)
        {
            var result = new ApiPageResult<NotificationDto> { Status = true, Message = Messages.EngOK, };
            try
            {


                var query = unit.Notification.GetByFilter(dto);
                var paged = new Paged<Notification, NotificationDto>(query, dto, mapper.Map<NotificationDto>);

                result.Data = await paged.GetItmesAsync();
                result.PageDto = paged.GetPageDto();

            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;

        }
        public async Task<ApiPageResult<UserNotificationDto>> GetNotifications(GetUserNotificationsByFilterDto dto)
        {
            var result = new ApiPageResult<UserNotificationDto> { Status = true, Message = Messages.EngOK, };
            try
            {


                var query = unit.UserNotification.GetByFilter(dto);
                var paged = new Paged<UserNotification, UserNotificationDto>(query, dto, mapper.Map<UserNotificationDto>);

                result.Data = await paged.GetItmesAsync();
                result.PageDto = paged.GetPageDto();

            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;

        }
          
        //public async Task<ApiResult<string>> GetRefferalByMe()
        //{
        //    var result = new ApiResult<string> { Status = true, Message = Messages.EngOK };
        //    try
        //    {
        //        var user = await unit.Users.GetByIdAsync(currentUser.UserId.Value);
        //        if (String.IsNullOrEmpty(user.ReffralCode))
        //        {
        //            var code = Agent.GetRandomCode(6);
        //            while ((await unit.Users.FindByExpressionAsync(p => p.ReffralCode == code)).Count != 0)
        //            {
        //                code = Agent.GetRandomCode(6);
        //            }
        //            user.ReffralCode = code;
        //            unit.Complete();
        //        }

        //        result.Data = user.ReffralCode;


        //    }
        //    catch (Exception e)
        //    {
        //        result.Error(e);
        //    }

        //    return result;

        //}


    }
}
