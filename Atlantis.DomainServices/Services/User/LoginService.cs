

using Atlantis.DomainServices.Mapping;
using AutoMapper;
using DomainCore.AppUsers;
using DomainCore.Dto.Base;
using DomainCore.Dto.General;
using DomainCore.Dto.User;
using DomainCore.DTO.Base;
using DomainCore.Entities;
using DomainCore.Entities.Base;
using DomainCore.Enums;
using DomainCore.General;
using DomainData.Contracts;
using DomainServices.Contracts;
using HtmlAgilityPack;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.WebUtilities;
using System;
using System.Collections.Generic;
using System.Drawing;

using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility.Tools;
using Utility.Tools.Auth;
using Utility.Tools.EmailServies;
using Utility.Tools.General;
using Utility.Tools.SMS;
using Utility.Tools.SMS.TwilioSMS;

namespace DomainServices.Services
{
    public class LoginService : ILoginService
    {
        private readonly IUnitOfWork unit;
        private readonly IEncrypter encrypter;
        private readonly IJwtHandler jwtHandler;
        private readonly ISMSService messageService;
        private readonly IMapper mapper;
        private readonly ISimpleEmailSender emailService;
        private readonly ICurrentUser currentUser;
        private readonly UserManager<IdentityUser> userManager;


        public LoginService(IUnitOfWork unit,
            IEncrypter encrypter,
            IJwtHandler jwtHandler,
            ISMSService messageService,
            IMapper mapper,
            ISimpleEmailSender emailService,
            ICurrentUser currentUser,
            UserManager<IdentityUser> userManager
            )
        {
            this.unit = unit;
            this.encrypter = encrypter;
            this.jwtHandler = jwtHandler;
            this.messageService = messageService;
            this.mapper = mapper;
            this.emailService = emailService;
            this.currentUser = currentUser;
            this.userManager = userManager;
        }

        public async Task<ApiResult> ForgotPassWord(ForgotPasswordDto dto)
        {
            var result = new ApiResult { Status = true, Message = Messages.EngOK };
            try
            {
                var user = await unit.Users.GetByEmailAsync(dto.Email);
                var identity = await GetIdentity(user);
                var token = await userManager.GeneratePasswordResetTokenAsync(identity);
                var param = new Dictionary<string, string>
                {
                    {"token", token },
                    {"email", dto.Email.ToLower() }
                 };
                var callback = QueryHelpers.AddQueryString(AdminSettings.ForgetPasswordURL, param);
                await emailService.SendAsync("Please click on this link for reset your password \n"+callback, dto.Email);

            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;
        }
        public async Task<ApiResult> ResetPassword(ResetPasswordDto dto)
        {
            var result = new ApiResult { Status = true, Message = Messages.EngOK };
            try
            {
                var user = await unit.Users.GetByEmailAsync(dto.Email);
                var identity = await GetIdentity(user);

                var resetPassResult = await userManager.ResetPasswordAsync(identity, dto.Token, dto.Password);
                if (!resetPassResult.Succeeded)
                {
                    var errors = resetPassResult.Errors.Select(e => e.Description);
                    throw new Exception(errors.FirstOrDefault());

                }
                user.SetPassword(dto.Password, encrypter);
                await unit.CompleteAsync();
            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;
        }
        public async Task<ApiResult> ChangePassWord(ChangePasswordDto dto)
        {
            var result = new ApiResult { Status = true, Message = Messages.EngOK };
            try
            {

                var user = await unit.Users.GetByIdAsync(currentUser.UserId.Value);
                //var identity = await GetIdentity(user);

                if (user == null) { result.Error(Errors.UserNotFound); return result; }
                if (user.Status != UserStatus.Active) { result.Error(Errors.UserNotActivated); return result; }
                if (!string.IsNullOrEmpty(user.Password) && !user.ValidatePassword(dto.OldPassword, encrypter))
                {
                    throw new LogicalException(Errors.WrongPassword);
                }

               

                if (!Agent.CheckPasswordComplexity(dto.NewPassword))
                {
                    throw new LogicalException(Errors.BadPassword);

                }


                user.SetPassword(dto.NewPassword, encrypter);
                await unit.CompleteAsync();





            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;
        }






        public async Task<ApiResult<UserDto>> Login(LoginDto dto)
        {
            var result = new ApiResult<UserDto> { Status = true, Message = Messages.EngOK };
            try
            {
                var user = IsValidEmail(dto.UserName) ? await unit.Users.GetByEmailAsync(dto.UserName) : await unit.Users.GetByMobileAsync(dto.UserName);
                if (user == null) { result.Error(Errors.UserNotFound); return result; }
                if (user.Status != UserStatus.Active) { result.Error(Errors.UserNotActivated); return result; }
                if (!string.IsNullOrEmpty(dto.Password) && user.ValidatePassword(dto.Password, encrypter))
                {




                    //TODO here need review ! we should make user mangaer and identiy service same
                    //اگه پسورد درست بود و توی یوزر منیجر سیو نشده بود دوباره سیو کن

                    //var identity = await GetIdentity(user, dto.Password);

                    //var isCorrect = await userManager.CheckPasswordAsync(identity, dto.Password);
                    //if (!isCorrect)
                    //{ result.Error(Errors.WrongPassword); return result; }

                    await AddPushId(user.Id, dto.PushId);

                    var userDto = mapper.Map<UserDto>(user);
                    userDto.Token = jwtHandler.Create(user.Id);
                    result.Data = userDto;

                }
                else { result.Error(Errors.WrongPassword); return result; };

            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;
        }
        private async Task<IdentityUser> GetIdentity(User user, string password = null)
        {
            try
            {
                var identity = await userManager.FindByIdAsync(user.Id.ToString());
                if (identity == null) identity = await userManager.FindByEmailAsync(user.Email.Address);
                if (identity != null)
                {
                    //update values 

                    if (user.Email?.Address != null) await userManager.SetEmailAsync(identity, user.Email.Address);
                    if (user.Mobile?.FullNumber != null) await userManager.SetPhoneNumberAsync(identity, user.Mobile.FullNumber);
                    if (user.Email?.Address != null) await userManager.SetUserNameAsync(identity, user.Email.Address);
                    return identity;
                };

                identity = new IdentityUser()
                {
                    EmailConfirmed = true,
                    Email = user.Email?.Address,
                    PhoneNumber = user.Mobile?.FullNumber,
                    UserName = user.Email?.Address,
                    Id = user.Id.ToString(),

                };

                var isCreated = password != null ?
                    await userManager.CreateAsync(identity, password) : await userManager.CreateAsync(identity);

                if (!isCreated.Succeeded)
                {
                    var errors = "";
                    isCreated.Errors.Select(p => p.Description).ToList().ForEach(p => errors.Concat("\\n  " + p));
                    throw new LogicalException(errors);
                }
                identity = await userManager.FindByIdAsync(user.Id.ToString());

                return identity;
            }
            catch (Exception)
            {
                throw new LogicalException("Failed to idenntify");
            }
        }
        public async Task<ApiResult<UserResisterDto>> RegisterByEmail(RegisterByEmailDto dto)
        {
            var result = new ApiResult<UserResisterDto> { Status = true, Message = Messages.EngOK };


            try
            {
                var user = await unit.Users.GetByEmailAsync(dto.Email);

                var status  = GetUserSatus(user);
                result.Data = status;
                if(!status.HasPassword )
                await SendAnActiveCodeByEmail(dto.Email);
                //await emailService.SendAsync($"Atlatis Clinics: Your Activation Code is {code}", dto.Email);


            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;
        }
        private UserResisterDto GetUserSatus(User user)
        {
            var d = new UserResisterDto();
            if (user == null)
            {
                d.HasPassword = false;
                d.IsNew = true;
            }
            else if (user.Status == UserStatus.Active && user.Password != null)
            {

                d.HasPassword = true;
                d.IsNew = false;

            }
            else if (user.Status == UserStatus.Active && user.Password == null)
            {


                d.HasPassword = false;
                d.IsNew = false;
            }

            else if (user.Status != UserStatus.Active)
            {
                throw new LogicalException(Errors.UserNotActivated);

            }
            return d;
        }

        private async Task SendAnActiveCodeByEmail(string email)
        {

            if (!unit.ActiveCode.CheckExeedEmail(email))
            {
                var code = Agent.GenerateRandomNo(6);
                var txt = GetActivationCodeEmailTemplate(code);
                await unit.ActiveCode.AddAsync(new ActiveCode() { Email = new Email(email), Code = code, });
                await unit.CompleteAsync();
                //await emailService.SendAsync(email, "Activation Code", txt, "", true);
                await emailService.SendAsync(txt, email);
            }
            else
            {
                throw new LogicalException(Errors.TooManyRequest);
            }
        }
        public async Task<ApiResult<UserDto>> ConfirmEmail(ConfirmEmailDto dto)
        {
            var result = new ApiResult<UserDto> { Status = true, Message = Messages.EngOK };
            try
            {
                var check = unit.ActiveCode.CheckActiveCodeByEmail(dto.Email, dto.Code);
                if (!check) throw new LogicalException(Errors.WrongConfirmationCode);

                var user = await unit.Users.GetByEmailAsync(dto.Email);

                if (user == null)
                {
                    user = User.Create();
                    user.SetEmail(new Email(dto.Email));
                    user.Status = UserStatus.Active;
                    user.UserRole = new List<UserRole> { new UserRole { RoleId = Roles.User } };
                    await unit.Users.AddAsync(user);
                    await unit.CompleteAsync();
                }
                await AddPushId(user.Id, dto.PushId);

                user = await unit.Users.GetDetailAsync(user.Id);
                //var identity = await GetIdentity(user);

                var userDto = mapper.Map<UserDto>(user);
                userDto.Token = jwtHandler.Create(user.Id);
                result.Data = userDto;


            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;
        }

        private async Task AddPushId(Guid userId, string pushId)
        {
            if (!unit.Device.IsExist(userId, pushId))
            {
                await unit.Device.AddAsync(new Device { PushId = pushId, UserId = userId });
                await unit.CompleteAsync();
            }

        }
        private string GetActivationCodeEmailTemplate(string code)
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(@" Your Activition Code Is : " + code + @" ");
            var txt = doc.ParsedText;
            return txt;



        }
        private bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
    }




}
