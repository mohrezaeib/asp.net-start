﻿using AutoMapper;
using DomainCore.Dto.Base;
using DomainCore.DTO.AppMore;
using DomainCore.DTO.Base;
using DomainCore.Entities.AppMore;
using DomainCore.Enums;
using DomainData.Contracts;
using DomainServices.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utility.Tools.General;

namespace DomainServices.Services
{
    public class AnnouncementCRUD : IAnnouncementCRUD
    {
        private readonly IUnitOfWork unit;
        private readonly IMapper mapper;

        public AnnouncementCRUD(IUnitOfWork unit,

           IMapper mapper
            )
        {
            this.unit = unit;
            this.mapper = mapper;
        }
        public async Task<ApiResult<AnnouncementDto>> EditAnnouncement(EditAnnouncementDto dto)
        {
            var result = new ApiResult<AnnouncementDto> { Status = true, Message = Messages.EngOK };
            try
            {
                var Announcement = await unit.Announcement.GetByIdAsync( dto.Id);
                Announcement = mapper.Map<EditAnnouncementDto, Announcement>(dto, Announcement);
                await unit.CompleteAsync();
                Announcement = await unit.Announcement.GetByIdAsync( Announcement.Id);
                result.Data = mapper.Map< AnnouncementDto>( Announcement);
            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;

        }
        public async Task<ApiResult<AnnouncementDto>> AddAnnouncement(AddAnnouncementDto dto)
        {
            var result = new ApiResult<AnnouncementDto> { Status = true, Message = Messages.EngOK };
            try
            {
                var Announcement = mapper.Map< Announcement>(dto);
                await unit.Announcement.AddAsync(Announcement);
                await unit.CompleteAsync();
                Announcement = await unit.Announcement.GetByIdAsync( Announcement.Id);
                result.Data = mapper.Map<AnnouncementDto>(Announcement);


            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;

        }

        public async Task<BaseApiResult> DeleteAnnouncement(BaseByIntDto dto)
        {
            var result = new BaseApiResult { Status = true, Message = Messages.EngOK };
            try
            {
                var Announcement = await unit.Announcement.GetByIdAsync(dto.Id);

                if (Announcement== null )
      
                {
                    result.Error(Errors.EntityNotFound);
                    return result;
                }

                unit.Announcement.Remove(Announcement);

                await unit.CompleteAsync();



            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;
        }


        public async Task<ApiResult<AnnouncementDto>> GetAnnouncement()
        {
            var result = new ApiResult<AnnouncementDto> { Status = true, Message = Messages.EngOK };
            try
            {
                var resp = await unit.Announcement.GetAllAsync();
                result.Data = resp.Any()?  mapper.Map<AnnouncementDto>(resp.FirstOrDefault()) : null;
            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;
        }

        
        public async Task<ApiPageResult<AnnouncementDto>> GetByFilter(BaseGetByPageDto dto)
        {
            var result = new ApiPageResult<AnnouncementDto> { Status = true, Message = Messages.EngOK };
            try
            {
                IQueryable<Announcement> query =  unit.Announcement.GetByFilter(dto);
                var paged = new Paged<Announcement, AnnouncementDto>(query, dto, mapper.Map<AnnouncementDto>);
                result.Data = await paged.GetItmesAsync();
                result.PageDto = paged.GetPageDto();
            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;
        }

        
    }


}
