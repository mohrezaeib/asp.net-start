﻿using DomainCore.Dto.Base;
using DomainCore.DTO.AppMore;
using DomainCore.DTO.Base;
using System.Threading.Tasks;

namespace DomainServices.Contracts
{
    public interface IAnnouncementCRUD : IApplicationService
    {
        Task<ApiResult<AnnouncementDto>> AddAnnouncement(AddAnnouncementDto dto);
        Task<BaseApiResult> DeleteAnnouncement(BaseByIntDto dto);
        Task<ApiResult<AnnouncementDto>> EditAnnouncement(EditAnnouncementDto dto);
        Task<ApiPageResult<AnnouncementDto>> GetByFilter(BaseGetByPageDto dto);
        Task<ApiResult<AnnouncementDto>> GetAnnouncement();
    }

}
