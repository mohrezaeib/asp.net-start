﻿
using Atlantis.DomainServices.Mapping;
using DomainCore.Dto.Base;
using DomainCore.Dto.General;
using DomainCore.DTO.AppMore;
using DomainCore.DTO.Base;

using System;
using System.Collections.Generic;
using System.Drawing;

using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility.Tools.General;

namespace DomainServices.Contracts
{
    public interface ISocialCRUD : IApplicationService
    {
        Task<ApiResult<SocialDto>> EditSocial(EditSocialDto dto);
        Task<ApiListResult<SocialDto>> EditSocialByList(List<EditSocialDto> dto);
        Task<ApiResult<SocialDto>> AddSocial(AddSocialDto dto);
        Task<BaseApiResult> DeleteSocial(BaseByIntDto dto);
        Task<ApiListResult<SocialDto>> GetAllSocial( );


       

    }

}
