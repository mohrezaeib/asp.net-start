﻿
using Atlantis.DomainServices.Mapping;
using DomainCore.Dto.Base;
using DomainCore.Dto.General;
using DomainCore.DTO.AppMore;
using DomainCore.DTO.Base;



using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility.Tools.General;

namespace DomainServices.Contracts
{
    public interface IFaQCRUD : IApplicationService
    {
        Task<ApiResult<FaQShortDto>> EditFaQ(EditFaQDto dto);
        Task<ApiResult<FaQShortDto>> AddFaQ(AddFaQDto dto);
        Task<BaseApiResult> DeleteFaQ(BaseByIntDto dto);
        Task<ApiListResult<FaQShortDto>> GetAllFaQ( );
        Task<ApiListResult<FaQDto>> GetAllFaQWithdetail();
        Task<ApiListResult<FaQDto>> GetFaQByCategory(BaseByIntDto dto);



    } 

}
