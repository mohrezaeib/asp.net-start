﻿
using Atlantis.DomainServices.Mapping;
using AutoMapper;
using DomainCore.Dto.Base;
using DomainCore.Dto.General;
using DomainCore.DTO.AppMore;
using DomainCore.DTO.Base;
using DomainCore.Entities.AppMore;
using DomainCore.Enums;
using DomainCore.General;
using DomainData.Contracts;
using DomainServices.Contracts;
using DomainServices.DtoService.Profiles;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Drawing;

using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility.Tools.General;
using static DomainServices.DtoService.Profiles.GeneralProfile;

namespace DomainServices.Services
{
    public class FaQCRUD : IFaQCRUD
    {
        private readonly IUnitOfWork unit;
        private readonly IMapper mapper;

        public FaQCRUD(IUnitOfWork unit,

           IMapper mapper
            )
        {
            this.unit = unit;
            this.mapper = mapper;
        }
        public async Task<ApiResult<FaQShortDto>> EditFaQ(EditFaQDto dto)
        {
            var result = new ApiResult<FaQShortDto> { Status = true, Message = Messages.EngOK };
            try
            {
                var FaQ = await unit.FaQ.GetByIdAsync(dto.Id);
                FaQ = mapper.Map<EditFaQDto, FaQ>(dto, FaQ);
                await unit.CompleteAsync();
                FaQ = await unit.FaQ.GetByIdAsync(FaQ.Id);
                result.Data = mapper.Map< FaQShortDto>( FaQ);
            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;

        }
        public async Task<ApiResult<FaQShortDto>> AddFaQ(AddFaQDto dto)
        {
            var result = new ApiResult<FaQShortDto> { Status = true, Message = Messages.EngOK };
            try
            {
                var FaQ = mapper.Map< FaQ>(dto);
                await unit.FaQ.AddAsync(FaQ);
                await unit.CompleteAsync();
                FaQ = await unit.FaQ.GetByIdAsync(FaQ.Id);
                result.Data = mapper.Map<FaQShortDto>(FaQ);


            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;

        }

        public async Task<BaseApiResult> DeleteFaQ(BaseByIntDto dto)
        {
            var result = new BaseApiResult { Status = true, Message = Messages.EngOK };
            try
            {
                var FaQ = await unit.FaQ.GetByIdAsync(dto.Id);
                if (FaQ== null )
      
                {
                    result.Error(Errors.EntityNotFound);
                    return result;
                }
                unit.FaQ.Remove(FaQ);

                await unit.CompleteAsync();



            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;
        }


        public async Task<ApiListResult<FaQShortDto>> GetAllFaQ()
        {
            var result = new ApiListResult<FaQShortDto> { Status = true, Message = Messages.EngOK };
            try
            {
                var resp = await unit.FaQ.GetAllAsync();
                result.Data = resp.Select(p=> mapper.Map<FaQShortDto>(p)).ToList();
            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;
        }
        
        public async Task<ApiListResult<FaQDto>> GetAllFaQWithdetail()
        {
            var result = new ApiListResult<FaQDto> { Status = true, Message = Messages.EngOK };
            try
            {
                var resp = await unit.FaQ.GetAllAsync();
                result.Data = resp.Select(p=> mapper.Map<FaQDto>(p)).ToList();
            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;
        }
        public async Task<ApiListResult<FaQDto>> GetFaQByCategory(BaseByIntDto dto)
        {
            var result = new ApiListResult<FaQDto> { Status = true, Message = Messages.EngOK };
            try
            {
                var resp = await unit.FaQ.GetAllAsync();
                result.Data = resp.Where(p=> p.CategoryId == dto.Id).Select(p=> mapper.Map<FaQDto>(p)).ToList();
            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;
        }

        
    }


}
