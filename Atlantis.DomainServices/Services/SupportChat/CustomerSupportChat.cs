﻿
using Atlantis.DomainServices.Mapping;
using AutoMapper;
using DomainCore.Dto.Base;
using DomainCore.Dto.General;
using DomainCore.DTO.AppMore;
using DomainCore.DTO.Base;
using DomainCore.DTO.SupportChat;
using DomainCore.Entities;
using DomainCore.Entities.AppMore;
using DomainCore.Entities.SupportChat;
using DomainCore.Enums;
using DomainCore.General;
using DomainData.Contracts;
using DomainServices.Contracts;
using DomainServices.DtoService.Profiles;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.Drawing;

using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility.Tools.General;
using static DomainServices.DtoService.Profiles.GeneralProfile;

namespace DomainServices.Services
{
    public class CustomerSupportChat : ICustomerSupportChat
    {
        private readonly IUnitOfWork unit;
        private readonly IMapper mapper;
        private readonly ICurrentUser currentUser;

        public CustomerSupportChat(IUnitOfWork unit,

           IMapper mapper,
           ICurrentUser currentUser
            )
        {
            this.unit = unit;
            this.mapper = mapper;
            this.currentUser = currentUser;
        }
        public async Task<ApiResult<Message>> SendMessage(SendMessageDto dto)
        {
            var result = new ApiResult<Message> { Status = true, Message = Messages.EngOK };
            try
            {
                var message = mapper.Map<Message>(dto);
                await unit.Message.AddAsync(message);
                await unit.CompleteAsync();
                message = await unit.Message.GetByIdAsync( message.Id);

                result.Data = message;
            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;
        }

        public async Task<ApiResult<StartSupportChatDto>> StartSupportChat()
        {
            var result = new ApiResult<StartSupportChatDto> { Status = true, Message = Messages.EngOK };
            result.Data = new StartSupportChatDto();
            try
            {

                var chat =await  unit.Chat.GetFirstOrDefaultAsync(p => p.CustomerId == currentUser.UserId);
                if (chat == null) {
                    chat = new Chat() { 
                    CustomerId = currentUser.UserId.Value,
                    };
                    await unit.Chat.AddAsync(chat);
                    await unit.CompleteAsync();

                }


                result.Data.Chat = mapper.Map<ChatDto>(chat);
            }
            catch (Exception e)
            {
                result.Error(e);

            }


            return result;
        }
    }


}
