﻿
using Atlantis.DomainServices.Mapping;
using DomainCore.Dto.Base;
using DomainCore.Dto.General;
using DomainCore.DTO.AppMore;
using DomainCore.DTO.Base;
using DomainCore.DTO.SupportChat;
using DomainCore.Entities.SupportChat;

using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility.Tools.General;

namespace DomainServices.Contracts
{
    public interface IAdminSupportChat : IApplicationService
    {
        //  Task<ApiResult<StartSupportChatDto>> StartSupportChat();
        // Task<ApiResult<AboutUsDto>> AddAboutUs(AddAboutUsDto dto);
        //Task<BaseApiResult> DeleteAboutUs(BaseByIntDto dto);
        Task<ApiResult<Message>> SendMessage(SendMessageDto dto);
        Task<ApiPageResult<ChatDto>> GetChatByFilter(GetChatByFilterDto dto);
        Task<ApiResult<Chat>> MarkChatAsRead(BaseByGuidDto dto);






    }

}
