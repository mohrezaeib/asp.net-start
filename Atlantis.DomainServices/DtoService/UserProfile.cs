﻿using Atlantis.DomainServices.Mapping;
using AutoMapper;
using DomainCore.AppUsers;
using DomainCore.Dto.General;
using DomainCore.Dto.User;

using DomainCore.Entities;
using DomainCore.General;
using DomainServices.Services;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Utility.Tools.General;
using DomainCore.Enums;
using AutoMapper.EquivalencyExpression;
using DomainCore.DTO.Base;

namespace DomainServices.DtoService.Profiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {

            CreateMap<AddUserFileDto, UserFile>().ConvertUsing((src, dst, context) => new UserFile { DocumentId = src.DocumentId, Title = src.Title, CreatedAt = Agent.Now });
            CreateMap<UserFile, DocumentDto>().ConvertUsing((src, dst, context) => context.Mapper.Map<DocumentDto>(src.Document));
            CreateMap<UserFile, UserFileDto>().ConvertUsing((src, dst, context) => new UserFileDto { Id = src.Document.Id, Title = src.Title, Type = src.Document.DocumentType, Url = AdminSettings.Root + src.Document.Url, Name = src.Document.Name });
            CreateMap<DomainCore.Enums.Roles, UserRole>().ConvertUsing((src, dst, context) => new UserRole { RoleId = src, CreatedAt = Agent.Now });
            CreateMap<UserRole, RoleDto>().ConvertUsing((src, dst, context) => new RoleDto { RoleId = src.RoleId, Name = src.Role?.Name,  });
            CreateMap<AddUserRoleDto, UserRole>()
                .ForAllMembers(opt => opt.Condition((src, dest, sourceMember) => sourceMember != null));
            ;
            CreateMap<BaseGetByPageDto, GetUserNotificationsByFilterDto>().ReverseMap().ForAllOtherMembers(opt => opt.IgnoreSourceWhenDefault());
            CreateMap<UserSetting, UserSettingDto>();
            CreateMap<UserSetting, UserSetting>();
            CreateMap<EditUserSettingDto, UserSetting>()
               .ForAllMembers(opt => opt.Condition((src, dest, sourceMember) => sourceMember != null));
            ;
            CreateMap<string, Email>().ConvertUsing((src, dst, context) => new Email(src) );
            CreateMap<Email ,string>().ConvertUsing((src, dst, context) => src?.Address );  
            CreateMap<string, Mobile>().ConvertUsing((src, dst, context) => new Mobile(src) );
            CreateMap<Mobile, string>().ConvertUsing((src, dst, context) => src?.FullNumber );
         
            CreateMap<AddUserNotificationDto, UserNotification>().ForAllOtherMembers(opt => opt.IgnoreSourceWhenDefault());
            CreateMap<UserNotification, UserNotificationDto>().ForAllOtherMembers(opt => opt.IgnoreSourceWhenDefault());
            CreateMap<Notification, NotificationDto>().ForAllOtherMembers(opt => opt.IgnoreSourceWhenDefault());
            ;
           

            CreateMap<User, UserDto>()
                // .ForMember(dest => dest.ProfileImage, opt => opt.MapFrom(src => DtoBuilder.CreateDocumentDto(src.ProfileImage)))
                .ForMember(dest => dest.IsNewUser, opt => opt.MapFrom(src => src.IsNewUser()))
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Roles, opt => opt.MapFrom(src => src.UserRole))
                .ForMember(dest => dest.Documents,
                         opt =>
                         {
                             opt.PreCondition(src => src.Documents != null);
                             opt.MapFrom(src => src.Documents.Where(p => p.Document != null).ToList());
                         })
                .ForMember(dest => dest.BirthdayDateTime, opt => opt.MapFrom(src => src.Birthday.HasValue ? src.Birthday.Value : new DateTime()))
                .ForMember(dest => dest.Birthday, opt => opt.MapFrom(src => src.Birthday.HasValue ? src.Birthday.Value.Date.ToShortDateString() : null))
                .ForMember(dest => dest.HasPassword, opt => opt.MapFrom(src => !String.IsNullOrEmpty(src.Password)))
                .ForAllMembers(opt => opt.Condition((src, dest, sourceMember) => sourceMember != null));
            ;
            CreateMap<User, UserShortDto>()
                //.ForMember(dest => dest.ProfileImage, opt => opt.MapFrom(src => DtoBuilder.CreateDocumentDto(src.ProfileImage)))
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.Id))
                .ForAllMembers(opt => opt.Condition((src, dest, sourceMember) => sourceMember != null));
            ;
            CreateMap<AddUserDto, User>().ForAllOtherMembers(opt => opt.IgnoreSourceWhenDefault());
            ;

            
            CreateMap<AddUserByAdminDto, User>()
                .ForMember(dest => dest.ProvinceId,
                        opt =>
                        {
                            opt.PreCondition(src => src.ProvinceId != null);
                            opt.MapFrom(src => src.ProvinceId);

                        })
                   .ForMember(dest => dest.UserRole,
                        opt =>
                        {
                            opt.PreCondition(src => src.Roles != null);
                            opt.MapFrom(src => src.Roles);

                        })
                   
            .ForAllOtherMembers(opt => opt.IgnoreSourceWhenDefault());

            ;
            CreateMap<EditProfileByAdminDto, User>()
                .EqualityComparison((src, dest) => src.UserId == dest.Id)
                .BeforeMap((src, dest) => dest.UserRole.Where(p => src.Roles != null && !src.Roles.Any(q => q.RoleId == p.RoleId )).ToList().ForEach(p => dest.UserRole.Remove(p)))

                   .ForMember(dest => dest.ProvinceId,
                        opt =>
                        {
                            opt.PreCondition(src => src.ProvinceId != null);
                            opt.MapFrom(src => src.ProvinceId);

                        })
                   .ForMember(dest => dest.UserRole,
                        opt =>
                        {
                            opt.PreCondition(src => src.Roles != null);
                            opt.MapFrom(src => src.Roles);

                        })

                   .ForAllOtherMembers(opt => opt.IgnoreSourceWhenDefault()); 

            ;
            CreateMap<EditProfileDto, User>()
                .EqualityComparison((src, dest) => src.UserId == dest.Id)

               .ForMember(dest => dest.ProvinceId,
                        opt =>
                        {
                            opt.PreCondition(src => src.ProvinceId != null);
                            opt.MapFrom(src => src.ProvinceId);

                        })
               .ForAllOtherMembers(opt => opt.IgnoreSourceWhenDefault());

            ;



        }

    }

  
    }


