﻿using Atlantis.DomainServices.Mapping;
using AutoMapper;
using DomainCore.Dto.General;
using DomainCore.Dto.User;
using DomainCore.DTO.Slider;
using DomainCore.Entities.General;
using DomainCore.General;
using DomainServices.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utility.Tools.General;

namespace DomainServices.DtoService.Profiles
{
    public class SliderProfile : Profile 
    {
       

        public SliderProfile() : base()
        {
            CreateMap<AddSliderDto, Slider>(MemberList.Source);
            CreateMap<EditSliderDto, Slider>();
            CreateMap<Slider, SliderDto>();  

        }

    }
   

}
