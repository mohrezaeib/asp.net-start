﻿using AutoMapper;
using DomainCore.Dto.General;

using DomainCore.General;
using DomainServices.Services;
using System;
using System.Collections.Generic;
using System.Text;
using Utility.Tools.General;

namespace DomainServices.DtoService.Profiles
{
    public class GeneralProfile : Profile 
    {
        public GeneralProfile()
        {
           
            CreateMap<Document, DocumentDto>()
                .ForMember(dest => dest.Url, opt => opt.MapFrom(src => AdminSettings.Root+ src.Url))
                .ForMember(dest => dest.Type, opt => opt.MapFrom(src => src.DocumentType))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForAllMembers(opt => opt.Condition((src, dest, sourceMember) => sourceMember != null)); ;
            
            CreateMap<City, CityDto>().ForAllMembers(opt => opt.Condition((src, dest, sourceMember) => sourceMember != null)); ;
            //.ForMember(dest => dest.ProfileImageId, opt => opt.MapFrom(src => src.ProfileImageId));

             
            CreateMap<Province, ProvinceDto>().ForAllMembers(opt => opt.Condition((src, dest, sourceMember) => sourceMember != null)); ;
            //.ForMember(dest => dest.ProfileImageId, opt => opt.MapFrom(src => src.ProfileImageId));

            CreateMap<Update, UpdateDto>()
           .ForAllOtherMembers(opt => opt.IgnoreSourceWhenDefault());




        }

    }
    public class Test
    {
        public int id { get; set; }
        public string Title { get; set; }
    }
    public class TestDto
    {
        public int id { get; set; }
        public string Title { get; set; }
    }
}
