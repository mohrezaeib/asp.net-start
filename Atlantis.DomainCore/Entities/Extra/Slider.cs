﻿using DomainCore.Base;
using DomainCore.General;
using System;
using System.Collections.Generic;
using System.Text;

namespace DomainCore.Extra
{
    public class Slider : Entity<Guid>
    {
        public virtual Document Document { get; set; }
        public Guid DocumentId { get; set; }
        public string Address{ get; set; }
        
    }
}
