﻿using DomainCore.Base;
using System;

namespace DomainCore.Entities.AppMore
{
    public class Announcement : Entity<int>
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }

    }
  
}
