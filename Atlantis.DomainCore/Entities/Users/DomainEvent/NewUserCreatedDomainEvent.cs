﻿using System;
using DomainCore.Entities.BaseEntities;

namespace DomainCore.AppUsers
{
    public class NewUserCreatedDomainEvent : DomainEvent , IDispatchRightAfterSaveChanges
    {
    

        public User User { get; set; }
        public NewUserCreatedDomainEvent(User user)
        {
            User = user;
        }
    }
    public class NewUserIsCreatingDomainEvent : DomainEvent , IDispatchRightBeforeSaveChanges
    {
        public User User { get; set; }
        public NewUserIsCreatingDomainEvent(User user)
        {
            User = user;
        }
    }
}
