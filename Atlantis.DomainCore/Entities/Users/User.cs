﻿
using DomainCore.Base;
using Utility.Tools.Auth;
using DomainCore.Entities;
using DomainCore.General;
using System;
using System.Collections.Generic;
using System.Text;
using DomainCore.Enums;
using System.ComponentModel.DataAnnotations;

using DomainCore.Entities.SupportChat;

using System.Linq;
using DomainCore.Entities.BaseEntities;
using PhoneNumbers;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace DomainCore.AppUsers
{
 

    public class User : Entity<Guid>
    {
        public User() : base()
        {
            
        }
        public static User Create()
        {
            var entity = new User();
            entity.AddDomainEvent(new NewEntityCreatedDomainEvent<User>(entity));
            entity.AddDomainEvent(new NewEntityCreatedDispatchBeforeSaveChangesDomainEvent<User>(entity));
            return entity;
        }
        public string Name { get; set; }
        public string FamilyName { get; set; }
      
       // public string Email { get { return EmailObject?.Address;  } set { EmailObject = new Email(value); } }
        public virtual Email Email { get;  private set; }

        // public string Mobile { get { return MobileObject?.FullNumber; } set { MobileObject = new Mobile(value); } }
        public virtual Mobile Mobile { get ; private set; }
        public void SetMobile(Mobile mobile)
        {
            Mobile = mobile;
        }
        public void SetEmail(Email email)
        {
            Email = email;
        }
        public DateTime? Birthday { get; set; }
        public Genders? Gender { get; set; }
  
        [DataType(DataType.Password)]
        public string Password { get; set; }
        public UserStatus Status { get; set; }


        public string Bio { get; set; }
        public virtual Province Province { get; set; }
        public int? ProvinceId { get; set; }
        public virtual string City { get; set; }
        public virtual Document ProfileImage { get; set; }
        public Guid? ProfileImageId { get; set; }
        public virtual UserSetting UserSetting { get; set; }
        public string Salt { get; set; }
        public string Address { get; set; }
        public string StreetAddress { get; set; }
        public string PostalCode { get; set; }
        public string GiftCode { get; set; }

        public virtual List<UserFile> Documents { get; set; }
        public  Guid? SalesManId { get; set; }
        public virtual List<Device> Device { get; set; }
        public virtual List<UserNotification> Notification { get; set; }

        public virtual List<UserRole> UserRole { get; set; }




        public void SetPassword(string password, IEncrypter encrypter)
        {
            Salt = encrypter.GetSalt();
            Password = encrypter.GetHash(password, Salt);
        }

        public bool Search(string key)
        {
            return (key == null || ((Email.Address != null && Email.Address.Contains(key)) ||
                                    (Name != null && Name.Contains(key)) ||
                                    (FamilyName != null && FamilyName.Contains(key)) ||
                                    (Mobile != null && Mobile.FullNumber.Contains(key))
                    ));
        }




        public bool ValidatePassword(string password, IEncrypter encrypter) =>
            Password.Equals(encrypter.GetHash(password, Salt));


        public bool? IsNewUser()
        {
            
            return null;
        }
    }

    public class Mobile : ValueObject<Mobile>
    {
        private Mobile()
        {
            Number = "";
            CountryCode = "";
            //Avoid Null Exception
            FullNumber = "";
        }
       // public Guid? UserId { get; set; }
        public string FullNumber { get; private  set; }
        public string Number { get; private set; }
        public string CountryCode { get; private set; }
        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return Number;
            yield return CountryCode;
        }
        public Mobile(string number, string countryCode)
        {
            Number = number;
            CountryCode = countryCode;
            FullNumber = CountryCode + Number;
        }
        public Mobile(string fullNumber)
        {
            try
            {
                PhoneNumberUtil phoneUtil = PhoneNumberUtil.GetInstance();

                // phone must begin with '+'
                PhoneNumber numberProto = phoneUtil.Parse(fullNumber, "");
                CountryCode = numberProto.CountryCode.ToString();
                Number = numberProto.NationalNumber.ToString();
                FullNumber = CountryCode + Number;

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        
        }
    }
    public class Email : ValueObject<Email>
    {
        private Email()
        {

        }
        //public Guid? UserId { get; set; }
        public string Address { get; private set; }
        public bool isConfirmed { get;  set; }
        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return Address.ToLower();
        }
        public Email(string address)
        {
            Address = address.ToLower();
            isConfirmed = false;
        }
         

        public void Confirm()
        {
            isConfirmed = true;
        }
    }

  
}
