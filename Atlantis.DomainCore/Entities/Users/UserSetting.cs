﻿using DomainCore.AppUsers;
using DomainCore.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace DomainCore.Entities
{
    public class UserSetting
    {
        public bool SendEmail { get; set; }
        public bool SendSMS { get; set; }
        public bool SendNotification { get; set; }
        public Guid UserId { get; set; }
        public User User { get; set; }

    }
}
