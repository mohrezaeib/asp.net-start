﻿using DomainCore.Base;
using DomainCore.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace DomainCore.AppUsers
{
    public class Role : Entity<Roles>
    {
        public Roles Id { get; set; }
        public string Name { get; set; }
        public virtual List<UserRole> UserRole { get; set; }
        public delegate int Comparisonee<in T>(T left, T right);
    }
}
