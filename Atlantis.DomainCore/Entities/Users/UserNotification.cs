﻿using DomainCore.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace DomainCore.AppUsers
{
    public class UserNotification: Entity<Guid>
    {
        //
        // Summary:
        //     Gets or sets the title of the notification.
        public string Title { get; set; }
        //
        // Summary:
        //     Gets or sets the body of the notification.
        public string Body { get; set; }
        //
        // Summary:
        //     Gets or sets the URL of the image to be displayed in the notification.
        public string ImageUrl { get; set; }
        public bool IsSeen { get; set; }
        public Guid UserId{ get; set; }
        public virtual User User { get; set; }
    }
    public class Notification: Entity<Guid>
    {
        //
        // Summary:
        //     Gets or sets the title of the notification.
        public string Title { get; set; }
        //
        // Summary:
        //     Gets or sets the body of the notification.
        public string Body { get; set; }
        //
        // Summary:
        //     Gets or sets the URL of the image to be displayed in the notification.
        public string ImageUrl { get; set; }
     
    }
}
