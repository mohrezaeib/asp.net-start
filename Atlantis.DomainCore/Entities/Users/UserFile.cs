﻿using DomainCore.Base;
using DomainCore.Enums;
using DomainCore.General;
using System;

namespace DomainCore.AppUsers
{
    public class UserFile : Entity<Guid>
    {
        public Guid UserId { get; set; }
        public virtual User User { get; set; }
        public Guid DocumentId { get; set; }
        public  virtual Document Document { get; set; }
        public string Title { get; set; }

    }
}
