﻿using System;
using System.Collections.Generic;
using System.Text;
using DomainCore.AppUsers;
using DomainCore.Base;

namespace DomainCore.General
{
    public class ActiveCode : Entity<Guid>
    {
        public string Code { get; set; }
        public Mobile Mobile { get; set; }
        public Email Email { get; set; }

    } 
  
}
