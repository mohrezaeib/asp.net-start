﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DomainCore.Base;

using MediatR;


namespace DomainCore.Entities.BaseEntities
{
    public class DomainEvent : INotification
    {
        public DateTime CreatedAt { get; set; }
        public DomainEvent()
        {
            CreatedAt = DateTime.Now;
        }
    }
   
    public interface IDispatchRightBeforeSaveChanges
    {

    } 
    public interface IDispatchRightAfterSaveChanges
    {

    }
    public interface ISaveInDBAndDispatchSomeTimeLater
    {
        public DateTime DispatchTime { get; set; }
    }
    //public class SavedInDbDomainEvents : Entity<Guid>
    //{
    //    public DateTime DispatchTime { get; set; }
    //}

    public class NewEntityCreatedDomainEvent<Entity>: DomainEvent where Entity : BaseEntity
    {
        public Entity Source { get; set; }
        public NewEntityCreatedDomainEvent(Entity entity)
        {
            Source = entity;    
        }
    }

    public class NewEntityCreatedDispatchBeforeSaveChangesDomainEvent<Entity> : DomainEvent , IDispatchRightBeforeSaveChanges where Entity : BaseEntity
    {
        public Entity Source { get; set; }
        public NewEntityCreatedDispatchBeforeSaveChangesDomainEvent(Entity entity)
        {
            Source = entity;
        }
    }
}
