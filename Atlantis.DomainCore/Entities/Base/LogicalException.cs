﻿using DomainCore.Enums;
using System;
using System.Collections.Generic;
using System.Text;
using Utility.Tools.General;

namespace DomainCore.Entities.Base
{
    public class LogicalException : Exception
    {
        public LogicalException(string  errorMessage)
        {
            ErrorMessage = errorMessage;
        }
        public LogicalException(string errorMessage , string extraDetail)
        {
            ErrorMessage = errorMessage;
            ExtraDeatail = extraDetail;
        }
        public string ErrorMessage { get; set; }
        public string ExtraDeatail { get; set; }

    }
}
