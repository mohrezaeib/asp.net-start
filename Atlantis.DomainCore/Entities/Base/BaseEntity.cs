﻿using DomainCore.Entities.BaseEntities;
using MediatR;
using System;
using System.Collections.Generic;
using Utility.Tools.General;

namespace DomainCore.Base
{
    public class Entity<ID> : BaseEntity, IEntity<ID>
    {

       

        public new ID Id { get; set; }


     
    }
    public abstract class BaseEntity : IAuditable, IEntity
    {
        public virtual object Id { get; set; }
        public bool isDeleted { get; set; }
        public long? UpdatedAt { get; set; }
        public Guid? CreatedBy { get; set; }
        public Guid? UpdatedBy { get; set; }
        public long? CreatedAt { get; set; }
        public DateTime CreatedAtDate { get; set; }
        public DateTime? UpdatedAtDate { get; set; }

        public BaseEntity()
        {
            CreatedAt = 0;
            CreatedAtDate = DateTime.MinValue;
        }
      
        private List<INotification> _domainEvents;

        public List<INotification> DomainEvents => _domainEvents;


        public void AddDomainEvent(INotification eventItem)

        {

            _domainEvents = _domainEvents ?? new List<INotification>();

            _domainEvents.Add(eventItem);

        }

        public void RemoveDomainEvent(INotification eventItem)

        {

            if (_domainEvents is null) return;

            _domainEvents.Remove(eventItem);

        }
        public void ClearDomainEvents()
        {
            _domainEvents?.Clear();
        }


    }
    public interface IEntity
    {
        public  object  Id { get; set; }
        public List<INotification> DomainEvents { get; }
        public void AddDomainEvent(INotification eventItem);
        public void RemoveDomainEvent(INotification eventItem);
        public void ClearDomainEvents();

    }

    public interface IAggregateRoot
    {

    }
    public interface IEntity<ID> : IEntity
    {
        public ID Id { get; set; }
    }

}
