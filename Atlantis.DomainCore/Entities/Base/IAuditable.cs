﻿using System;

namespace DomainCore.Base
{
    public interface  IAuditable 
    {
        public bool isDeleted { get; set; } 
        public long? UpdatedAt { get; set; }
        public Guid? CreatedBy { get; set; }
        public Guid? UpdatedBy { get; set; }
        public long? CreatedAt { get; set; }
        public DateTime CreatedAtDate { get; set; }
        public DateTime? UpdatedAtDate { get; set; }



    }
}
