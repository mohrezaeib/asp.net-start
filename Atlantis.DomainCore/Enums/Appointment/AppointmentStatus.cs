﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DomainCore.Enums
{
    public enum AppointmentStatus
    {
        [Display(Description = "BookedButNotConfirmed")]
        BookedButNotConfirmed = 1,
        [Display(Description = "Confirmed")]
        Booked = 2,
        [Display(Description = "Attended")]
        Complete = 3,
        [Display(Description = "NoShow")]
        NoShow = 4,
        [Display(Description = "CancelledByUser")]
        CancelledByUser = 5,
        [Display(Description = "CancelledByAdmin")]
        CancelledByAdmin = 6,
        [Display(Description = "RescheduledByUser")]
        RescheduledByUser = 7,
        [Display(Description = "RescheduledByAdmin")]
        RescheduledByAdmin = 8,

    }
   
}
