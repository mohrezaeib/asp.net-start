﻿using System.ComponentModel.DataAnnotations;

namespace DomainCore.Enums
{
    public enum ReadyToBookStatus
    {
        [Display(Description = "Has not  an Active  Service of this type ")]
        NoActiveService = 0,
        [Display(Description = "Has a  Service  Without Active Appointment")]
        ServiceWithoutActiveAppointment = 1,
        [Display(Description = "Has a service with  an  Active Appointment and it is ok")]
        ServiceWithAppointmentOk = 2,
        [Display(Description = "Has a  service with  an Active Appointment and  there are new body parts  that could be appended to that appointment ")]
        ServiceWithAppointmenCouldExtend = 3,
        [Display(Description = "Has a  service with  an Active Appointment but this appointment could not extended with the new body parts")]
        ServiceWithAppointmenCouldNotExtend = 4,




    }

}
