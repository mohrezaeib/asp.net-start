﻿using System.ComponentModel.DataAnnotations;

namespace DomainCore.Enums
{
    public enum UnitTimeStatus
    {
        [Display(Description = "Full")]
        Full = 1,
        [Display(Description = "Empty")]
        Empty = 2,
        

    }
}
