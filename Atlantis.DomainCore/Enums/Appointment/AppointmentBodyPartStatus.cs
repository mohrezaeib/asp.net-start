﻿using System.ComponentModel.DataAnnotations;

namespace DomainCore.Enums
{
    public enum AppointmentBodyPartStatus
    {
        [Display(Description = "Active")]
        Active = 1,
        [Display(Description = "Booked")]
        Booked = 2,
        [Display(Description = "Complete")]
        Complete = 3,
        [Display(Description = "Missed")]
        MissedBodyPart = 4,
        [Display(Description = "Deactive")]
        Deactive = 5,
        [Display(Description = "NoShow")]
        NoShow = 6,
    }


}
