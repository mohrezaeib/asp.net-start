﻿using System.ComponentModel.DataAnnotations;

namespace DomainCore.Enums
{
    public enum Status
    {
      
        [Display(Description = "فعال")]
        Active = 1,
        [Display(Description = "غیر فعال")]
        Deactive = 2,
        [Display(Description = "حذف شده")]
        Deleted = 3,
        [Display(Description = "رد شده")]
        Rejected = 4

    }



    }
