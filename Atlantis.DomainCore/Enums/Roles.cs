﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DomainCore.Enums
{
    public enum Roles
    {
        [Display(Description = "User")]
        User = 1,
        [Display(Description = "Admin")]
        Admin = 2,
        [Display(Description = "Clinic Manager")]
        ClinicManager = 3,
        [Display(Description = "Employee")]
        Employee = 4,

        [Display(Description = "Accountant")]
        Accountant = 5,
        [Display(Description = "Influencer")]
        SalesMan = 6,


    }

    public enum RefrenceType
    {
        [Display(Description = "User")]
        User = 1,
        [Display(Description = "Admin")]
        Influencer = 2,


    }
}
