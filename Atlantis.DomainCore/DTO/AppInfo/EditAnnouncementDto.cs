﻿namespace DomainCore.DTO.AppMore
{
    public class EditAnnouncementDto : AddAnnouncementDto
    {
        public int Id { get; set; }
    }

}
