﻿using DomainCore.DTO.Base;
using System;

namespace DomainCore.DTO.AppMore
{
    public class AnnouncementDto : BaseDto<int>
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
    }

}
