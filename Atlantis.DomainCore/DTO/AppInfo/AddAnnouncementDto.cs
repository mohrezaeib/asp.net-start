﻿using System;

namespace DomainCore.DTO.AppMore
{
    public class AddAnnouncementDto
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
    }

}
