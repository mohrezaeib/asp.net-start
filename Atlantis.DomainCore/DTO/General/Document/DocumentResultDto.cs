﻿
using DomainCore.Enums;
using System;
using System.Collections.Generic;

namespace DomainCore.Dto.General
{
    public class DocumentDto
    {
        public Guid Id { get; set; }
        public string Url { get; set; }
        public DocumentType Type { get; set; }
        public string Name { get; set; }

    }
    public class UserFileDto : DocumentDto
    {
        
        public string Title { get; set; }
    }
     public class AddUserFileDto 
    {
        
        public string Title { get; set; }
        public Guid DocumentId  { get; set; }
    }

}

