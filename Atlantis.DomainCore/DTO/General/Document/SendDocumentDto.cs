﻿using DomainCore.Enums;
using Enums;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace DomainCore.Dto.General
{
    public  class SendDocumentDto
    {
        public IFormFile File { get; set; }
        public DocumentType Type { get; set; }
        public string Name { get; set; }

        // public bool Watermark { get; set; }
    }
      public  class removebgDto
    {
      
        public string deviceId { get; set; }
        public string url { get; set; }

        // public bool Watermark { get; set; }
    }
}
