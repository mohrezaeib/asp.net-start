﻿using DomainCore.DTO.AppMore;
using DomainCore.DTO.Base;
using DomainCore.Enums;
using System;
using System.Collections.Generic;

namespace DomainCore.Dto.General
{
    public class SplashResultDto 
    {
        public UpdateDto Update { get; set; }
        public List<AnnouncementDto> Announcements { get; set; }
    }

    public class   UpdateDto : BaseDto<Guid>
    {
        public int? NewVersionCode { get; set; }
        public int? FromVersionCode { get; set; }
        public int? ToVersionCode { get; set; }
        public OS OS { get; set; }
        public string PkgName { get; set; }
        public bool IsForce { get; set; }
        public string Description { get; set; }
        public string Link { get; set; }
    }
}
