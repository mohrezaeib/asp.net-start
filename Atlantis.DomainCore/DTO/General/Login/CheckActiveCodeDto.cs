﻿
using DomainCore.Dto.Base;
using DomainCore.Dto.User;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DomainCore.Dto.General
{

    public class LoginDto
    {

        public string UserName { get; set; }
        public string Password { get; set; }
        public string PushId { get; set; }
    } 
   
    public class ForgotPasswordDto
    {
        public string Email { get; set; }

    }
    public class ResetPasswordDto
    {
        [Required(ErrorMessage = "Password is required")]
        public string Password { get; set; }
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
        public string Email { get; set; }
        public string Token { get; set; }
    }
    public class RegisterByEmailDto
    {
        public string Email { get; set; }

    }
    public class ConfirmEmailDto
    {
        public string Email { get; set; }
        public string Code { get; set; }
        public string PushId { get; set; }

    }
    public class ChangePasswordDto
    {
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }

    }

}
