﻿using System;
using System.Collections.Generic;

namespace DomainCore.Dto.User
{
    public class AddUserNotificationDto
    {
        //
        // Summary:
        //     Gets or sets the title of the notification.
        public string Title { get; set; }
        //
        // Summary:
        //     Gets or sets the body of the notification.
        public string Body { get; set; }
        //
        // Summary:
        //     Gets or sets the URL of the image to be displayed in the notification.
        public string ImageUrl { get; set; }
        public List<Guid> Users { get; set; }
    } 


}
