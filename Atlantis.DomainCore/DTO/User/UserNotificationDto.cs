﻿using DomainCore.DTO.Base;
using System;

namespace DomainCore.Dto.User
{
    public class UserNotificationDto :BaseDto<Guid>
    {
        //     Gets or sets the title of the notification.
        public string Title { get; set; }
        //
        // Summary:
        //     Gets or sets the body of the notification.
        public string Body { get; set; }
        //
        // Summary:
        //     Gets or sets the URL of the image to be displayed in the notification.
        public string ImageUrl { get; set; }
        public bool IsSeen { get; set; }
        public Guid UserId { get; set; }
    } 
    public class NotificationDto :BaseDto<Guid>
    {
        //     Gets or sets the title of the notification.
        public string Title { get; set; }
        //
        // Summary:
        //     Gets or sets the body of the notification.
        public string Body { get; set; }
        //
        // Summary:
        //     Gets or sets the URL of the image to be displayed in the notification.
        public string ImageUrl { get; set; }
      
    } 

}
