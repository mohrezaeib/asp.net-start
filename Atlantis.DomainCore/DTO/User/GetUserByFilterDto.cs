﻿using DomainCore.DTO.Base;
using DomainCore.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace DomainCore.Dto.User
{
    public class GetUsersByFilterDto : BaseGetByPageDto
    {
        public int? ProvinceId { get; set; }
        public string City { get; set; }
        public UserStatus? Status { get; set; }
        public Genders? Gender { get; set; }
        public List<Roles> Role { get; set; }
        public string Keyword { get; set; }
        public string KeywordEmail { get; set; }
        public string KeywordName { get; set; }
        public string KeywordPhone { get; set; }


        public DateTime? Date { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }

    }

    public class GetUserNotificationsByFilterDto : BaseGetByPageDto
    {
       
        public bool? IsSeen { get; set; }
        public Guid? UserId { get; set; }
     
        public string Keyword { get; set; }
       

        public DateTime? Date { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }

    }
    public class GetNotificationsByFilterDto : BaseGetByPageDto
    {
       
   
     
        public string Keyword { get; set; }
       

        public DateTime? Date { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }

    }
    public class GenerateFileNoDto 
    {

        public int ClinicId { get; set; }
        public Guid UserId { get; set; }

    } 
    public class NewFileNoDto 
    {

        public int ClinicId { get; set; }
        public Guid UserId { get; set; }
        public int Number { get; set; }

    }
}
