﻿using DomainCore.Entities.Base;
using System;



namespace DomainCore.Dto.Base
{
    public class BaseApiResult
    {
        public BaseApiResult()
        {
        }

        public bool Status { get; set; }
        public string Message { get; set; }
        public string ClientMessage { get; set; }
        public string ExtraDetail { get; set; }
      
        public BaseApiResult Error(Exception e)
        {
            Status = false;
            Message = e.Message;
            ClientMessage = "Sorry, A Server Error Occured.";
            if(e.GetType() == typeof(LogicalException) )
            {
                var logicalException = (LogicalException)e;
                ExtraDetail = logicalException.ExtraDeatail;
                Error(logicalException.ErrorMessage);
                
                return this;

            }
            ExtraDetail = e.ToString();
            return this;

        }
        public BaseApiResult Error( string errorMessage )
        {
            Status = false;
            Message =  errorMessage ;
            ClientMessage = errorMessage;
            return this;
        }
    }
}
