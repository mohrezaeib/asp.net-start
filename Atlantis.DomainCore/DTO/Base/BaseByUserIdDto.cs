﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DomainCore.DTO.Base
{
    public class BaseByUserIdDto
    {
        public Guid UserId { get; set; }
    }
}
