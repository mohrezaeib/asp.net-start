﻿
using DomainCore.Entities;
using DomainData.Contracts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using System.Threading.Tasks;

namespace EndPointWeb.Filters
{
    public class AuthorizeUser : ActionFilterAttribute
    {
        //public string Type { get; set; }
        public override async  void OnActionExecuting(ActionExecutingContext context)
        {
            IUnitOfWork unit = (IUnitOfWork)context.HttpContext.RequestServices.GetService(typeof(IUnitOfWork));
            ICurrentUser currentUser = (ICurrentUser)context.HttpContext.RequestServices.GetService(typeof(ICurrentUser));
            if (context.ActionArguments.TryGetValue("dto", out object value))
            {
                var identity = context.HttpContext.User.Identity as ClaimsIdentity;
                if (identity != null)
                {
                    var basedToken = context.HttpContext.Request.Headers["Authorization"].ToString();
                    if (basedToken != null && basedToken != "")
                    {
                        var token = basedToken.Split(' ')[1];
                        var handler = new JwtSecurityTokenHandler();
                        var jwtToken = handler.ReadToken(token) as JwtSecurityToken;
                        var userId = jwtToken.Claims.FirstOrDefault(p => p.Type == "sub").Value;

                        //No Async here !
                        currentUser.User =  unit.Users.GetByIdAsync( Guid.Parse(userId)).Result;


                        var dtoUserId = value?.GetType()
                            .GetProperties()?.FirstOrDefault(q => q.Name == "UserId" )?
                            .GetValue(value, null)?.ToString(); 
                       

                        //check if the user is the one with token
                        if(dtoUserId != null && dtoUserId != userId)
                        {
                            context.Result = new StatusCodeResult(401);

                        }


                    }
                    else
                        context.Result = new StatusCodeResult(401);
                }
                else
                    context.Result = new StatusCodeResult(401);
            }


        }
    }
}