﻿using System;
using Microsoft.AspNetCore.Mvc;

using DomainServices.Contracts;
using System.Threading.Tasks;
using DomainCore.Dto.General;
using DomainCore.Dto.Base;
using DomainCore.Dto.User;
using DomainCore.DTO.Base;
using API.Controllers.BaseControllers;
using DomainCore.DTO.Slider;
using Utility.Tools.EmailServies;
using Stripe;
using System.Collections.Generic;
using Utility.Tools.General;
using DomainData.Contracts;

namespace API.Controllers
{

    public class TestController : SimpleController
    {
        private readonly IGeneralService generalService;
        private readonly ICityService cityService;
        private readonly ISimpleEmailSender emailService;
        private readonly IUnitOfWork unit;
        private readonly IUserService userService;

        public TestController(
            IGeneralService generalService,
            ICityService cityService,
            ISimpleEmailSender emailService, IUnitOfWork unit,
            IUserService userService

            )
        {
            this.generalService = generalService;
            this.cityService = cityService;
            this.emailService = emailService;
            this.unit = unit;
            this.userService = userService;
        }


        [HttpPost]
        public async  Task<ApiResult<UserDto>>  AddUserAndTrigerZappier([FromBody] AddUserByAdminDto dto)
        {
           return await userService.AddUserByAdmin(dto);
        }
      

        
        //[ResponseCache()]
        //public async Task<ApiListResult<ProvinceDto>> GetProvinces()
        //{
        //    return await cityService.GetProvines();
        //}

        //[HttpGet]
        //public async Task<ApiListResult<CityDto>> GetByProvinceId([FromQuery] BaseByIntDto dto)
        //{
        //    return await cityService.GetByProvinceId(dto);
        //}



    }

}