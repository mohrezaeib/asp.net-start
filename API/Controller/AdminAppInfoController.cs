﻿//using System;
//using Microsoft.AspNetCore.Mvc;
//using Twilio.Clients;
//using Twilio.Rest.Api.V2010.Account;
//using Twilio.Types;
//using Twilio.TwiML;
//using DomainServices.Contracts;
//using System.Threading.Tasks;
//using DomainCore.Dto.General;
//using DomainCore.Dto.Base;
//using DomainCore.Dto.User;
//using API.Controllers.BaseControllers;
//using DomainCore.DTO.Base;
//using DomainCore.DTO.AppMore;
//using System.Collections.Generic;

//namespace API.Controllers
//{
    
//    public class AdminAppInfoController : SimpleController
//    {
//        private readonly ISocialCRUD socialCRUD;
//        private readonly IAboutUsCRUD AboutUsCRUD;
//        private readonly IFaQCRUD FaQCRUD;
//        private readonly IContactUsCRUD ContactUsCRUD;
//        private readonly IFaQCategoryCRUD FaQCategoryCRUD;
//        private readonly IAnnouncementCRUD AnnouncementCRUD;

//        public AdminAppInfoController(
//            ISocialCRUD  socialCRUD , 
//            IAboutUsCRUD aboutUsCRUD,
//            IFaQCRUD FaQCRUD, 
//            IContactUsCRUD contactUsCRUD,
//            IFaQCategoryCRUD faQCategoryCRUD,
//            IAnnouncementCRUD AnnouncementCRUD


//            )
//        {
//            this.socialCRUD = socialCRUD;
//            this.AboutUsCRUD = aboutUsCRUD;
//            this.FaQCRUD = FaQCRUD;
//            this.ContactUsCRUD = contactUsCRUD;
//            this.FaQCategoryCRUD = faQCategoryCRUD;
//            this.AnnouncementCRUD = AnnouncementCRUD;
//        }


//        #region
//        [HttpPost]
//        public async Task<ApiResult<SocialDto>> AddSocial([FromBody] AddSocialDto dto)
//        {
//            return await socialCRUD.AddSocial(dto);
//        }
//        [HttpPut]
//        public async Task<ApiResult<SocialDto>> EditSocial([FromBody] EditSocialDto dto)
//        {
//            return await socialCRUD.EditSocial(dto);
//        } 
//        [HttpPut]
//        public async Task<ApiListResult<SocialDto>> EditSocialByList([FromBody] List<EditSocialDto> dto)
//        {
//            return await socialCRUD.EditSocialByList(dto);
//        }
//        [HttpDelete]
//        public async Task<BaseApiResult> DeleteSocial([FromQuery] BaseByIntDto dto)
//        {
//            return await socialCRUD.DeleteSocial(dto);
//        }
       
//        [HttpGet]
//        public async Task<ApiListResult<SocialDto>> GetAllSocial()
//        {
//            return await socialCRUD.GetAllSocial();
//        }

//        #endregion
       
        
//        [HttpPost]
//        public async Task<ApiResult<FaQShortDto>> AddFaQ([FromBody] AddFaQDto dto)
//        {
//            return await FaQCRUD.AddFaQ(dto);
//        }
//        [HttpPut]
//        public async Task<ApiResult<FaQShortDto>> EditFaQ([FromBody] EditFaQDto dto)
//        {
//            return await FaQCRUD.EditFaQ(dto);
//        }
//        [HttpDelete]
//        public async Task<BaseApiResult> DeleteFaQ([FromQuery] BaseByIntDto dto)
//        {
//            return await FaQCRUD.DeleteFaQ(dto);
//        }
       
//        [HttpGet]
//        public async Task<ApiListResult<FaQShortDto>> GetAllFaQ()
//        {
//            return await FaQCRUD.GetAllFaQ();
//        }
//         [HttpGet]
//        public async Task<ApiListResult<FaQDto>> GetAllFaQWithdetail()
//        {
//            return await FaQCRUD.GetAllFaQWithdetail();
//        } 

//        [HttpGet]
//        public async Task<ApiListResult<FaQDto>> GetFaQByCategory ([FromQuery] BaseByIntDto dto)
//        {
//            return await FaQCRUD.GetFaQByCategory(dto);
//        }

//        #region
//        [HttpPost]
//        public async Task<ApiResult<AnnouncementDto>> AddAnnouncement([FromBody] AddAnnouncementDto dto)
//        {
//            return await AnnouncementCRUD.AddAnnouncement(dto);
//        }
//        [HttpPut]
//        public async Task<ApiResult<AnnouncementDto>> EditAnnouncement([FromBody] EditAnnouncementDto dto)
//        {
//            return await AnnouncementCRUD.EditAnnouncement(dto);
//        }
//        [HttpDelete]
//        public async Task<BaseApiResult> DeleteAnnouncement([FromQuery] BaseByIntDto dto)
//        {
//            return await AnnouncementCRUD.DeleteAnnouncement(dto);
//        }
       
//        [HttpGet]
//        public async Task<ApiPageResult<AnnouncementDto>> GetByFilter([FromQuery]  BaseGetByPageDto dto)
//        {
//            return await AnnouncementCRUD.GetByFilter( dto);
//        }
//        #endregion

//       #region
//        [HttpPost]
//        public async Task<ApiResult<FaQCategoryShortDto>> AddFaQCategory([FromBody] AddFaQCategoryDto dto)
//        {
//            return await FaQCategoryCRUD.AddFaQCategory(dto);
//        }
//        [HttpPut]
//        public async Task<ApiResult<FaQCategoryShortDto>> EditFaQCategory([FromBody] EditFaQCategoryDto dto)
//        {
//            return await FaQCategoryCRUD.EditFaQCategory(dto);
//        }
//        [HttpDelete]
//        public async Task<BaseApiResult> DeleteFaQCategory([FromQuery] BaseByIntDto dto)
//        {
//            return await FaQCategoryCRUD.DeleteFaQCategory(dto);
//        }
       
//        [HttpGet]
//        public async Task<ApiListResult<FaQCategoryShortDto>> GetAllFaQCategory()
//        {
//            return await FaQCategoryCRUD.GetAllFaQCategory();
//        }
//          [HttpGet]
//        public async Task<ApiListResult<FaQCategoryDto>> GetAllFaQCategoryWithDetail()
//        {
//            return await FaQCategoryCRUD.GetAllFaQCategoryWithDetail();
//        }

//        #endregion
//        #region
//        //[HttpPost]
//        //public async Task<ApiResult<ContactUsDto>> AddContactUs([FromBody] AddContactUsDto dto)
//        //{
//        //    return await ContactUsCRUD.AddContactUs(dto);
//        //}
//        [HttpPut]
//        public async Task<ApiResult<ContactUsDto>> EditContactUsStatus([FromBody] EditContactUsStatusDto dto)
//        {
//            return await ContactUsCRUD.EditContactUsStatus(dto);
//        }
//        //[HttpDelete]
//        //public async Task<BaseApiResult> DeleteContactUs([FromQuery] BaseByIntDto dto)
//        //{
//        //    return await ContactUsCRUD.DeleteContactUs(dto);
//        //}
       
//        [HttpGet]
//        public async Task<ApiPageResult<ContactUsDto>> GetContactUsByFilter([FromQuery] GetContactUsByFilterDto dto)
//        {
//            return await ContactUsCRUD.GetContactUsByFilter(dto);
//        }

//        #endregion

//        #region
//        //[HttpPost]
//        //public async Task<ApiResult<AboutUsDto>> AddAboutUs([FromBody] AddAboutUsDto dto)
//        //{
//        //    return await AboutUsCRUD.AddAboutUs(dto);
//        //}
//        [HttpPut]
//        public async Task<ApiResult<AboutUsDto>> EditAboutUs([FromBody] EditAboutUsDto dto)
//        {
//            return await AboutUsCRUD.EditAboutUs(dto);
//        }
//        //[HttpDelete]
//        //public async Task<BaseApiResult> DeleteAboutUs([FromQuery] BaseByIntDto dto)
//        //{
//        //    return await AboutUsCRUD.DeleteAboutUs(dto);
//        //}

//        [HttpGet]
//        public async Task<ApiResult<AboutUsDto>> GetAboutUs()
//        {
//            return await AboutUsCRUD.GetAboutUs();
//        }

//        #endregion
//    }

//}