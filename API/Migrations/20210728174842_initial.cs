﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace API.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AboutUs",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Title = table.Column<string>(type: "text", nullable: true),
                    Description = table.Column<string>(type: "text", nullable: true),
                    isDeleted = table.Column<bool>(type: "boolean", nullable: false),
                    UpdatedAt = table.Column<long>(type: "bigint", nullable: true),
                    CreatedBy = table.Column<Guid>(type: "uuid", nullable: true),
                    UpdatedBy = table.Column<Guid>(type: "uuid", nullable: true),
                    CreatedAt = table.Column<long>(type: "bigint", nullable: true),
                    CreatedAtDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedAtDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AboutUs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ActiveCodes",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Code = table.Column<string>(type: "character varying(10)", maxLength: 10, nullable: false),
                    Mobile_FullNumber = table.Column<string>(type: "text", nullable: true),
                    Mobile_Number = table.Column<string>(type: "text", nullable: true),
                    Mobile_CountryCode = table.Column<string>(type: "text", nullable: true),
                    Email_Address = table.Column<string>(type: "text", nullable: true),
                    Email_isConfirmed = table.Column<bool>(type: "boolean", nullable: true),
                    isDeleted = table.Column<bool>(type: "boolean", nullable: false),
                    UpdatedAt = table.Column<long>(type: "bigint", nullable: true),
                    CreatedBy = table.Column<Guid>(type: "uuid", nullable: true),
                    UpdatedBy = table.Column<Guid>(type: "uuid", nullable: true),
                    CreatedAt = table.Column<long>(type: "bigint", nullable: true),
                    CreatedAtDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedAtDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ActiveCodes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Announcements",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Title = table.Column<string>(type: "text", nullable: true),
                    Description = table.Column<string>(type: "text", nullable: true),
                    Start = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    End = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    isDeleted = table.Column<bool>(type: "boolean", nullable: false),
                    UpdatedAt = table.Column<long>(type: "bigint", nullable: true),
                    CreatedBy = table.Column<Guid>(type: "uuid", nullable: true),
                    UpdatedBy = table.Column<Guid>(type: "uuid", nullable: true),
                    CreatedAt = table.Column<long>(type: "bigint", nullable: true),
                    CreatedAtDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedAtDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Announcements", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AppRoles",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: true),
                    isDeleted = table.Column<bool>(type: "boolean", nullable: false),
                    UpdatedAt = table.Column<long>(type: "bigint", nullable: true),
                    CreatedBy = table.Column<Guid>(type: "uuid", nullable: true),
                    UpdatedBy = table.Column<Guid>(type: "uuid", nullable: true),
                    CreatedAt = table.Column<long>(type: "bigint", nullable: true),
                    CreatedAtDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedAtDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<string>(type: "text", nullable: false),
                    Name = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<string>(type: "text", nullable: false),
                    UserName = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    Email = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(type: "boolean", nullable: false),
                    PasswordHash = table.Column<string>(type: "text", nullable: true),
                    SecurityStamp = table.Column<string>(type: "text", nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "text", nullable: true),
                    PhoneNumber = table.Column<string>(type: "text", nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(type: "boolean", nullable: false),
                    TwoFactorEnabled = table.Column<bool>(type: "boolean", nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true),
                    LockoutEnabled = table.Column<bool>(type: "boolean", nullable: false),
                    AccessFailedCount = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Countries",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    isDeleted = table.Column<bool>(type: "boolean", nullable: false),
                    UpdatedAt = table.Column<long>(type: "bigint", nullable: true),
                    CreatedBy = table.Column<Guid>(type: "uuid", nullable: true),
                    UpdatedBy = table.Column<Guid>(type: "uuid", nullable: true),
                    CreatedAt = table.Column<long>(type: "bigint", nullable: true),
                    CreatedAtDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedAtDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Countries", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Documents",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Url = table.Column<string>(type: "text", nullable: false),
                    DocumentType = table.Column<int>(type: "integer", nullable: false),
                    Size = table.Column<long>(type: "bigint", nullable: false),
                    Duration = table.Column<int>(type: "integer", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: true),
                    Discriminator = table.Column<string>(type: "text", nullable: false),
                    FileName = table.Column<string>(type: "text", nullable: true),
                    isDeleted = table.Column<bool>(type: "boolean", nullable: false),
                    UpdatedAt = table.Column<long>(type: "bigint", nullable: true),
                    CreatedBy = table.Column<Guid>(type: "uuid", nullable: true),
                    UpdatedBy = table.Column<Guid>(type: "uuid", nullable: true),
                    CreatedAt = table.Column<long>(type: "bigint", nullable: false),
                    CreatedAtDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedAtDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Documents", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "FaQCategorys",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Title = table.Column<string>(type: "text", nullable: true),
                    isDeleted = table.Column<bool>(type: "boolean", nullable: false),
                    UpdatedAt = table.Column<long>(type: "bigint", nullable: true),
                    CreatedBy = table.Column<Guid>(type: "uuid", nullable: true),
                    UpdatedBy = table.Column<Guid>(type: "uuid", nullable: true),
                    CreatedAt = table.Column<long>(type: "bigint", nullable: true),
                    CreatedAtDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedAtDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FaQCategorys", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Notifications",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Title = table.Column<string>(type: "text", nullable: true),
                    Body = table.Column<string>(type: "text", nullable: true),
                    ImageUrl = table.Column<string>(type: "text", nullable: true),
                    isDeleted = table.Column<bool>(type: "boolean", nullable: false),
                    UpdatedAt = table.Column<long>(type: "bigint", nullable: true),
                    CreatedBy = table.Column<Guid>(type: "uuid", nullable: true),
                    UpdatedBy = table.Column<Guid>(type: "uuid", nullable: true),
                    CreatedAt = table.Column<long>(type: "bigint", nullable: true),
                    CreatedAtDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedAtDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Notifications", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Updates",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    NewVersionCode = table.Column<int>(type: "integer", nullable: true),
                    FromVersionCode = table.Column<int>(type: "integer", nullable: true),
                    ToVersionCode = table.Column<int>(type: "integer", nullable: true),
                    OS = table.Column<int>(type: "integer", nullable: false),
                    PkgName = table.Column<string>(type: "text", nullable: true),
                    IsForce = table.Column<bool>(type: "boolean", nullable: false),
                    Description = table.Column<string>(type: "text", nullable: true),
                    Link = table.Column<string>(type: "text", nullable: true),
                    isDeleted = table.Column<bool>(type: "boolean", nullable: false),
                    UpdatedAt = table.Column<long>(type: "bigint", nullable: true),
                    CreatedBy = table.Column<Guid>(type: "uuid", nullable: true),
                    UpdatedBy = table.Column<Guid>(type: "uuid", nullable: true),
                    CreatedAt = table.Column<long>(type: "bigint", nullable: true),
                    CreatedAtDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedAtDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Updates", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    RoleId = table.Column<string>(type: "text", nullable: false),
                    ClaimType = table.Column<string>(type: "text", nullable: true),
                    ClaimValue = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    UserId = table.Column<string>(type: "text", nullable: false),
                    ClaimType = table.Column<string>(type: "text", nullable: true),
                    ClaimValue = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: false),
                    ProviderKey = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: false),
                    ProviderDisplayName = table.Column<string>(type: "text", nullable: true),
                    UserId = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "text", nullable: false),
                    RoleId = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "text", nullable: false),
                    LoginProvider = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: false),
                    Name = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: false),
                    Value = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Provinces",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    ShortName = table.Column<string>(type: "text", nullable: true),
                    CountryId = table.Column<int>(type: "integer", nullable: false),
                    isDeleted = table.Column<bool>(type: "boolean", nullable: false),
                    UpdatedAt = table.Column<long>(type: "bigint", nullable: true),
                    CreatedBy = table.Column<Guid>(type: "uuid", nullable: true),
                    UpdatedBy = table.Column<Guid>(type: "uuid", nullable: true),
                    CreatedAt = table.Column<long>(type: "bigint", nullable: true),
                    CreatedAtDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedAtDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Provinces", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Provinces_Countries_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Countries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Sliders",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ExternalLink = table.Column<string>(type: "text", nullable: true),
                    IsInside = table.Column<bool>(type: "boolean", nullable: false),
                    InternalLinkId = table.Column<int>(type: "integer", nullable: true),
                    DocumentId = table.Column<Guid>(type: "uuid", nullable: false),
                    isDeleted = table.Column<bool>(type: "boolean", nullable: false),
                    UpdatedAt = table.Column<long>(type: "bigint", nullable: true),
                    CreatedBy = table.Column<Guid>(type: "uuid", nullable: true),
                    UpdatedBy = table.Column<Guid>(type: "uuid", nullable: true),
                    CreatedAt = table.Column<long>(type: "bigint", nullable: true),
                    CreatedAtDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedAtDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sliders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Sliders_Documents_DocumentId",
                        column: x => x.DocumentId,
                        principalTable: "Documents",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Socials",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Title = table.Column<string>(type: "text", nullable: true),
                    Link = table.Column<string>(type: "text", nullable: true),
                    ImageId = table.Column<Guid>(type: "uuid", nullable: true),
                    AboutUsId = table.Column<int>(type: "integer", nullable: true),
                    SocialType = table.Column<int>(type: "integer", nullable: false),
                    isDeleted = table.Column<bool>(type: "boolean", nullable: false),
                    UpdatedAt = table.Column<long>(type: "bigint", nullable: true),
                    CreatedBy = table.Column<Guid>(type: "uuid", nullable: true),
                    UpdatedBy = table.Column<Guid>(type: "uuid", nullable: true),
                    CreatedAt = table.Column<long>(type: "bigint", nullable: true),
                    CreatedAtDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedAtDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Socials", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Socials_AboutUs_AboutUsId",
                        column: x => x.AboutUsId,
                        principalTable: "AboutUs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Socials_Documents_ImageId",
                        column: x => x.ImageId,
                        principalTable: "Documents",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "FaQs",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Question = table.Column<string>(type: "text", nullable: true),
                    Answer = table.Column<string>(type: "text", nullable: true),
                    CategoryId = table.Column<int>(type: "integer", nullable: false),
                    isDeleted = table.Column<bool>(type: "boolean", nullable: false),
                    UpdatedAt = table.Column<long>(type: "bigint", nullable: true),
                    CreatedBy = table.Column<Guid>(type: "uuid", nullable: true),
                    UpdatedBy = table.Column<Guid>(type: "uuid", nullable: true),
                    CreatedAt = table.Column<long>(type: "bigint", nullable: true),
                    CreatedAtDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedAtDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FaQs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FaQs_FaQCategorys_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "FaQCategorys",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Cities",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    PostalCode = table.Column<string>(type: "text", nullable: true),
                    Lat = table.Column<double>(type: "double precision", nullable: false),
                    Lng = table.Column<double>(type: "double precision", nullable: false),
                    ProvinceId = table.Column<int>(type: "integer", nullable: false),
                    isDeleted = table.Column<bool>(type: "boolean", nullable: false),
                    UpdatedAt = table.Column<long>(type: "bigint", nullable: true),
                    CreatedBy = table.Column<Guid>(type: "uuid", nullable: true),
                    UpdatedBy = table.Column<Guid>(type: "uuid", nullable: true),
                    CreatedAt = table.Column<long>(type: "bigint", nullable: true),
                    CreatedAtDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedAtDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cities", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Cities_Provinces_ProvinceId",
                        column: x => x.ProvinceId,
                        principalTable: "Provinces",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AppUsers",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: true),
                    FamilyName = table.Column<string>(type: "text", nullable: true),
                    Email_Address = table.Column<string>(type: "text", nullable: true),
                    Email_isConfirmed = table.Column<bool>(type: "boolean", nullable: true),
                    Mobile_FullNumber = table.Column<string>(type: "text", nullable: true),
                    Mobile_Number = table.Column<string>(type: "text", nullable: true),
                    Mobile_CountryCode = table.Column<string>(type: "text", nullable: true),
                    Birthday = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    Gender = table.Column<int>(type: "integer", nullable: true),
                    Password = table.Column<string>(type: "text", nullable: true),
                    Status = table.Column<int>(type: "integer", nullable: false),
                    Bio = table.Column<string>(type: "text", nullable: true),
                    ProvinceId = table.Column<int>(type: "integer", nullable: true),
                    City = table.Column<string>(type: "text", nullable: true),
                    ProfileImageId = table.Column<Guid>(type: "uuid", nullable: true),
                    UserSetting_SendEmail = table.Column<bool>(type: "boolean", nullable: true),
                    UserSetting_SendSMS = table.Column<bool>(type: "boolean", nullable: true),
                    UserSetting_SendNotification = table.Column<bool>(type: "boolean", nullable: true),
                    Salt = table.Column<string>(type: "text", nullable: true),
                    Address = table.Column<string>(type: "text", nullable: true),
                    StreetAddress = table.Column<string>(type: "text", nullable: true),
                    PostalCode = table.Column<string>(type: "text", nullable: true),
                    GiftCode = table.Column<string>(type: "text", nullable: true),
                    SalesManId = table.Column<Guid>(type: "uuid", nullable: true),
                    CityId = table.Column<int>(type: "integer", nullable: true),
                    DocumentId = table.Column<Guid>(type: "uuid", nullable: true),
                    isDeleted = table.Column<bool>(type: "boolean", nullable: false),
                    UpdatedAt = table.Column<long>(type: "bigint", nullable: true),
                    CreatedBy = table.Column<Guid>(type: "uuid", nullable: true),
                    UpdatedBy = table.Column<Guid>(type: "uuid", nullable: true),
                    CreatedAt = table.Column<long>(type: "bigint", nullable: true),
                    CreatedAtDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedAtDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppUsers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AppUsers_Cities_CityId",
                        column: x => x.CityId,
                        principalTable: "Cities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AppUsers_Documents_DocumentId",
                        column: x => x.DocumentId,
                        principalTable: "Documents",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AppUsers_Documents_ProfileImageId",
                        column: x => x.ProfileImageId,
                        principalTable: "Documents",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AppUsers_Provinces_ProvinceId",
                        column: x => x.ProvinceId,
                        principalTable: "Provinces",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AppUserRoles",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    UserId = table.Column<Guid>(type: "uuid", nullable: false),
                    RoleId = table.Column<int>(type: "integer", nullable: false),
                    isDeleted = table.Column<bool>(type: "boolean", nullable: false),
                    UpdatedAt = table.Column<long>(type: "bigint", nullable: true),
                    CreatedBy = table.Column<Guid>(type: "uuid", nullable: true),
                    UpdatedBy = table.Column<Guid>(type: "uuid", nullable: true),
                    CreatedAt = table.Column<long>(type: "bigint", nullable: true),
                    CreatedAtDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedAtDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppUserRoles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AppUserRoles_AppRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AppRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AppUserRoles_AppUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AppUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Chats",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    CustomerId = table.Column<Guid>(type: "uuid", nullable: false),
                    isDeleted = table.Column<bool>(type: "boolean", nullable: false),
                    UpdatedAt = table.Column<long>(type: "bigint", nullable: true),
                    CreatedBy = table.Column<Guid>(type: "uuid", nullable: true),
                    UpdatedBy = table.Column<Guid>(type: "uuid", nullable: true),
                    CreatedAt = table.Column<long>(type: "bigint", nullable: true),
                    CreatedAtDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedAtDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Chats", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Chats_AppUsers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "AppUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ContactUs",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Message = table.Column<string>(type: "text", nullable: true),
                    UserId = table.Column<Guid>(type: "uuid", nullable: false),
                    Status = table.Column<int>(type: "integer", nullable: false),
                    isDeleted = table.Column<bool>(type: "boolean", nullable: false),
                    UpdatedAt = table.Column<long>(type: "bigint", nullable: true),
                    CreatedBy = table.Column<Guid>(type: "uuid", nullable: true),
                    UpdatedBy = table.Column<Guid>(type: "uuid", nullable: true),
                    CreatedAt = table.Column<long>(type: "bigint", nullable: true),
                    CreatedAtDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedAtDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContactUs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ContactUs_AppUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AppUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Devices",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    UserId = table.Column<Guid>(type: "uuid", nullable: false),
                    PushId = table.Column<string>(type: "text", nullable: false),
                    isDeleted = table.Column<bool>(type: "boolean", nullable: false),
                    UpdatedAt = table.Column<long>(type: "bigint", nullable: true),
                    CreatedBy = table.Column<Guid>(type: "uuid", nullable: true),
                    UpdatedBy = table.Column<Guid>(type: "uuid", nullable: true),
                    CreatedAt = table.Column<long>(type: "bigint", nullable: false),
                    CreatedAtDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedAtDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Devices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Devices_AppUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AppUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DocumentUsers",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    DocumentId = table.Column<Guid>(type: "uuid", nullable: false),
                    UserId = table.Column<Guid>(type: "uuid", nullable: true),
                    UsertId = table.Column<Guid>(type: "uuid", nullable: false),
                    isDeleted = table.Column<bool>(type: "boolean", nullable: false),
                    UpdatedAt = table.Column<long>(type: "bigint", nullable: true),
                    CreatedBy = table.Column<Guid>(type: "uuid", nullable: true),
                    UpdatedBy = table.Column<Guid>(type: "uuid", nullable: true),
                    CreatedAt = table.Column<long>(type: "bigint", nullable: true),
                    CreatedAtDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedAtDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DocumentUsers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DocumentUsers_AppUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AppUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DocumentUsers_Documents_DocumentId",
                        column: x => x.DocumentId,
                        principalTable: "Documents",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserFile",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    UserId = table.Column<Guid>(type: "uuid", nullable: false),
                    DocumentId = table.Column<Guid>(type: "uuid", nullable: false),
                    Title = table.Column<string>(type: "text", nullable: true),
                    isDeleted = table.Column<bool>(type: "boolean", nullable: false),
                    UpdatedAt = table.Column<long>(type: "bigint", nullable: true),
                    CreatedBy = table.Column<Guid>(type: "uuid", nullable: true),
                    UpdatedBy = table.Column<Guid>(type: "uuid", nullable: true),
                    CreatedAt = table.Column<long>(type: "bigint", nullable: false),
                    CreatedAtDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedAtDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserFile", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserFile_AppUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AppUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserFile_Documents_DocumentId",
                        column: x => x.DocumentId,
                        principalTable: "Documents",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserNotifications",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Title = table.Column<string>(type: "text", nullable: true),
                    Body = table.Column<string>(type: "text", nullable: true),
                    ImageUrl = table.Column<string>(type: "text", nullable: true),
                    IsSeen = table.Column<bool>(type: "boolean", nullable: false),
                    UserId = table.Column<Guid>(type: "uuid", nullable: false),
                    isDeleted = table.Column<bool>(type: "boolean", nullable: false),
                    UpdatedAt = table.Column<long>(type: "bigint", nullable: true),
                    CreatedBy = table.Column<Guid>(type: "uuid", nullable: true),
                    UpdatedBy = table.Column<Guid>(type: "uuid", nullable: true),
                    CreatedAt = table.Column<long>(type: "bigint", nullable: true),
                    CreatedAtDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedAtDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserNotifications", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserNotifications_AppUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AppUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Messages",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    SenderId = table.Column<Guid>(type: "uuid", nullable: false),
                    DocumentId = table.Column<Guid>(type: "uuid", nullable: true),
                    Text = table.Column<string>(type: "text", nullable: true),
                    IsAdmin = table.Column<bool>(type: "boolean", nullable: false),
                    ChatId = table.Column<Guid>(type: "uuid", nullable: false),
                    TempId = table.Column<Guid>(type: "uuid", nullable: false),
                    isDeleted = table.Column<bool>(type: "boolean", nullable: false),
                    UpdatedAt = table.Column<long>(type: "bigint", nullable: true),
                    CreatedBy = table.Column<Guid>(type: "uuid", nullable: true),
                    UpdatedBy = table.Column<Guid>(type: "uuid", nullable: true),
                    CreatedAt = table.Column<long>(type: "bigint", nullable: true),
                    CreatedAtDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedAtDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Messages", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Messages_AppUsers_SenderId",
                        column: x => x.SenderId,
                        principalTable: "AppUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Messages_Chats_ChatId",
                        column: x => x.ChatId,
                        principalTable: "Chats",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Messages_Documents_DocumentId",
                        column: x => x.DocumentId,
                        principalTable: "Documents",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "AppRoles",
                columns: new[] { "Id", "CreatedAt", "CreatedAtDate", "CreatedBy", "Name", "UpdatedAt", "UpdatedAtDate", "UpdatedBy", "isDeleted" },
                values: new object[,]
                {
                    { 1, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "User", null, null, null, false },
                    { 2, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Admin", null, null, null, false },
                    { 3, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Clinic Manager", null, null, null, false },
                    { 4, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Employee", null, null, null, false },
                    { 5, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Accountant", null, null, null, false },
                    { 6, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Influencer", null, null, null, false }
                });

            migrationBuilder.InsertData(
                table: "AppUsers",
                columns: new[] { "Id", "Address", "Bio", "Birthday", "City", "CityId", "CreatedAt", "CreatedAtDate", "CreatedBy", "DocumentId", "FamilyName", "Gender", "GiftCode", "Name", "Password", "PostalCode", "ProfileImageId", "ProvinceId", "SalesManId", "Salt", "Status", "StreetAddress", "UpdatedAt", "UpdatedAtDate", "UpdatedBy", "isDeleted", "Email_Address", "Email_isConfirmed", "Mobile_CountryCode", "Mobile_FullNumber", "Mobile_Number" },
                values: new object[,]
                {
                    { new Guid("11111111-1111-1111-1111-111111111111"), null, null, null, null, null, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "Rezaei", null, null, "Mohammad Reza", "fhkZYYYv0VjI8ICpV1JhHZgPn7dNqngb1/keDJ6NNFVlRYeOZR8aBw==", null, null, null, null, "gfb0yyyoum8TEFKBgFv/eg+6PYsCnOeelc27dsuMutqQyElFCHj0ng==", 1, null, null, null, null, false, "mohrezaeib@gmail.com", true, "98", "989308071499", "9308071499" },
                    { new Guid("11111111-1111-1111-1111-111111111112"), null, null, null, null, null, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "Khaki", null, null, "Kimia", "Bpl2gsvWruQ4lR72ZrUMsQ+U65bHzWPUK5cK3ksZmU31layGzj5Izw==", null, null, null, null, "yM/xrd6WlOpWjyM+MNFKcQTVL4keGx4PBLgVVxu06ApiisD1O53Ggg==", 1, null, null, null, null, false, "kimiakhaki@gmail.com", true, "98", "989360064055", "9360064055" },
                    { new Guid("11111111-1111-1111-1111-111111111114"), null, null, null, null, null, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "Sabet Nejad", null, null, "Mohammad", "fudb1zSyADLgG/ul2H/raV7mcQJT/qQH/x20cYEoBBoOIcUYZeF6sg==", null, null, null, null, "xWtWYrlBWYFVVPWhZUhjnRl2RmWR492L/iCelpjkqvS+XGu2f4/p1A==", 1, null, null, null, null, false, "msabetnejad@gmail.com", true, "98", "989131234567", "9131234567" },
                    { new Guid("11111111-1111-1111-1111-111111111115"), null, null, null, null, null, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "Najafi", null, null, "Mohammad", "TiOvF9WkZq4vIXBL43fYD2cLXKuN0sVZIMH4IY1oE/vsID3DDcQv2Q==", null, null, null, null, "uet6T6hEh3v/Iw2Vxb2lhvrHJn6R+Vranuu6WR4jkdHsPUwYLEPdHQ==", 1, null, null, null, null, false, "mohammadnjf950@gmail.com", true, "98", "989136843492", "9136843492" }
                });

            migrationBuilder.InsertData(
                table: "Countries",
                columns: new[] { "Id", "CreatedAt", "CreatedAtDate", "CreatedBy", "Name", "UpdatedAt", "UpdatedAtDate", "UpdatedBy", "isDeleted" },
                values: new object[] { 1, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Canada", null, null, null, false });

            migrationBuilder.InsertData(
                table: "Documents",
                columns: new[] { "Id", "CreatedAt", "CreatedAtDate", "CreatedBy", "Discriminator", "DocumentType", "Duration", "Name", "Size", "UpdatedAt", "UpdatedAtDate", "UpdatedBy", "Url", "isDeleted" },
                values: new object[,]
                {
                    { new Guid("11111111-1111-1111-1111-111111111139"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Document", 1, 0, null, 0L, null, null, null, "/Documents/11111111-1111-1111-1111-111111111139.jpg", false },
                    { new Guid("11111111-1111-1111-1111-111111111140"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Document", 1, 0, null, 0L, null, null, null, "/Documents/11111111-1111-1111-1111-111111111140.jpg", false },
                    { new Guid("11111111-1111-1111-1111-111111111141"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Document", 1, 0, null, 0L, null, null, null, "/Documents/11111111-1111-1111-1111-111111111141.jpg", false },
                    { new Guid("11111111-1111-1111-1111-111111111142"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Document", 1, 0, null, 0L, null, null, null, "/Documents/11111111-1111-1111-1111-111111111142.jpg", false },
                    { new Guid("11111111-1111-1111-1111-111111111150"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Document", 1, 0, null, 0L, null, null, null, "/Documents/11111111-1111-1111-1111-111111111150.svg", false },
                    { new Guid("11111111-1111-1111-1111-111111111151"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Document", 1, 0, null, 0L, null, null, null, "/Documents/11111111-1111-1111-1111-111111111151.svg", false },
                    { new Guid("11111111-1111-1111-1111-111111111152"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Document", 1, 0, null, 0L, null, null, null, "/Documents/11111111-1111-1111-1111-111111111152.svg", false },
                    { new Guid("11111111-1111-1111-1111-111111111153"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Document", 1, 0, null, 0L, null, null, null, "/Documents/11111111-1111-1111-1111-111111111153.svg", false },
                    { new Guid("11111111-1111-1111-1111-111111111154"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Document", 1, 0, null, 0L, null, null, null, "/Documents/11111111-1111-1111-1111-111111111154.svg", false },
                    { new Guid("11111111-1111-1111-1111-111111111155"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Document", 1, 0, null, 0L, null, null, null, "/Documents/11111111-1111-1111-1111-111111111155.svg", false },
                    { new Guid("11111111-1111-1111-1111-111111111161"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Document", 1, 0, null, 0L, null, null, null, "/Documents/11111111-1111-1111-1111-111111111161.jpg", false },
                    { new Guid("11111111-1111-1111-1111-111111111157"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Document", 1, 0, null, 0L, null, null, null, "/Documents/11111111-1111-1111-1111-111111111157.jpg", false },
                    { new Guid("11111111-1111-1111-1111-111111111158"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Document", 1, 0, null, 0L, null, null, null, "/Documents/11111111-1111-1111-1111-111111111158.jpg", false },
                    { new Guid("11111111-1111-1111-1111-111111111159"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Document", 1, 0, null, 0L, null, null, null, "/Documents/11111111-1111-1111-1111-111111111159.jpg", false },
                    { new Guid("11111111-1111-1111-1111-111111111160"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Document", 1, 0, null, 0L, null, null, null, "/Documents/11111111-1111-1111-1111-111111111160.jpg", false },
                    { new Guid("11111111-1111-1111-1111-111111111138"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Document", 1, 0, null, 0L, null, null, null, "/Documents/11111111-1111-1111-1111-111111111138.jpg", false },
                    { new Guid("11111111-1111-1111-1111-111111111162"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Document", 1, 0, null, 0L, null, null, null, "/Documents/11111111-1111-1111-1111-111111111162.jpg", false },
                    { new Guid("11111111-1111-1111-1111-111111111163"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Document", 1, 0, null, 0L, null, null, null, "/Documents/11111111-1111-1111-1111-111111111163.jpg", false },
                    { new Guid("11111111-1111-1111-1111-111111111164"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Document", 1, 0, null, 0L, null, null, null, "/Documents/11111111-1111-1111-1111-111111111164.jpg", false },
                    { new Guid("11111111-1111-1111-1111-111111111165"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Document", 1, 0, null, 0L, null, null, null, "/Documents/11111111-1111-1111-1111-111111111165.jpg", false },
                    { new Guid("11111111-1111-1111-1111-111111111166"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Document", 1, 0, null, 0L, null, null, null, "/Documents/11111111-1111-1111-1111-111111111166.jpg", false },
                    { new Guid("11111111-1111-1111-1111-111111111156"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Document", 1, 0, null, 0L, null, null, null, "/Documents/11111111-1111-1111-1111-111111111156.jpg", false },
                    { new Guid("11111111-1111-1111-1111-111111111137"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Document", 1, 0, null, 0L, null, null, null, "/Documents/11111111-1111-1111-1111-111111111137.jpg", false },
                    { new Guid("11111111-1111-1111-1111-111111111132"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Document", 1, 0, null, 0L, null, null, null, "/Documents/11111111-1111-1111-1111-111111111132.jpg", false },
                    { new Guid("11111111-1111-1111-1111-111111111135"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Document", 1, 0, null, 0L, null, null, null, "/Documents/11111111-1111-1111-1111-111111111135.jpg", false },
                    { new Guid("11111111-1111-1111-1111-111111111111"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Document", 1, 0, null, 0L, null, null, null, "/Documents/11111111-1111-1111-1111-111111111111.jpg", false },
                    { new Guid("11111111-1111-1111-1111-111111111112"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Document", 1, 0, null, 0L, null, null, null, "/Documents/11111111-1111-1111-1111-111111111112.jpg", false },
                    { new Guid("11111111-1111-1111-1111-111111111113"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Document", 1, 0, null, 0L, null, null, null, "/Documents/11111111-1111-1111-1111-111111111113.jpg", false },
                    { new Guid("11111111-1111-1111-1111-111111111114"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Document", 1, 0, null, 0L, null, null, null, "/Documents/11111111-1111-1111-1111-111111111114.jpg", false },
                    { new Guid("11111111-1111-1111-1111-111111111115"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Document", 1, 0, null, 0L, null, null, null, "/Documents/11111111-1111-1111-1111-111111111115.jpg", false },
                    { new Guid("11111111-1111-1111-1111-111111111116"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Document", 1, 0, null, 0L, null, null, null, "/Documents/11111111-1111-1111-1111-111111111116.jpg", false },
                    { new Guid("11111111-1111-1111-1111-111111111117"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Document", 1, 0, null, 0L, null, null, null, "/Documents/11111111-1111-1111-1111-111111111117.jpg", false },
                    { new Guid("11111111-1111-1111-1111-111111111118"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Document", 1, 0, null, 0L, null, null, null, "/Documents/11111111-1111-1111-1111-111111111118.jpg", false },
                    { new Guid("11111111-1111-1111-1111-111111111119"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Document", 1, 0, null, 0L, null, null, null, "/Documents/11111111-1111-1111-1111-111111111119.jpg", false },
                    { new Guid("11111111-1111-1111-1111-111111111120"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Document", 1, 0, null, 0L, null, null, null, "/Documents/11111111-1111-1111-1111-111111111120.jpg", false },
                    { new Guid("11111111-1111-1111-1111-111111111121"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Document", 1, 0, null, 0L, null, null, null, "/Documents/11111111-1111-1111-1111-111111111121.jpg", false },
                    { new Guid("11111111-1111-1111-1111-111111111122"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Document", 1, 0, null, 0L, null, null, null, "/Documents/11111111-1111-1111-1111-111111111122.jpg", false },
                    { new Guid("11111111-1111-1111-1111-111111111123"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Document", 1, 0, null, 0L, null, null, null, "/Documents/11111111-1111-1111-1111-111111111123.jpg", false },
                    { new Guid("11111111-1111-1111-1111-111111111124"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Document", 1, 0, null, 0L, null, null, null, "/Documents/11111111-1111-1111-1111-111111111124.jpg", false },
                    { new Guid("11111111-1111-1111-1111-111111111125"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Document", 1, 0, null, 0L, null, null, null, "/Documents/11111111-1111-1111-1111-111111111125.jpg", false },
                    { new Guid("11111111-1111-1111-1111-111111111126"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Document", 1, 0, null, 0L, null, null, null, "/Documents/11111111-1111-1111-1111-111111111126.jpg", false },
                    { new Guid("11111111-1111-1111-1111-111111111130"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Document", 1, 0, null, 0L, null, null, null, "/Documents/11111111-1111-1111-1111-111111111130.jpg", false },
                    { new Guid("11111111-1111-1111-1111-111111111131"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Document", 1, 0, null, 0L, null, null, null, "/Documents/11111111-1111-1111-1111-111111111131.jpg", false },
                    { new Guid("11111111-1111-1111-1111-111111111167"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Document", 1, 0, null, 0L, null, null, null, "/Documents/11111111-1111-1111-1111-111111111167.jpg", false },
                    { new Guid("11111111-1111-1111-1111-111111111133"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Document", 1, 0, null, 0L, null, null, null, "/Documents/11111111-1111-1111-1111-111111111133.jpg", false },
                    { new Guid("11111111-1111-1111-1111-111111111134"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Document", 1, 0, null, 0L, null, null, null, "/Documents/11111111-1111-1111-1111-111111111134.jpg", false },
                    { new Guid("11111111-1111-1111-1111-111111111136"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Document", 1, 0, null, 0L, null, null, null, "/Documents/11111111-1111-1111-1111-111111111136.jpg", false },
                    { new Guid("11111111-1111-1111-1111-111111111168"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Document", 1, 0, null, 0L, null, null, null, "/Documents/11111111-1111-1111-1111-111111111168.jpg", false }
                });

            migrationBuilder.InsertData(
                table: "AppUserRoles",
                columns: new[] { "Id", "CreatedAt", "CreatedAtDate", "CreatedBy", "RoleId", "UpdatedAt", "UpdatedAtDate", "UpdatedBy", "UserId", "isDeleted" },
                values: new object[,]
                {
                    { new Guid("11111111-1111-1111-1111-111111317111"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 2, null, null, null, new Guid("11111111-1111-1111-1111-111111111111"), false },
                    { new Guid("11111111-1111-1111-1111-111111177415"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 1, null, null, null, new Guid("11111111-1111-1111-1111-111111111115"), false },
                    { new Guid("11111111-1111-1111-1111-111111167315"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 4, null, null, null, new Guid("11111111-1111-1111-1111-111111111115"), false },
                    { new Guid("11111111-1111-1111-1111-111111147115"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 2, null, null, null, new Guid("11111111-1111-1111-1111-111111111115"), false },
                    { new Guid("11111111-1111-1111-1111-111111217414"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 1, null, null, null, new Guid("11111111-1111-1111-1111-111111111114"), false },
                    { new Guid("11111111-1111-1111-1111-111111207314"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 4, null, null, null, new Guid("11111111-1111-1111-1111-111111111114"), false },
                    { new Guid("11111111-1111-1111-1111-111111197214"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 3, null, null, null, new Guid("11111111-1111-1111-1111-111111111114"), false },
                    { new Guid("11111111-1111-1111-1111-111111187114"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 2, null, null, null, new Guid("11111111-1111-1111-1111-111111111114"), false },
                    { new Guid("11111111-1111-1111-1111-111111157215"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 3, null, null, null, new Guid("11111111-1111-1111-1111-111111111115"), false },
                    { new Guid("11111111-1111-1111-1111-111111127312"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 4, null, null, null, new Guid("11111111-1111-1111-1111-111111111112"), false },
                    { new Guid("11111111-1111-1111-1111-111111117212"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 3, null, null, null, new Guid("11111111-1111-1111-1111-111111111112"), false },
                    { new Guid("11111111-1111-1111-1111-111111107112"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 2, null, null, null, new Guid("11111111-1111-1111-1111-111111111112"), false },
                    { new Guid("11111111-1111-1111-1111-111111617311"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 4, null, null, null, new Guid("11111111-1111-1111-1111-111111111111"), false },
                    { new Guid("11111111-1111-1111-1111-111111517311"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 3, null, null, null, new Guid("11111111-1111-1111-1111-111111111111"), false },
                    { new Guid("11111111-1111-1111-1111-111111417211"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 1, null, null, null, new Guid("11111111-1111-1111-1111-111111111111"), false },
                    { new Guid("11111111-1111-1111-1111-111111137412"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 1, null, null, null, new Guid("11111111-1111-1111-1111-111111111112"), false }
                });

            migrationBuilder.InsertData(
                table: "AppUsers",
                columns: new[] { "Id", "Address", "Bio", "Birthday", "City", "CityId", "CreatedAt", "CreatedAtDate", "CreatedBy", "DocumentId", "FamilyName", "Gender", "GiftCode", "Name", "Password", "PostalCode", "ProfileImageId", "ProvinceId", "SalesManId", "Salt", "Status", "StreetAddress", "UpdatedAt", "UpdatedAtDate", "UpdatedBy", "isDeleted" },
                values: new object[,]
                {
                    { new Guid("11111111-1111-1111-1111-111111111122"), null, null, null, null, null, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "three", null, null, "customer", null, null, new Guid("11111111-1111-1111-1111-111111111133"), null, null, null, 1, null, null, null, null, false },
                    { new Guid("11111111-1111-1111-1111-111111111123"), null, null, null, null, null, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "four", null, null, "customer", null, null, new Guid("11111111-1111-1111-1111-111111111134"), null, null, null, 1, null, null, null, null, false },
                    { new Guid("11111111-1111-1111-1111-111111111124"), null, null, null, null, null, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "five", null, null, "customer", null, null, new Guid("11111111-1111-1111-1111-111111111135"), null, null, null, 1, null, null, null, null, false },
                    { new Guid("11111111-1111-1111-1111-111111111125"), null, null, null, null, null, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "six", null, null, "customer", null, null, new Guid("11111111-1111-1111-1111-111111111136"), null, null, null, 1, null, null, null, null, false },
                    { new Guid("11111111-1111-1111-1111-111111111129"), null, null, null, null, null, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "ten", null, null, "customer", null, null, new Guid("11111111-1111-1111-1111-111111111140"), null, null, null, 1, null, null, null, null, false },
                    { new Guid("11111111-1111-1111-1111-111111111127"), null, null, null, null, null, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "eight", null, null, "customer", null, null, new Guid("11111111-1111-1111-1111-111111111138"), null, null, null, 1, null, null, null, null, false },
                    { new Guid("11111111-1111-1111-1111-111111111128"), null, null, null, null, null, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "nine", null, null, "customer", null, null, new Guid("11111111-1111-1111-1111-111111111139"), null, null, null, 1, null, null, null, null, false },
                    { new Guid("11111111-1111-1111-1111-111111111130"), null, null, null, null, null, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "eleven", null, null, "customer", null, null, new Guid("11111111-1111-1111-1111-111111111141"), null, null, null, 1, null, null, null, null, false },
                    { new Guid("11111111-1111-1111-1111-111111111121"), null, null, null, null, null, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "two", null, null, "customer", null, null, new Guid("11111111-1111-1111-1111-111111111132"), null, null, null, 1, null, null, null, null, false },
                    { new Guid("11111111-1111-1111-1111-111111111126"), null, null, null, null, null, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "seven", null, null, "customer", null, null, new Guid("11111111-1111-1111-1111-111111111137"), null, null, null, 1, null, null, null, null, false },
                    { new Guid("11111111-1111-1111-1111-111111111120"), null, null, null, null, null, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "one", null, null, "customer", null, null, new Guid("11111111-1111-1111-1111-111111111131"), null, null, null, 1, null, null, null, null, false }
                });

            migrationBuilder.InsertData(
                table: "AppUsers",
                columns: new[] { "Id", "Address", "Bio", "Birthday", "City", "CityId", "CreatedAt", "CreatedAtDate", "CreatedBy", "DocumentId", "FamilyName", "Gender", "GiftCode", "Name", "Password", "PostalCode", "ProfileImageId", "ProvinceId", "SalesManId", "Salt", "Status", "StreetAddress", "UpdatedAt", "UpdatedAtDate", "UpdatedBy", "isDeleted", "Email_Address", "Email_isConfirmed", "Mobile_CountryCode", "Mobile_FullNumber", "Mobile_Number" },
                values: new object[] { new Guid("11111111-1111-1111-1111-111111111113"), null, null, null, null, null, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "Jalali", null, null, "Milad", "HihQtiJtbS2CSb2ALdY2tFw5/jpmdjE7iQGp3gXCOENckq3NQPjIEg==", null, new Guid("11111111-1111-1111-1111-111111111168"), null, null, "R+JvcbJu4fI4ae76w7YjdE5mQrQkw55pt+lLhxs1GipdBmpqpkJ0Og==", 1, null, null, null, null, false, "miladjalali.dev@gmail.com", true, "98", "989365857579", "9365857579" });

            migrationBuilder.InsertData(
                table: "AppUsers",
                columns: new[] { "Id", "Address", "Bio", "Birthday", "City", "CityId", "CreatedAt", "CreatedAtDate", "CreatedBy", "DocumentId", "FamilyName", "Gender", "GiftCode", "Name", "Password", "PostalCode", "ProfileImageId", "ProvinceId", "SalesManId", "Salt", "Status", "StreetAddress", "UpdatedAt", "UpdatedAtDate", "UpdatedBy", "isDeleted" },
                values: new object[] { new Guid("11111111-1111-1111-1111-111111111131"), null, null, null, null, null, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "twelve", null, null, "customer", null, null, new Guid("11111111-1111-1111-1111-111111111142"), null, null, null, 1, null, null, null, null, false });

            migrationBuilder.InsertData(
                table: "Provinces",
                columns: new[] { "Id", "CountryId", "CreatedAt", "CreatedAtDate", "CreatedBy", "Name", "ShortName", "UpdatedAt", "UpdatedAtDate", "UpdatedBy", "isDeleted" },
                values: new object[,]
                {
                    { 9, 1, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "New Brunswick", "NB", null, null, null, false },
                    { 8, 1, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Newfoundland and Labrador", "NL", null, null, null, false },
                    { 7, 1, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Saskatchewan", "SK", null, null, null, false },
                    { 6, 1, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Nova Scotia", "NS", null, null, null, false },
                    { 4, 1, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Alberta", "AB", null, null, null, false },
                    { 3, 1, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "British Columbia", "BC", null, null, null, false },
                    { 2, 1, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Quebec", "QC", null, null, null, false },
                    { 1, 1, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Ontario", "ON", null, null, null, false },
                    { 5, 1, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Manitoba", "MB", null, null, null, false }
                });

            migrationBuilder.InsertData(
                table: "Sliders",
                columns: new[] { "Id", "CreatedAt", "CreatedAtDate", "CreatedBy", "DocumentId", "ExternalLink", "InternalLinkId", "IsInside", "UpdatedAt", "UpdatedAtDate", "UpdatedBy", "isDeleted" },
                values: new object[,]
                {
                    { 3, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new Guid("11111111-1111-1111-1111-111111111117"), null, 1, true, null, null, null, false },
                    { 2, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new Guid("11111111-1111-1111-1111-111111111111"), null, 1, true, null, null, null, false },
                    { 1, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new Guid("11111111-1111-1111-1111-111111111124"), null, 1, true, null, null, null, false }
                });

            migrationBuilder.InsertData(
                table: "AppUserRoles",
                columns: new[] { "Id", "CreatedAt", "CreatedAtDate", "CreatedBy", "RoleId", "UpdatedAt", "UpdatedAtDate", "UpdatedBy", "UserId", "isDeleted" },
                values: new object[,]
                {
                    { new Guid("11111111-1111-1111-1111-111111177413"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 1, null, null, null, new Guid("11111111-1111-1111-1111-111111111113"), false },
                    { new Guid("11111111-1111-1111-1111-111111111120"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 1, null, null, null, new Guid("11111111-1111-1111-1111-111111111120"), false },
                    { new Guid("11111111-1111-1111-1111-111111111121"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 1, null, null, null, new Guid("11111111-1111-1111-1111-111111111121"), false },
                    { new Guid("11111111-1111-1111-1111-111111111122"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 1, null, null, null, new Guid("11111111-1111-1111-1111-111111111122"), false },
                    { new Guid("11111111-1111-1111-1111-111111111123"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 1, null, null, null, new Guid("11111111-1111-1111-1111-111111111123"), false },
                    { new Guid("11111111-1111-1111-1111-111111111124"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 1, null, null, null, new Guid("11111111-1111-1111-1111-111111111124"), false },
                    { new Guid("11111111-1111-1111-1111-111111111125"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 1, null, null, null, new Guid("11111111-1111-1111-1111-111111111125"), false },
                    { new Guid("11111111-1111-1111-1111-111111111126"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 1, null, null, null, new Guid("11111111-1111-1111-1111-111111111126"), false },
                    { new Guid("11111111-1111-1111-1111-111111167313"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 4, null, null, null, new Guid("11111111-1111-1111-1111-111111111113"), false },
                    { new Guid("11111111-1111-1111-1111-111111111128"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 1, null, null, null, new Guid("11111111-1111-1111-1111-111111111128"), false },
                    { new Guid("11111111-1111-1111-1111-111111111129"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 1, null, null, null, new Guid("11111111-1111-1111-1111-111111111129"), false },
                    { new Guid("11111111-1111-1111-1111-111111111130"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 1, null, null, null, new Guid("11111111-1111-1111-1111-111111111130"), false },
                    { new Guid("11111111-1111-1111-1111-111111111131"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 1, null, null, null, new Guid("11111111-1111-1111-1111-111111111131"), false },
                    { new Guid("11111111-1111-1111-1111-111111147113"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 2, null, null, null, new Guid("11111111-1111-1111-1111-111111111113"), false },
                    { new Guid("11111111-1111-1111-1111-111111157213"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 3, null, null, null, new Guid("11111111-1111-1111-1111-111111111113"), false },
                    { new Guid("11111111-1111-1111-1111-111111111127"), 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 1, null, null, null, new Guid("11111111-1111-1111-1111-111111111127"), false }
                });

            migrationBuilder.InsertData(
                table: "Cities",
                columns: new[] { "Id", "CreatedAt", "CreatedAtDate", "CreatedBy", "Lat", "Lng", "Name", "PostalCode", "ProvinceId", "UpdatedAt", "UpdatedAtDate", "UpdatedBy", "isDeleted" },
                values: new object[,]
                {
                    { 71, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 49.024999999999999, -122.8028, "White Rock", "V4B", 3, null, null, null, false },
                    { 70, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 49.157699999999998, -121.9509, "Chilliwack", "V4Z V2R V2P", 3, null, null, null, false },
                    { 66, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 49.364100000000001, -123.00660000000001, "North Vancouver", "V7P V7R V7L V7N V7H V7J V7K V7G", 3, null, null, null, false },
                    { 64, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 49.164200000000001, -123.93640000000001, "Nanaimo", "V9R V9S V9V V9T", 3, null, null, null, false },
                    { 57, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 50.676099999999998, -120.3408, "Kamloops", "V1S V2C V2B V2E", 3, null, null, null, false },
                    { 54, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 49.084699999999998, -123.0586, "Delta", "V4C V4E V4G V4K V4M V4L", 3, null, null, null, false },
                    { 46, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 48.484000000000002, -123.381, "Saanich", "V8N V8X V8Z V8P V8R V9A V9E V8Y", 3, null, null, null, false },
                    { 45, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 49.104399999999998, -122.5827, "Langley", "V1M V4W V2Z V2Y V3A", 3, null, null, null, false },
                    { 37, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 49.888100000000001, -119.4956, "Kelowna", "V1X V1Y V1P V1W V1V", 3, null, null, null, false },
                    { 38, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 49.049999999999997, -122.3167, "Abbotsford", "V4X V2S V2T V3G", 3, null, null, null, false },
                    { 72, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 49.216700000000003, -122.59999999999999, "Maple Ridge", "V4R V2W V2X", 3, null, null, null, false },
                    { 28, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 49.166699999999999, -123.13330000000001, "Richmond", "V6Y V6X V6W V6V V7E V7A V7B V7C", 3, null, null, null, false },
                    { 26, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 49.2667, -122.9667, "Burnaby", "V5B V5G V5E V5C V5J V5H V5A V3J V3N", 3, null, null, null, false },
                    { 17, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 48.4283, -123.3647, "Victoria", "V8T V8W V8S V8R V9A V8V", 3, null, null, null, false },
                    { 12, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 49.189999999999998, -122.8489, "Surrey", "V4A V4N V4P V3R V3S V3T V3V V3W V3X V3Z", 3, null, null, null, false },
                    { 3, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 49.25, -123.09999999999999, "Vancouver", "V6Z V6S V6R V6P V6N V6M V6L V6K V6J V6H V6G V6E V6C V6B V6A V5S V5P V5Z V5N V5L V5M V5K V5V V5W V5T V5R V5X V5Y", 3, null, null, null, false },
                    { 100, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 45.566699999999997, -73.200000000000003, "Beloeil", "J3G", 2, null, null, null, false },
                    { 96, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 45.670000000000002, -73.879999999999995, "Blainville", "J7B J7C J7E", 2, null, null, null, false },
                    { 39, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 49.283900000000003, -122.7919, "Coquitlam", "V3B V3C V3E V3J V3K", 3, null, null, null, false },
                    { 75, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 53.916899999999998, -122.74939999999999, "Prince George", "V2K V2N V2M V2L", 3, null, null, null, false },
                    { 4, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 51.049999999999997, -114.0667, "Calgary", "T1Y T2H T2K T2J T2L T2N T2A T2C T2B T2E T2G T2Y T2X T2Z T2S T2R T2T T2V T3N T3L T3M T3J T3K T3H T3G T3E T3B T3C T3A T3R T3S T3P", 4, null, null, null, false },
                    { 94, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 49.262500000000003, -122.7811, "Port Coquitlam", "V3B V3C", 3, null, null, null, false },
                    { 95, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 45.9636, -66.643100000000004, "Fredericton", "E3G E3C E3B E3A", 9, null, null, null, false },
                    { 83, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 45.2806, -66.076099999999997, "Saint John", "E2P E2L E2M E2N E2H E2J E2K", 9, null, null, null, false },
                    { 52, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 46.0989, -64.724199999999996, "Dieppe", "E1A", 9, null, null, null, false },
                    { 50, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 46.132800000000003, -64.7714, "Moncton", "E1H E1A E1C E1E E1G", 9, null, null, null, false },
                    { 49, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 47.481699999999996, -52.7971, "St. John's", "A1H A1S A1E A1G A1A A1C A1B", 8, null, null, null, false },
                    { 27, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 50.454700000000003, -104.6067, "Regina", "S4T S4V S4W S4R S4S S4X S4Y S4Z", 7, null, null, null, false },
                    { 24, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 52.133299999999998, -106.6833, "Saskatoon", "S7H S7K S7J S7M S7L S7N S7S S7R S7W S7V", 7, null, null, null, false },
                    { 59, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 46.1389, -60.193100000000001, "Cape Breton", "B2A B1S B1V B1G B1E B1B B1C B1A B1N B1L B1M B1J B1H B1T B1R B1P B1Y B1K", 6, null, null, null, false },
                    { 79, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 49.206899999999997, -122.9111, "New Westminster", "V3L V3M", 3, null, null, null, false },
                    { 15, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 44.647500000000001, -63.590600000000002, "Halifax", "B2Z B2Y B2X B2W B2V B2T B2S B2R B3T B3V B3P B3R B3L B3M B3N B3H B3J B3K B3E B3G B3A B3B B0J B3Z B3S B4E B4G B4A B4C B4B B0N", 6, null, null, null, false },
                    { 90, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 51.291699999999999, -114.01439999999999, "Airdrie", "T4B T4A", 4, null, null, null, false },
                    { 88, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 55.1708, -118.79470000000001, "Grande Prairie", "T8V T8X T8W", 4, null, null, null, false },
                    { 87, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 50.041699999999999, -110.67749999999999, "Medicine Hat", "T1A T1B T1C", 4, null, null, null, false },
                    { 93, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 45.216700000000003, -72.5167, "Lac-Brome", "J0E", 2, null, null, null, false },
                    { 78, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 57.604199999999999, -111.3284, "Wood Buffalo", "T9H T9J T9K T0P", 4, null, null, null, false },
                    { 60, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 49.694200000000002, -112.83280000000001, "Lethbridge", "T1H T1J T1K", 4, null, null, null, false },
                    { 56, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 52.268099999999997, -113.8111, "Red Deer", "T4R T4P T4N", 4, null, null, null, false },
                    { 5, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 53.534399999999998, -113.4903, "Edmonton", "T5X T5Y T5Z T5P T5R T5S T5T T5V T5W T5H T5J T5K T5L T5M T5N T5A T5B T5C T5E T5G T6X T6T T6W T6V T6P T6S T6R T6M T6L T6H T6K T6J T6E T6G T6A T6C T6B", 4, null, null, null, false },
                    { 8, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 49.884399999999999, -97.1464, "Winnipeg", "R2N R2M R2L R2K R2J R2H R2G R2C R2Y R2X R2W R2V R2R R2P R3L R3M R3N R3H R3J R3K R3E R3G R3A R3B R3X R3Y R3T R3V R3W R3P R3R R3S", 5, null, null, null, false },
                    { 85, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 53.630299999999998, -113.6258, "St. Albert", "T8N", 4, null, null, null, false },
                    { 92, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 45.616700000000002, -72.950000000000003, "Saint-Hyacinthe", "J2T J2S J2R", 2, null, null, null, false },
                    { 82, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 45.883299999999998, -72.4833, "Drummondville", "J2E J2C J2B J2A J1Z", 2, null, null, null, false },
                    { 58, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 43.166699999999999, -80.25, "Brantford", "N3P N3R N3S N3T N3V", 1, null, null, null, false },
                    { 55, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 42.422899999999998, -82.132400000000004, "Chatham", "N8A N0P N7L N7M", 1, null, null, null, false },
                    { 53, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 43.466700000000003, -80.5167, "Waterloo", "N2K N2J N2L N2V N2T", 1, null, null, null, false },
                    { 51, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 48.382199999999997, -89.246099999999998, "Thunder Bay", "P7G P7E P7B P7C P7J P7K", 1, null, null, null, false },
                    { 48, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 43.508299999999998, -79.883300000000006, "Milton", "L7J L0P L9T", 1, null, null, null, false },
                    { 44, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 43.8583, -79.0364, "Ajax", "L1Z L1T L1S", 1, null, null, null, false },
                    { 43, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 43.883299999999998, -78.941699999999997, "Whitby", "L0B L1P L1R L1M L1N", 1, null, null, null, false },
                    { 42, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 43.397199999999998, -80.311400000000006, "Cambridge", "N3H N3C N3E N1R N1S N1P N1T", 1, null, null, null, false },
                    { 41, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 43.549999999999997, -80.25, "Guelph", "N1C N1G N1E N1K N1H N1L", 1, null, null, null, false },
                    { 33, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 43.899999999999999, -78.849999999999994, "Oshawa", "L1L L1H L1J L1K L1G", 1, null, null, null, false },
                    { 32, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 44.371099999999998, -79.676900000000003, "Barrie", "L9J L4N L4M", 1, null, null, null, false },
                    { 31, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 43.316699999999997, -79.799999999999997, "Burlington", "L7R L7S L7P L7T L7N L7L L7M", 1, null, null, null, false },
                    { 30, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 43.450000000000003, -79.683300000000003, "Oakville", "L6M L6L L6H L6K L6J", 1, null, null, null, false },
                    { 29, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 43.866700000000002, -79.433300000000003, "Richmond Hill", "L4S L4C L4B L4E", 1, null, null, null, false },
                    { 23, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 42.283299999999997, -83.0, "Windsor", "N8T N8W N8P N8S N8R N8Y N8X N9J N9B N9C N9A N9G N9E N0R", 1, null, null, null, false },
                    { 21, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 43.833300000000001, -79.5, "Vaughan", "L0J L4K L4J L4H L4L L6A", 1, null, null, null, false },
                    { 20, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 43.060000000000002, -79.106700000000004, "Niagara Falls", "L2E L2G L2H L2J L3B", 1, null, null, null, false },
                    { 19, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 43.183300000000003, -79.2333, "St. Catharines", "L2M L2N L2P L2S L2R L2T L2W", 1, null, null, null, false },
                    { 18, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 43.8767, -79.263300000000001, "Markham", "L3T L3R L3P L3S L6E L6G L6C L6B", 1, null, null, null, false },
                    { 16, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 42.983600000000003, -81.249700000000004, "London", "N5Z N5X N5Y N5V N5W N6A N6P N6G N6E N6C N6N N6L N6J N6H N6B N6M N6K", 1, null, null, null, false },
                    { 13, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 43.418599999999998, -80.472800000000007, "Kitchener", "N2K N2H N2N N2M N2C N2B N2A N2G N2E N2R N2P", 1, null, null, null, false },
                    { 11, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 43.683300000000003, -79.7667, "Brampton", "L7A L6T L6W L6V L6P L6S L6R L6Y L6X L6Z", 1, null, null, null, false },
                    { 10, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 43.256700000000002, -79.869200000000006, "Hamilton", "L0R L0P L8W L8V L8T L8S L8R L8P L8G L8E L8N L8M L8L L8K L8J L8H L9G L9A L9B L9C L9H L9K N1R", 1, null, null, null, false },
                    { 7, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 43.600000000000001, -79.650000000000006, "Mississauga", "L4W L4V L4T L4Z L4Y L4X L5R L5V L5W L5A L5B L5C L5E L5G L5H L5J L5K L5L L5M L5N", 1, null, null, null, false },
                    { 6, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 45.424700000000001, -75.694999999999993, "Ottawa", "K4P K4M K4A K4B K4C K7S K1S K1R K1P K1W K1V K1T K1Z K1Y K1X K1C K1B K1G K1E K1K K1J K1H K1N K1M K1L K0A K2R K2S K2P K2V K2W K2T K2J K2K K2H K2L K2M K2B K2C K2A K2G K2E", 1, null, null, null, false },
                    { 62, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 43.935000000000002, -78.6083, "Clarington", "L0B L0A L1E L1B L1C", 1, null, null, null, false },
                    { 86, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 45.399999999999999, -72.7333, "Granby", "J2G J2J J2H", 2, null, null, null, false },
                    { 63, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 43.8354, -79.088999999999999, "Pickering", "L0H L0B L1X L1Y L1V L1W", 1, null, null, null, false },
                    { 69, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 44.049999999999997, -79.466700000000003, "Newmarket", "L3X L3Y", 1, null, null, null, false },
                    { 81, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 45.783299999999997, -74.0, "Saint-J�r�me", "J7Y J7Z J5L", 2, null, null, null, false },
                    { 80, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 45.380000000000003, -73.75, "Ch�teauguay", "J6K J6J", 2, null, null, null, false },
                    { 68, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 45.7333, -73.466700000000003, "Repentigny", "J6A J5Z J5Y", 2, null, null, null, false },
                    { 67, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 45.466700000000003, -73.450000000000003, "Brossard", "J4Y J4X J4Z J4W", 2, null, null, null, false },
                    { 61, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 45.316699999999997, -73.2667, "Saint-Jean-sur-Richelieu", "J2W J2Y J2X J3A J3B", 2, null, null, null, false },
                    { 47, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 45.700000000000003, -73.633300000000006, "Terrebonne", "J6Y J6V J7M J6X J6W", 2, null, null, null, false },
                    { 40, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 46.350000000000001, -72.549999999999997, "Trois-Rivi�res", "G9C G9B G9A G8T G8V G8W G8Y G8Z", 2, null, null, null, false },
                    { 36, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 46.799999999999997, -71.183300000000003, "L�vis", "G7A G6J G6K G6C G6Z G6X G6Y G6V G6W", 2, null, null, null, false },
                    { 35, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 48.416699999999999, -71.066699999999997, "Saguenay", "G8A G7N G7H G7K G7T G7Z G7G G7B G7J G7P G7S G7Y G7X", 2, null, null, null, false },
                    { 34, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 45.399999999999999, -71.900000000000006, "Sherbrooke", "J1N J1L J1M J1J J1K J1H J1G J1E J1C J1R", 2, null, null, null, false },
                    { 25, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 45.533299999999997, -73.5167, "Longueuil", "J4T J4V J4P J4R J4M J4L J4N J4H J4K J4J J4G J3Y J3Z", 2, null, null, null, false },
                    { 22, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 45.4833, -75.650000000000006, "Gatineau", "J8P J8R J8T J8Y J8X J8Z J8M J9J J9H J9A", 2, null, null, null, false },
                    { 14, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 45.583300000000001, -73.75, "Laval", "H7N H7L H7M H7J H7K H7H H7G H7E H7B H7C H7A H7X H7Y H7V H7W H7T H7R H7S H7P", 2, null, null, null, false },
                    { 9, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 46.813899999999997, -71.208100000000002, "Quebec City", "G1N G1M G1L G1K G1J G1H G1G G1E G1C G1B G1Y G1X G1W G1V G1T G1S G1R G1P G3E G3G G3K G3J G2G G2E G2B G2C G2A G2N G2L G2M G2J G2K", 2, null, null, null, false },
                    { 2, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 45.508899999999997, -73.561700000000002, "Montr�al", "H1X H1Y H1Z H1P H1R H1S H1T H1V H1W H1H H1J H1K H1L H1M H1N H1A H1B H1C H1E H1G H2Y H2X H2Z H2T H2W H2V H2P H2S H2R H2M H2L H2N H2H H2K H2J H2E H2G H2A H2C H2B H3B H3C H3A H3G H3E H3J H3K H3H H3N H3L H3M H3R H3S H3V H3W H3T H3X H4G H4E H4C H4B H4A H4N H4M H4L H4K H4J H4H H4V H4S H4R H4P H8N H8S H8R H8P H8T H8Z H8Y H9A H9C H9E H9H H9J H9K", 2, null, null, null, false },
                    { 99, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 46.299999999999997, -79.450000000000003, "North Bay", "P1A P1B P1C", 1, null, null, null, false },
                    { 98, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 42.9833, -79.2333, "Welland", "L3B L3C", 1, null, null, null, false },
                    { 97, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 44.0, -79.466700000000003, "Aurora", "L4G", 1, null, null, null, false },
                    { 91, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 43.630000000000003, -79.950000000000003, "Halton Hills", "L7J L7G L0P L9T", 1, null, null, null, false },
                    { 89, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 42.774999999999999, -81.183300000000003, "St. Thomas", "N5R N5P", 1, null, null, null, false },
                    { 84, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 43.866700000000002, -79.866699999999994, "Caledon", "L7K L7C L7E", 1, null, null, null, false },
                    { 77, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 42.999400000000001, -82.308899999999994, "Sarnia", "N7V N7W N7S N7T N7X", 1, null, null, null, false },
                    { 76, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 46.533299999999997, -84.349999999999994, "Sault Ste. Marie", "P6A P6C P6B", 1, null, null, null, false },
                    { 74, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 44.350000000000001, -78.75, "Kawartha Lakes", "L0K L0B L0A K9V K0M", 1, null, null, null, false },
                    { 73, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 44.299999999999997, -78.316699999999997, "Peterborough", "K9K K9J K9H K9L", 1, null, null, null, false },
                    { 65, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 46.490000000000002, -81.010000000000005, "Sudbury", "P0M P3N P3L P3B P3C P3A P3G P3Y P3P", 1, null, null, null, false },
                    { 1, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 43.741700000000002, -79.3733, "Toronto", "M5T M5V M5P M5S M5R M5E M5G M5A M5C M5B M5M M5N M5H M5J M4X M4Y M4R M4S M4P M4V M4W M4T M4J M4K M4H M4N M4L M4M M4B M4C M4A M4G M4E M3N M3M M3L M3K M3J M3H M3C M3B M3A M2P M2R M2L M2M M2N M2H M2J M2K M1C M1B M1E M1G M1H M1K M1J M1M M1L M1N M1P M1S M1R M1T M1W M1V M1X M9P M9R M9W M9V M9M M9L M9N M9A M9C M9B M6P M6R M6S M6A M6B M6C M6E M6G M6H M6J M6K M6L M6M M6N M8Z M8X M8Y M8V M8W", 1, null, null, null, false }
                });

            migrationBuilder.CreateIndex(
                name: "IX_AppUserRoles_RoleId",
                table: "AppUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_AppUserRoles_UserId",
                table: "AppUserRoles",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AppUsers_CityId",
                table: "AppUsers",
                column: "CityId");

            migrationBuilder.CreateIndex(
                name: "IX_AppUsers_DocumentId",
                table: "AppUsers",
                column: "DocumentId");

            migrationBuilder.CreateIndex(
                name: "IX_AppUsers_Email_Address",
                table: "AppUsers",
                column: "Email_Address",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AppUsers_Mobile_FullNumber",
                table: "AppUsers",
                column: "Mobile_FullNumber",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AppUsers_ProfileImageId",
                table: "AppUsers",
                column: "ProfileImageId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AppUsers_ProvinceId",
                table: "AppUsers",
                column: "ProvinceId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Chats_CustomerId",
                table: "Chats",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Cities_ProvinceId",
                table: "Cities",
                column: "ProvinceId");

            migrationBuilder.CreateIndex(
                name: "IX_ContactUs_UserId",
                table: "ContactUs",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Devices_UserId",
                table: "Devices",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_DocumentUsers_DocumentId",
                table: "DocumentUsers",
                column: "DocumentId");

            migrationBuilder.CreateIndex(
                name: "IX_DocumentUsers_UserId",
                table: "DocumentUsers",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_FaQs_CategoryId",
                table: "FaQs",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Messages_ChatId",
                table: "Messages",
                column: "ChatId");

            migrationBuilder.CreateIndex(
                name: "IX_Messages_DocumentId",
                table: "Messages",
                column: "DocumentId");

            migrationBuilder.CreateIndex(
                name: "IX_Messages_SenderId",
                table: "Messages",
                column: "SenderId");

            migrationBuilder.CreateIndex(
                name: "IX_Provinces_CountryId",
                table: "Provinces",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_Sliders_DocumentId",
                table: "Sliders",
                column: "DocumentId");

            migrationBuilder.CreateIndex(
                name: "IX_Socials_AboutUsId",
                table: "Socials",
                column: "AboutUsId");

            migrationBuilder.CreateIndex(
                name: "IX_Socials_ImageId",
                table: "Socials",
                column: "ImageId");

            migrationBuilder.CreateIndex(
                name: "IX_UserFile_DocumentId",
                table: "UserFile",
                column: "DocumentId");

            migrationBuilder.CreateIndex(
                name: "IX_UserFile_UserId",
                table: "UserFile",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserNotifications_UserId",
                table: "UserNotifications",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ActiveCodes");

            migrationBuilder.DropTable(
                name: "Announcements");

            migrationBuilder.DropTable(
                name: "AppUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "ContactUs");

            migrationBuilder.DropTable(
                name: "Devices");

            migrationBuilder.DropTable(
                name: "DocumentUsers");

            migrationBuilder.DropTable(
                name: "FaQs");

            migrationBuilder.DropTable(
                name: "Messages");

            migrationBuilder.DropTable(
                name: "Notifications");

            migrationBuilder.DropTable(
                name: "Sliders");

            migrationBuilder.DropTable(
                name: "Socials");

            migrationBuilder.DropTable(
                name: "Updates");

            migrationBuilder.DropTable(
                name: "UserFile");

            migrationBuilder.DropTable(
                name: "UserNotifications");

            migrationBuilder.DropTable(
                name: "AppRoles");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "FaQCategorys");

            migrationBuilder.DropTable(
                name: "Chats");

            migrationBuilder.DropTable(
                name: "AboutUs");

            migrationBuilder.DropTable(
                name: "AppUsers");

            migrationBuilder.DropTable(
                name: "Cities");

            migrationBuilder.DropTable(
                name: "Documents");

            migrationBuilder.DropTable(
                name: "Provinces");

            migrationBuilder.DropTable(
                name: "Countries");
        }
    }
}
