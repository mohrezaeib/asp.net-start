﻿using System;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using Utility.Tools;
using Utility.Tools.Auth;

using Utility.Tools.Swager;

using DomainCore.General;
using DomainData.Ef.Context;
using DomainData.Ef.Extentions;
using Atlantis.DomainServices;
using DomainData.Contracts;
using DomainData.Ef.Repositories;
using Utility.Tools.SMS;

using DomainData.Ef;
using Utility.Tools.EmailServies;

using DomainCore.Entities;
using Microsoft.Extensions.Hosting;
using WebApi.Middleware;
using Microsoft.AspNetCore.Http;
using MediatR;
using DomainServices.Contracts;
using Microsoft.AspNetCore.Identity;
using EmailService;

namespace AtlantisAPI
{
    public class Startup
    {

        public IConfiguration Configuration { get; }


        public Startup(IConfiguration Configuration)
        {
            this.Configuration = Configuration;
        }


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddOptions();
            services.AddSwager();
            services.AddMvc(options =>
            {
                options.EnableEndpointRouting = false;
            }).AddSessionStateTempDataProvider()
            .SetCompatibilityVersion(CompatibilityVersion.Latest);

             services.AddEntityFrameworkNpgsql().AddDbContext<MainContext>(opt =>
               opt.UseNpgsql(
               Configuration.GetConnectionString("Default"), b => b.MigrationsAssembly("API"))

           );

            services.AddCors(options =>
            {

                options.AddDefaultPolicy(
                        builder => builder
                         .SetIsOriginAllowedToAllowWildcardSubdomains()
                            .WithOrigins(
                            "http://*.api.org",
                            "http://api.org",
                            "http://*.api.com",
                            "http://api.com",
                           
                            "http://localhost:4200",
                            "http://127.0.0.1:5500"
                            )
                            .WithMethods("PUT", "POST", "DELETE", "GET")
                             .WithExposedHeaders("content-disposition")
                            .AllowAnyHeader()
                     .AllowCredentials()
                     .SetPreflightMaxAge(TimeSpan.FromMinutes(2))

                    );
            });



            services.AddJwt(Configuration);
            services.AddRepositories();
            services.AddApplicationServices();
            services.AddAutoMapperExtention();
            services.AddMediatR(  typeof(IApplicationService).Assembly);
            services.AddScoped<IEncrypter, Encrypter>();
            services.AddDbContext<IContext, MainContext>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddMemoryCache();
            services.AddLazyCache();


            services.AddHttpContextAccessor();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<ICurrentUser, CurrentUser>();
            Configuration.GetSection<AdminSettings>();

            services.AddScoped<ISimpleEmailSender, SimpleEmailSender>();
            var emailConfig = Configuration
                     .GetSection("EmailConfiguration")
                     .Get<EmailConfiguration>();
            services.AddSingleton(emailConfig);
            services.AddScoped<IEmailSender, EmailSender>();
            services.AddTwilioSMSService();
            services.FireBaseNotification(Configuration);

            services.AddControllers().AddNewtonsoftJson(options =>
                options.SerializerSettings.ReferenceLoopHandling =
                Newtonsoft.Json.ReferenceLoopHandling.Ignore
                );

            services.AddApplicationInsightsTelemetry();



            services.AddDefaultIdentity<IdentityUser>(options => options.SignIn.RequireConfirmedAccount = true)
                          .AddEntityFrameworkStores<MainContext>();

            services.Configure<DataProtectionTokenProviderOptions>(opt =>
            {
                opt.TokenLifespan = TimeSpan.FromHours(2);
            }
            );
            services.FireBaseNotification(Configuration);

            

        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env )
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }
            app.MigrationInitialisation();

            app.UseDeveloperExceptionPage();

            //app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();
            app.UseCors();

            app.UseAuthentication();
            app.UseAuthorization();



            app.UseMvcWithDefaultRoute(); ;
            app.ConfigureSwager();
            app.UseDefaultFiles();
            app.UseMiddleware<JwtMiddleware>();
           
        }



    }

  
}
