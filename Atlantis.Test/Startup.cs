﻿// using Atlantis.DomainServices;
// using DomainCore.Entities;
// using DomainData.Contracts;
// using DomainData.Ef;
// using DomainData.Ef.Context;
// using DomainData.Ef.Extentions;
// using DomainData.Ef.Repositories;
// using DomainServices.Contracts;
// using DomainServices.Services;
// using MediatR;
// using Microsoft.AspNetCore.Http;
// using Microsoft.EntityFrameworkCore;
// using Microsoft.Extensions.Configuration;
// using Microsoft.Extensions.DependencyInjection;
// using System;
// using System.Collections.Generic;
// using System.Linq;
// using System.Reflection;
// using System.Text;
// using System.Threading.Tasks;
// using Utility.Tools.Auth;
// using Utility.Tools.EmailServies;

// namespace Atlantis.IntegrationTest
// {
//     public class Startup
//     {
//         public void ConfigureServices(IServiceCollection services)
//         {

//             var configuration = new ConfigurationBuilder()
//                     .SetBasePath(System.IO.Directory.GetCurrentDirectory())
//                     .AddJsonFile("appsettings.json", false, true)
//                     .Build();


//             var connectionString = configuration.GetConnectionString("DefaultTest");
//             services.AddDbContext<MainContext>(options1 => options1.UseSqlServer(connectionString));
//             services.AddRepositories();
//             services.AddApplicationServices();
//             services.AddAutoMapperExtention();
//             services.AddScoped<IEncrypter, Encrypter>();
//             services.AddDbContext<IContext, MainContext>();
//             services.AddScoped<IUnitOfWork, UnitOfWork>();
//             services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
//             services.AddMemoryCache();

//             services.AddHttpContextAccessor();
//             services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
//             services.AddScoped<ICurrentUser, CurrentUser>();


//            // services.AddScoped<ISimpleEmailSender, EmailService>();
//            // services.AddEventServices();
//             services.AddMediatR(typeof(NewOrderCreatedDomainEventHandler).GetTypeInfo().Assembly);
//             //services.AddEventBus(configuration);



//             //services.AddTwilioSMSService();
//             //  services.FireBaseNotification(Configuration);

//             //services.AddTransient<IAdminOrder, AdminOrder>();
//             //services.AddTransient<IUnitOfWork, UnitOfWork>();
//             ////services.AddTransient<IBRepository, BRepository>();
//         }
//     }
// }
